mySettings = {
    onShiftEnter:  	{keepDefault:false, replaceWith:'[br]'},
    markupSet: [
        {name:'����������', key:'B', openWith:'[b]', closeWith:'[/b]'},
        {name:'������', key:'I', openWith:'[i]', closeWith:'[/i]'},
        {name:'������������', key:'U', openWith:'[u]', closeWith:'[/u]'},
        {separator:'---------------' },
        {name:'������', key:'L', openWith:'[url=[![�� ������ �������� ������. �������:\n\t- http://google.com\n\t- http://www.regforum.ru/showthread.php?t=46451\n\t- /posts/popular/:!:http://]!]]', closeWith:'[/url]', placeHolder:'����� ������'},
        {separator:'---------------' },
        {name:'������������� ������', openWith:'[list]\n', closeWith:'\n[/list]'},
        {name:'������������ ������', openWith:'[list=[![� ������ ������ ������ ������?:!:1]!]]\n', closeWith:'\n[/list]'},
        {name:'����� ������', openWith:'[*] '},
    ]
}