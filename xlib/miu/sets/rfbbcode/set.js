mySettings = {
    onShiftEnter:  	{keepDefault:false, replaceWith:'[br]'},
    markupSet: [
        {name:'����������', key:'B', openWith:'[b]', closeWith:'[/b]'},
        {name:'������', key:'I', openWith:'[i]', closeWith:'[/i]'},
        {name:'������������', key:'U', openWith:'[u]', closeWith:'[/u]'},
		{name:'������������', key:'H', openWith:'[h]', closeWith:'[/h]'},
		{name:'� ��������', className:'in_quotes', key:'Q', openWith:'[laquo]', closeWith:'[raquo]'},
        {separator:'---------------' },

        {name:'������', className:'link', key:'L', openWith:'[url=[![�� ������ �������� ������. �������:\n\t- http://google.com\n\t- http://www.regforum.ru/showthread.php?t=46451\n\t- /posts/popular/:!:http://]!]]', closeWith:'[/url]', placeHolder:'����� ������'},
        {name:'������ �� ���������', key:'M',
            className:'member',
            /* replaceWith:function(markItUp) {
                username = prompt("������� ��� ������������", "��������");
                if(username)
                userid = prompt("������� ID ������������", "2");
                if(userid)
                return '[member='+userid+']'+username+'[/member]';
            } */
        },
        {separator:'---------------' },
        {name:'������������� ������', className:'list-bullet', openWith:'[list]\n', closeWith:'\n[/list]'},
        {name:'������������ ������', className:'list-numeric', openWith:'[list=[![� ������ ������ ������ ������?:!:1]!]]\n', closeWith:'\n[/list]'},
        {name:'����� ������', className:'list-item', openWith:'[*] '},
        {separator:'---------------' },
        {name:'������', className:'quote', openWith:'[quote]', closeWith:'[/quote]'}
    ]
}