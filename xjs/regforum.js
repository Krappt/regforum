/**
 * Created with JetBrains PhpStorm.
 * User: max
 * Date: 05.04.13
 * Time: 11:46
 */

/*global window,document,jQuery,$,tinyMCE */
/*jslint browser: true, devel: true, bitwise: true, evil: true, nomen: true, plusplus: true, sloppy: true, todo: true, forin: true */

/* fallback on console calls */
if (window.console === undefined || window.console.log === undefined) {
    //noinspection JSLint
    console = {};
    console.log = function () {};
}

var JSON = JSON || {};

// implement JSON.stringify serialization
JSON.stringify = JSON.stringify || function (obj) {
    var t = typeof obj,
        result,
        n,
        v,
        json = [],
        arr = (obj && obj.constructor === Array);

    if (t !== "object" || obj === null) {
        // simple data type
        if (t === "string") {
            obj = '"' + obj + '"';
        }
        result = String(obj);
    } else {
        // recurse array or object
        for (n in obj) {
            //noinspection JSUnfilteredForInLoop
            v = obj[n];
            t = typeof v;
            if (t === "string") {
                v = '"' + v + '"';
            } else {
                if (t === "object" && v !== null) {
                    v = JSON.stringify(v);
                }
            }
            json.push((arr ? "" : '"' + n + '":') + String(v));
        }
        result = (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
    return result;
};

// implement JSON.parse de-serialization
JSON.parse = JSON.parse || function (str) {
    var p;
    if (str === "") {
        str = '""';
    }
    eval("var p=" + str + ";");
    return p;
};

/**
 * extend jQuery
 * @returns {{}}
 */
(function ($) {
    /**
     * jQuery serializeObject
     * @returns {*}
     */
    $.fn.serializeObject = function () {
        "use strict";
        var result,
            extend,
            node;

        result = {};
        extend = function (i, element) {
            node = result[element.name];

            // If node with same name exists already, need to convert it to an array as it
            // is a multi-value field (i.e., checkboxes)

            if (node !== undefined && node !== null) {
                if ($.isArray(node)) {
                    node.push(element.value);
                } else {
                    result[element.name] = [node, element.value];
                }
            } else {
                result[element.name] = element.value;
            }
        };

        $.each(this.serializeArray(), extend);
        return result;
    };

    /**
     * jQuery toggleDisabled
     * @returns {*}
     */
    $.fn.toggleDisabled = function () {
        return this.each(function () {
            this.disabled = !this.disabled;
        });
    };

}(jQuery));

$.expr[":"].contains = $.expr.createPseudo(function (arg) {
    return function (elem) {
        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});

/**
 * todo: merge all namespaces
 * @type {*|{}}
 */
$.regforum = $.regforum || {};

/**
 * Basic properties
 * $.regforum
 * @type {*}
 */
$.regforum = $.extend(true, $.regforum, {

    name : 'regforum',

    legacy: false,

    touch: false,

    /**
     * $.regforum.namespace
     * @param ns
     * @returns {*}
     */
    namespace: function (ns) {
        var parts = ns.split("."),
            parent = this,
            i,
            max;
        if (parts[0] === this.name) {
            parts = parts.slice(1);
        }
        for (i = 0, max = parts.length; i < max; i++) {
            if (parent[parts[i]] === undefined) {
                parent[parts[i]] = {};
            }
            parent = parent[parts[i]];
        }
        return parent;
    },

    settings: function () {
        return {
            zi: {
                max: 20000,
                dialog: 21000
            }
        };
    }
});

/**
 * Extended properties
 * $.regforum
 * @type {*}
 */
$.regforum = $.extend(true, $.regforum, {
    /**
     * $.regforum.parsePathname
     * @param pathname
     * @returns {Array}
     */
    parsePathname: function (pathname) {
        var data = [],
            prefix = this.name,
            location = document.location,
            separator = ' ';
        //noinspection JSLint
        (pathname === undefined) && (pathname = location.pathname);

        pathname = $.trim(pathname.replace(/\//g, separator));
        data = pathname.split(separator);
        //noinspection JSLint
        (data[0] === prefix) && data.shift();
        return data;
    },

    /**
     * $.regforum.getCurrentNamespace
     * @returns {string}
     */
    getCurrentNamespace: function () {
        return this.parsePathname().join(".");
    },

    /**
     * UUID generator
     * @returns {string}
     */
    uuid: function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            //noinspection JSLint
            var r = Math.random() * 16|0, v = c === 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    },

    /**
     * Creates document element
     * @param tagName
     * @returns {HTMLElement}
     */
    ce: function (tagName) {
        return document.createElement(tagName.toUpperCase());
    },

    /**
     * $.regforum.init
     */
    init: function () {
        //noinspection JSCheckFunctionSignatures
        var it = this,
            nsCurrent = it.parsePathname(),
            nsData,
            copyright = document.getElementsByClassName('copyright'),
            key;

        it.legacy = ($.browser.msie && ($.browser.version | 0) < 8);
        if (it.legacy) {
            (function () {
                var zIndexNumber = it.settings().zi.max;
                // Put your target element(s) in the selector below!
                $('body *:not(script,noscript,link,.ui-dialog)').each(function () { // overhead
                    $(this).css('zIndex', zIndexNumber);
                    zIndexNumber -= 5;
                });
            }());
        }

        //noinspection JSLint
        it.touch = ('ontouchstart' in window) || ('onmsgesturechange' in window);

        if (console) { console.log('Current namespace: ' + it.getCurrentNamespace()); }
        it.namespace(it.getCurrentNamespace());

        nsData = (function (_ns) {
            var initFn = $.regforum,
                i,
                max;

            for (i = 0, max = _ns.length; i < max; i++) {
                initFn = initFn[_ns[i]];
            }

            //noinspection JSLint
            if (!!initFn && !!initFn.init && typeof initFn.init == "function") {
                return initFn.init.call({});
            }
            return {};

        }(nsCurrent));

        function copyRight() {
            var selection = window.getSelection(),
                element_body,
                divnew,
                content,
                link;

            if (selection.toString().length < 250) { return; }

            element_body = document.getElementsByTagName('body')[0];
            link = document.createElement('p');
            divnew = document.createElement('div');
            content = selection.getRangeAt(0).cloneContents();

            divnew.style.position = 'absolute';
            divnew.style.left = '-99999px';
            link.innerHTML = "��������: <a href='" + document.location.href + "'>" + document.location.href + "</a>";
            content.appendChild(link);
            divnew.appendChild(content);
            element_body.appendChild(divnew);
            selection.selectAllChildren(divnew);
            window.setTimeout(function () {
                element_body.removeChild(divnew);
            }, 0);
        }

        if (copyright.length) {
            for (key in copyright) {
                copyright[key].oncopy = copyRight;
            }
        }

        //noinspection JSLint
        (it.getCurrentNamespace() === '') && it._initFrontpage();

        return nsData;
    },

    /**
     * @private
     * $.regforum._initFrontpage
     *
     */
    _initFrontpage: function () {

        var ticker = $('#ticker');

        ticker.find('ul').addClass('slider').end().find('li').addClass('slide');

        ticker.carousel({
            slider: '.slider',
            slide: '.slide',
            addPagination: false,
            addNav : true
        });

    },

    /**
     * $.regforum.events
     */
    events: {

        /**
         * $.regforum.events.register
         * @param selector
         * @param event
         * @param handlers
         */
        register: function (selector, event, handlers) {
            var i,
                m;
            selector = (selector instanceof jQuery) ? selector : $(selector);
            if (!handlers.length) {
                return;
            }
            for (i = 0, m = handlers.length; i < m; i++) {
                selector.on(event, handlers[i]);
            }
        },

        /**
         * $.regforum.events.unregister
         * @param selector
         * @param event
         * @returns {Array}
         */
        unregister: function (selector, event) {
            selector = (selector instanceof jQuery) ? selector : $(selector);
            var events = this.getHandlers(selector, event),
                handlers = [],
                ns = this,
                i,
                m;
            if (ns.handlersExist(selector, event)) {
                events = events[event];
                for (i = 0, m = events.length; i < m; i++) {
                    handlers.push(events[i]);
                }
                selector.off(event);
            }
            return handlers;
        },

        /**
         * $.regforum.events.getHandlers
         * @param selector
         * @param event
         * @returns {*} mixed
         */
        getHandlers: function (selector, event) {
            selector = (selector instanceof jQuery) ? selector : $(selector);
            return jQuery._data(selector[0]).events;
        },

        /**
         * $.regforum.events.handlersExist
         * @param selector
         * @param event
         * @returns {boolean}
         */
        handlersExist: function (selector, event) {
            var events = this.getHandlers(selector, event);
            return (!!events && !!events[event] && !!events[event].length);
        }
    },

    /**
     * $.regforum.upload
     */
    upload: {

        _images: [],

        _unloaderInit: false,

        uploadDir: '',

        modelName: undefined,

        _fancyboxContent: function () {
            return document.getElementById('uploaded-gallery-img');
        },

        _onUnload: function (form) {
            if (!$.regforum.upload._unloaderInit) {
                $(window).on('beforeunload', function () {
                    return '�� �������� ������� ������������� ���������.';
                });
                form.on('submit', function () {
                    $(window).off('beforeunload');
                });
                $.regforum.upload._unloaderInit = true;
            }
        },

        /**
         * $.regforum.upload.onDropzoneUpload
         *
         * @param response
         * @param originalFile
         */
        onDropzoneUpload: function (response, originalFile) {
            var it = this,
                _img,
                ti,
                jcropInstance,
                cropData = [],
                cropOptions = {
                    setSelect: [50, 50, 250, 150],
                    aspectRatio: 1.5
                },
                fancyLink = $('#fancy-link'),
                photosField = $('#_' + it.modelName + '_photos'),
                submits;

            switch (it.modelName) {
            case 'UserProfile':
                cropOptions = {
                    setSelect: [50, 50, 200, 200],
                    aspectRatio: 1
                };
                break;
            default:
                break;
            }

            submits = photosField.parents('form').find(':submit');
            submits.prop('disabled', true).fadeTo('fast', 0.7).removeClass('active-keyframe');

            if (!submits.filter(':last').next().hasClass('warning')) {
                submits.filter(':last').after(
                    $($.regforum.ce('div')).addClass('ml20 warning')
                        .html('<span class="sign sign_red">���������� ��������������� �����������</span>')
                );
            }


            if (response.success) {
                _img = $.regforum.ce('img');
                _img.setAttribute('src', response.uri);

                if (!!response.size) {
                    _img.setAttribute('width', response.size.width);
                    _img.setAttribute('height', response.size.height);
                }

                $(originalFile.previewTemplate)
                    .removeClass('success').addClass('pointer')
                    .on('click.preview', function () {

                        var preview = $(this),
                            hint,
                            tip = $.regforum.ce('span');

                        hint = function () {

                            var span = $($.regforum.ce('span'));

                            span.html('��������� ��������������');
                            span.on('click', function () {

                                cropData.uri = response.uri;
                                cropData.modelName = response.modelName;
                                cropData.type = originalFile.type;
                                cropData.modelId = fancyLink.data('model-id');

                                $.post('/image/crop/', cropData, function (data) {

                                    $('#fancybox-close').click();
                                    preview.off('click.preview').removeClass('pointer').addClass('success').fadeTo('fast', 0.5);
                                });
                            });

                            $($.regforum.upload._fancyboxContent()).find('.tip').hide().end().prepend(span);
                            $('#fancybox-content').css({'border-color' : 'lightgreen'});
                        };

                        $(tip).addClass('tip').html('�������� ������� ��� ������� �����������')
                            .delay(600)
                            .animate({
                                backgroundColor : '#ffffff',
                                color: '#333333'
                            }, 400);

                        $($.regforum.upload._fancyboxContent()).html('')
                            .append(_img)
                            .prepend(tip);

                        $('#fancybox-content').css({'border-color' : 'white'});

                        //noinspection JSLint
                        (!jcropInstance || jcropInstance.destroy());

                        $(_img).Jcrop($.extend({
                            keySupport: false,
                            bgColor: '#222222',
                            bgOpacity: 0.4,
                            onSelect: function (c) {
                                if (hint) {
                                    hint();
                                    hint = undefined;
                                }
                                cropData = c;
                            }
                        }, cropOptions), function () {
                            jcropInstance = this;
                        });

                        fancyLink.click();
                        return false;
                    });
                $.regforum.upload._images.push(response.filename);
                photosField.val($.regforum.upload._images.join(','));
            } else {
                $(originalFile.previewTemplate).removeClass('success').addClass('error');
            }
            ti = setInterval(function () {
                if (!$.regforum.upload.forceCrop()) {
                    submits.prop('disabled', false).fadeTo('fast', 1).addClass('active-keyframe');
                    $('.warning').remove();
                    $.regforum.upload._onUnload(photosField.parents('form'));
                    clearInterval(ti);
                }
            }, 400);

        },

        /**
         * $.regforum.upload.forceCrop
         * @returns {boolean}
         */
        forceCrop: function () {
            var it = this,
                result = false,
                previewsToCrop = $('.image-preview.processing:not(.error):not(.success)'),
                isFancyContent = $('#fancybox-content').is(':visible'),
                isFancyOverlay = $('#fancybox-overlay').is(':visible');

            if (previewsToCrop.length) {
                if (!isFancyContent && !isFancyOverlay) {
                    previewsToCrop[0].click();
                }
                result = true;
            }
            return result;
        },

        /**
         * $.regforum.upload.handlePhotosField
         */
        handlePhotosField: function () {
            var it = this,
                photosField = $('#_' + it.modelName + '_photos'),
                photosJoined;

            photosJoined = photosField.val();
            if (!!$.trim(photosJoined)) {
                it._images = photosJoined.split(',');
                $('dl.tabs dt#photos-gallery-tab').click();
            }
        },


        /**
         * $.regforum.upload.handleUploaderControls
         */
        handleUploaderControls: function (fn) {
            var it = this,
                tabs = $('dl.tabs dt');

            //noinspection JSLint
            tabs.length && tabs.on('click', function () {
                var self = this,
                    _f;

                _f = function () {
                    $(self)
                        .siblings().removeClass('selected').end()
                        .next('dd').andSelf().addClass('selected');
                };

                if (!!this.id) {
                    switch (this.id) {
                    case 'photos-uploader-tab':
                        if ($('#photos-gallery').find('.marked-to-remove').length) {
                            if (confirm(
                                    '� ��� ������� ���������� �� �������� �����������. '
                                        + '��� ������������ � ����� �������� ������ ������� ����� �����. '
                                        + '����������?'
                                )) {
                                _f();
                            }
                        } else {
                            _f();
                        }
                        break;
                    case 'photos-gallery-tab':
                        _f();
                        it.renderGallery();
                        break;
                    }
                } else {
                    _f();
                }

                //noinspection JSLint
                !fn || fn.call({});
            });
        },

        /**
         * $.regforum.upload.renderGallery
         */
        renderGallery: function () {

            var it = this,
                fancyLink = $('#fancy-link'),
                modelId = fancyLink.data('model-id'),
                photosField = $('#_' + it.modelName + '_photos'),
                ti,
                _img,
                _li,
                _container = $.regforum.ce('ul'),
                _gallery = $('#photos-gallery');

            _gallery.html('');

            ti = setInterval(function () { // wait for initialization from views
                if (it.uploadDir != '') {

                    var m = it._images.length,
                        updatePhotosField,
                        i;

                    updatePhotosField = function () {
                        var images = [];

                        $('#' + _gallery.attr('id')).find('li').filter(':not(.marked-to-remove)').find('img:visible').each(function (i) {
                            var src = this.src.split('/').slice(-1)[0];
                            if (src.indexOf('?') != -1) {
                                src = src.split('?')[0];
                            }
                            images[i] = src;
                        });
                        photosField.val(images.join(','));
                    };

                    if (!!m) {
                        for (i = 0; i < m; i++) {
                            _img = $.regforum.ce('img');
                            _img.src = it.uploadDir + modelId + '/' + it._images[i] + '?t=' + Math.random();
                            _img.height = (it.modelName == 'UserProfile') ? 186 : 180;

                            //noinspection JSLint
                            $(_img).on('click', function () {

                                var _this = this;

                                $(it._fancyboxContent()).html('')
                                    .append($($.regforum.ce('img')).attr('src', _this.src))
                                    .find('img')
                                    .one('load', function () {

                                        fancyLink.click();

                                    }).each(function () {
                                        //noinspection JSLint
                                        this.complete && $(this).load();
                                    });
                            });

                            _li = $.regforum.ce('li');
                            _li.appendChild(_img);

                            //noinspection JSLint
                            $(_li).mouseenter(function () {
                                var selfEl = this;
                                //noinspection JSLint
                                $(this).hasClass('marked-to-remove') || $(this).append(
                                    $($.regforum.ce('div')).html('<i class="i-zoom">���������</i><i class="i-remove">�������� �� ��������</i>')
                                ).find('.i-zoom').on('click', function () {
                                    $(this).parent().prev().click();
                                }).end().find('.i-remove').on('click', function () {
                                    var _this = this;
                                    if (confirm('����� ���������� ����� ����������� ����� �������')) {

                                        $(selfEl).addClass('marked-to-remove').fadeTo('fast', 0.4);
                                        $(_this).find('div').remove();
                                        photosField.parents('form').find(':submit').addClass('active-keyframe');
                                        $.regforum.upload._onUnload(photosField.parents('form'));
                                        updatePhotosField();
                                    }
                                });
                            }).mouseleave(function () {
                                $(this).find('div').remove();
                            });

                            _container.appendChild(_li);
                        }
                        _gallery.append(_container);

                        _li = null;
                        _img = null;
                        _container = null;


                        $('#' + _gallery.attr('id')).find('ul').dragsort({
                            dragSelector: 'li',
                            dragEnd: updatePhotosField,
                            dragBetween: false,
                            placeHolderTemplate: '<li></li>'
                        }).end().find('img').attr('title', '���������� �����������, ����� �����������');

                    } else {
                        _gallery.html('������� ���������� ��������� ����������.<br />����� �������� ����������� ����� ���� ��������������� � �����������.');
                    }

                    clearInterval(ti);
                }
            }, 10);
        },

        /**
         * $.regforum.upload.viewGallery
         */
        viewGallery: function (images) {
            var it = this,
                ti,
                fancyLink = $('#fancy-link'),
                _images = $(images);

            _images.on('click', function () {
                $(it._fancyboxContent()).html('')
                    .append($($.regforum.ce('img')).attr('src', this.src))
                    .find('img')
                    .one('load', function () {
                        fancyLink.click();
                    }).each(function () {
                        //noinspection JSLint
                        this.complete && $(this).load();
                    });
            });
        }

    },

    /**
     * $.regforum.topics
     */
    topics: {
        /**
         * $.regforum.topics.makeFavorite
         * @param control
         * @param topicId
         * @returns {boolean}
         */
        makeFavorite: function (control, topicId) {

            var action = 1,
                count = $('#favourite_count'),
                fcount = count.text() ? parseInt(count.text(), 10) : 0;

            //noinspection JSLint
            $(control).hasClass('remove_favourite') && (action = 0);

            $.ajax('/posts/topicfavourite/', {

                type: "GET",
                data: { topic_id: topicId, action: action },
                cache: false,
                success: function () {
                    if (action) {
                        $(control).removeClass('add_favourite').addClass('remove_favourite');
                        count.text(fcount + 1);
                    } else {
                        $(control).addClass('add_favourite').removeClass('remove_favourite');
                        count.text(fcount - 1);
                    }
                }
            });
            return false;
        },

        /**
         * $.regforum.topics.init
         */
        init: function () {
            var voteMembersCtrl = $('.topic_controls .vote .publication'),
                voteModeratorsCtrl = $('.vote-stats ul li span'),
                commentForm = '#comment-form',
                topicId = $('h1').attr('id');

            //noinspection JSLint
            /**
             * voted members control
             */
            voteMembersCtrl.length && voteMembersCtrl.on('click', function () {

                var votedMembers = $('.voted_members');

                $(commentForm).css('visibility', 'hidden')
                    .siblings('.topic_comment').removeClass('place-editor');

                if (votedMembers.is(':visible')) {
                    votedMembers.fadeOut('slow');
                } else {


                    $.ajax('/posts/getvotedmembers/', {
                        type: "GET",
                        data: { topicid: topicId },
                        cache: false,
                        success: function (response) {
                            $('.voted_members').html(response);
                        }
                    });
                    votedMembers.fadeIn('slow');
                }
            });

            //noinspection JSLint
            voteModeratorsCtrl.length && voteModeratorsCtrl.on('click', function () {

                var votedMembers = $('.voted_members'),
                    _this,
                    type;
                $.regforum.topics.hideCommentForm();

                if (votedMembers.is(':visible')) {
                    votedMembers.fadeOut('slow');
                } else {
                    _this = $(this);
                    type = _this.attr('id');

                    $.ajax('/posts/getsandboxvotedmembers/', {
                        type: "GET",
                        data: { topicid: topicId, votetype: type },
                        cache: false,
                        success: function (response) {
                            $('.voted_members').html(response);
                        }
                    });
                    votedMembers.fadeIn('slow');
                }
            });

            //���� ������ ������� + �������
            //����� ������� ������� - document, ��� �� ������ ��������� ���� ��� ����������� �, � ������������� ������, ��������� stopPropagation
            $(document).on('click', commentForm + ' input.btn_thank', function () {
                var _this = $(this);
                if (_this.closest(commentForm).find('input#parent_comment').val()) { return; } //���� ���� parent_comment ������ �������� �� �����������. ������������.
                $.post('',
                    { vote: 1 },
                    function (response) {
                        _this.removeClass("disabled");//������� ���������� ���������� �������
                        if (response && !response.errors) {
                            _this.parent().hide();
                            $('#topic_votectr').html(response);
                        } else if (response.errors) {
                            console.log(response.errors);
                        } else {
                            console.error('some ajax error');
                        }
                    });
            });

        },

        /**
         * $.regforum.topics.rejectForm
         * @param arr - ������ ���������, �� ����� �� ������� ������ ������������ ���������
         * @param text_field - ��������� ���� ���� ��������
         * @param additionalEl ��� ������ ��� ������� � ����������� ���������.
         */
        rejectForm: function (arr, text_field, additionalEl) {
            var additionalElVal = $(additionalEl).val() || '',
                msg_arr = {
                    not_uniq_cont: '� ��������� ���� ���������� ' + additionalElVal + ' ���� ��������� ��-�� �������������� ������. ' +
                        '\n' + '�� ������ ������ ��������� � ��� �������� � ������������ ��� ��������.',
                    didnt_discovered: '� ��������� ���� ���������� ' + additionalElVal + ' ���� ���������, ��� ��� ���� �� ��������. ' +
                        '\n' + '�� ������ ������ ��������� � ��� �������� � ������������ ��� ��������.',
                    conditions_violation: '� ��������� ���� ���������� ' + additionalElVal + ' ���� ��������� ��� �� ��������������� �������� ����������.',
                    advertisement: '� ��������� ���� ���������� ' + additionalElVal + ' ���� ��������� ��� ���������.',
                    another_reason: '� ��������� ���� ���������� ' + additionalElVal + ' ���� ��������� �� �������:' + '\n' +
                        '���� �� ����������� ������, � ��� ����� ��������������� ����� ��������, �� �� � ������������� �� ����������.'
                },
                greetings = '������������!' + '\n\n',
                ps = '� ���������� �����������, ��������.';

            $(arr).on('click', function () {
                var id = $(this).attr('id'),
                    text = $(text_field).val();
                if (msg_arr[id]) {
                    text += greetings + msg_arr[id] + '\n\n' + ps;
                    $(text_field).val(text);
                }
            });
        }
    },

    /**
     * $.regforum.profile
     */
    profile: {

        /**
         * $.regforum.profile.edit
         */
        edit: {

            /**
             * $.regforum.profile.edit.init
             */
            init: function () {

                var uploadNs = $.regforum.upload,
                    ti,
                    validatableFields = $('#UserProfile_phone, #Users_icq');

                ti = setInterval(function () {
                    if (uploadNs.uploadDir !== undefined) {
                        uploadNs.handleUploaderControls();
                        uploadNs.handlePhotosField();
                        clearInterval(ti);
                    }

                }, 10);

                validatableFields.on('keyup', function (e) {
                    var _value = $(this).val();
                    //noinspection JSLint
                    /^\d+$/.test(_value) || $(this).val(_value.slice(0,-1));
                });
            }
        },

        /**
         * $.regforum.profile.companies
         */
        companies: {

            /**
             * $.regforum.profile.companies.init
             */
            init: function (fn) {

                //noinspection JSLint
                !fn || fn.call({});
            },

            /**
             * $.regforum.profile.companies.officeHours
             *
             * Dependencies
             * jslider (http://egorkhmelev.github.com/jslider/)
             *
             * @param selector
             */
            officeHours: function (selector) {

                selector = selector || 'input[type="slider"]:visible';
                $(selector).slider({
                    from: 360,
                    to: 1320,
                    step: 15,
                    dimension: '',
                    /*
                    scale: [
                        '0:00','1:00','2:00','3:00','4:00','5:00',
                        '6:00','7:00','8:00','9:00','10:00','11:00',
                        '12:00','13:00','14:00','15:00','16:00','17:00',
                        '18:00','19:00','20:00','21:00','22:00','23:00',
                        '24:00'
                    ],
                    */
                    scale: [
                        '6:00', '8:00', '10:00',
                        '12:00', '14:00', '16:00',
                        '18:00', '20:00', '22:00'
                    ],
                    limits: false,
                    calculate: function (value) {
                        var hours = Math.floor(value / 60),
                            mins = (value - hours * 60);
                        return (hours < 10 ? "0" + hours : hours) + ":" + (mins == 0 ? "00" : mins);
                    }
                });

                return $(selector).slider();
            },

            /**
             * $.regforum.profile.companies.officeDaysPostRender
             */
            officeDaysPostRender: function (lastOnly) {
                var _checkAll,
                    _checkboxes,
                    ti,
                    rcoh,
                    _legacy;

                lastOnly = lastOnly || false;

                rcoh = $('.rcoh');
                rcoh = !lastOnly ? rcoh : rcoh.last();
                _checkboxes = rcoh.find('.rcoh-days :checkbox');

                _checkAll = _checkboxes.filter(function () {
                    return this.id.indexOf('_all') != -1;
                });

                _checkAll.off('click');

                _legacy = ($.browser.msie && ($.browser.version | 0) < 9);
                //noinspection JSLint
                !_legacy || $('div.form .rcoh .rcoh-days span:last-child label').css({
                    'background' : 'none',
                    'border' : 'none',
                    'padding' : 0,
                    'border-bottom' : '1px dashed #4444ba',
                    'color' : '#4444ba',
                    'filter': "progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#ffffff', GradientType=0)"
                }).parent().css('display', 'block');

                /**
                 * override checkAll handlers for checkBoxList
                 */
                ti = setInterval(function () {
                    var _handlers = $.regforum.events.unregister(_checkAll, 'click');

                    _checkAll.on('click', function () {
                        $(this).parents('.rcoh-days').find('input').not(':last').prop('checked', this.checked);
                    });

                    _checkboxes.on('click', function () {

                        var checkboxes = $(this).parents('div.rcoh-days').find(':checkbox'),
                            labels = checkboxes.prevAll(),
                            to;

                        to = setTimeout(function () {
                            labels.removeClass('checked');
                            checkboxes.filter(':checked').prevAll().addClass('checked');
                            clearTimeout(to);
                        }, 200);

                    });

                    _checkboxes.filter(':checked').prevAll().addClass('checked');

                    clearInterval(ti);
                }, 20);
            },

            /**
             * $.regforum.profile.companies.sliderPostRender
             */
            sliderPostRender: function () {
                var sliderScale = $('.jslider-scale');

                try {

                    switch ($('input[type="slider"]:visible').slider().settings.scale.length) {
                    case 25:
                        sliderScale.width(sliderScale.width() * 0.992);
                        break;
                    case 13:
                        sliderScale.width(sliderScale.width() * 1.005);
                        break;
                    }

                } catch (e) {
                    console.log('jslider is not initialized');
                }
            },

            /**
             * $.regforum.profile.companies.editorInit
             * tinyMCE settings
             */
            editorInit: function (fieldsToInit) {

                var user_dialog = $('#user_dialog'),
                    addNs = $.regforum.profile.companies.add;

                if (user_dialog.length) {
                    user_dialog.dialog({
                        modal: true,
                        autoOpen: false,
                        closeOnEscape: true,
                        resizable: false,
                        title: "������ �� ���������",
                        height: 500,
                        width: 320,
                        zIndex: $.regforum.settings().zi.dialog,
                        buttons: {
                            "�������": function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                }

                try {
                    //noinspection JSLint
                    tinyMCE.init({
                        mode: "exact", //exact
                        modal: false,
                        elements: fieldsToInit, //
                        onchange_callback: addNs.editorCounter,
                        handle_event_callback: addNs.editorCounter,
                        theme: "advanced",
                        language: "ru",
                        plugins: "paste,preview,fullscreen,searchreplace,inlinepopups",
                        width: "970",
                        height: "460",
                        gecko_spellcheck: true,
                        theme_advanced_buttons1: "bold,italic,underline,strikethrough,upper_�ase,lower_�ase,outdent,indent,blockquote,inquotes,user,|,search,replace,|,pasteword,|,bullist,numlist,|,undo,redo,|,image,insertimage,link,unlink,|,formatselect,|,removeformat,preview,code,fullscreen,spoiler",
                        theme_advanced_buttons2: "",
                        theme_advanced_buttons3: "",
                        theme_advanced_buttons4: "",
                        theme_advanced_toolbar_location: "top",
                        theme_advanced_toolbar_align: "left",
                        theme_advanced_statusbar_location: "bottom",
                        theme_advanced_resizing: false,
                        convert_urls: false,
                        theme_advanced_blockformats: "p,h2,blockquote",
                        content_css: "/xcss/tinymce_body.css?v12",
                        setup: function (ed) {
                            ed.onLoadContent.add(function (ed) {
                                var state = ed.dom.select('hr.spoiler').length;
                                ed.controlManager.setActive('spoiler', state);
                            });
                            ed.addButton('upper_�ase', {
                                title : '������������� � ������� ��������',
                                image : '/ximg/toolbar/edit-uppercase.png',
                                onclick : function () {
                                    ed.focus();
                                    ed.selection.select();
                                    ed.selection.setContent(ed.selection.getContent().toUpperCase());
                                }
                            });
                            ed.addButton('lower_�ase', {
                                title : '������������� � ������ ��������',
                                image : '/ximg/toolbar/edit-lowercase.png',
                                onclick : function () {
                                    ed.focus();
                                    ed.selection.select();
                                    ed.selection.setContent(ed.selection.getContent().toLowerCase());
                                }
                            });
                            ed.addButton('inquotes', {
                                title: '� ��������',
                                image: '/ximg/toolbar/in_quotes.png',
                                onclick: function () {
                                    ed.focus();
                                    ed.selection.select();
                                    ed.selection.setContent('&laquo;' + ed.selection.getSel() + '&raquo;');
                                }
                            });
                            ed.addButton('user', {
                                title: '������ �� ���������',
                                image: '/ximg/toolbar/user-add.png',
                                onclick: function () {
                                    ed.focus();

                                    $.regforum.helpers.autocomplete.findUsers(user_dialog, function () {
                                        var docLocation = document.location;
                                        ed.selection.setContent('<a class="member" href="' + docLocation.protocol + '//' + docLocation.host + '/members/' + this.id + '/">' + this.text + '</a>');
                                    });
                                }

                            });
                            ed.addButton('spoiler', {
                                title: '������ ����� ��� �������',
                                image: '/ximg/toolbar/spoiler.png',
                                onclick: function () {
                                    var hr = ed.dom.select('hr.spoiler'),
                                        state = true;
                                    if (hr.length) {
                                        ed.dom.remove(hr);
                                        state = false;
                                    } else {
                                        ed.selection.setNode(ed.dom.create('hr', { 'class' : 'spoiler'}, ''));
                                    }
                                    ed.controlManager.setActive('spoiler', state);
                                }
                            });
                        },
                        verify_html: true,
                        inline_styles: false,
                        apply_source_formatting: true,
                        convert_fonts_to_spans: true,
                        //valid_children : "-div[style,class],-p[style],-span[style,class]",
                        valid_elements: "a[href|target|class],strong,br,img[src|width|height|align|hspace|vspace],em,p[align|class],h2,blockquote,u,ol,ul,li,del,hr[class]",
                        invalid_elements: "iframe,script,applet,object,h3,h4,h5,h6,h7,h8,h9,cite,button,section,head,header,footer,article,embed,pre",
                        removeformat_selector: 'b,strong,em,i,span,ins,div,li,ul,h2',
                        'formats': {
                            'alignleft': {'selector': 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes': 'align-left'},
                            'aligncenter': {'selector': 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes': 'align-center'},
                            'alignright': {'selector': 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes': 'align-right'},
                            'alignfull': {'selector': 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes': 'align-justify'},
                            'italic': {'inline': 'em'},
                            'bold': {'inline': 'strong'},
                            'underline': {'inline': 'u'},
                            'strikethrough': {'inline': 'del'}
                        },
                        removeformat: [
                            {selector: 'div,b,strong,em,i,font,u,strike', remove: 'all', split: true, expand: false, block_expand: true, deep: true},
                            {selector: 'span', attributes: ['style', 'class'], remove: 'empty', split: true, expand: false, deep: true},
                            {selector: '*', attributes: ['style', 'class'], remove: 'all', split: true, expand: true, deep: true}
                        ],
                        protect: [
                            /\<\/?(if|endif)\>/g, // Protect <if> & </endif>
                            /\<xsl\:[^>]+\>/g, // Protect <xsl:...>
                            /<\?php.*?\?>/g // Protect php code
                        ]
                    });
                } catch (e) {}
            },

            /**
             * $.regforum.profile.companies.map
             * Dependencies
             *
             * YMaps (YMapshttp://api.yandex.ru/maps/doc/jsapi/)
             * TinyMCE
             */
            map: {

                _myMap: undefined,

                _placeMark: undefined,

                _point: [64, 104],

                _coordinates: '#map_coordinates',

                /**
                 * $.regforum.profile.companies.map.init
                 * @param fn
                 */
                init: function (fn) {
                    var it = $.regforum.profile.companies.map;

                    //noinspection JSCheckFunctionSignatures
                    it.setMap();
                    //noinspection JSCheckFunctionSignatures
                    it.setPlaceMark(it.getMap());

                    //noinspection JSUnresolvedVariable
                    it.getMap().geoObjects.add(it.getPlaceMark());

                    //noinspection JSUnusedLocalSymbols
                    it.getPlaceMark().events.add('dragend', function (e) {
                        //noinspection JSUnresolvedVariable,JSUnresolvedFunction
                        var point = it.getPlaceMark().geometry.getCoordinates();

                        it._point = point;
                        it.getMapCoordinates().val(point);
                        it.getMetroStation(point);

                    });

                    //noinspection JSLint
                    !fn || fn.call({});

                },

                /**
                 * $.regforum.profile.companies.map.setMap
                 * @param fn
                 * @param options
                 */
                setMap: function (options, fn) {

                    options = options || {
                        center: this._point,
                        zoom: 3,
                        behaviors: ['default', 'scrollZoom']
                    };

                    //noinspection JSUnresolvedVariable,JSUnresolvedFunction,JSLint
                    this._myMap = new ymaps.Map('map', options);

                    //noinspection JSLint
                    !fn || fn.call({});
                },

                /**
                 * $.regforum.profile.companies.map.getMap
                 * @returns {*}
                 */
                getMap: function () {
                    return this._myMap;
                },

                getMapCoordinates: function () {
                    return $(this._coordinates);
                },

                /**
                 * $.regforum.profile.companies.map.setPlaceMark
                 * @param map
                 * @param hint
                 * @param options
                 */
                setPlaceMark: function (map, hint, options) {

                    var it = this;

                    hint = hint || {
                        hintContent: '� ��������� ������ ��������������. ���� ����� �����������, ��������� ����� ������� ����'
                    };

                    options = options || {
                        draggable: true
                    };

                    //noinspection JSUnresolvedVariable,JSUnresolvedFunction,JSLint
                    it._placeMark = new ymaps.Placemark(it._point, hint, options);

                    //noinspection JSUnresolvedVariable
                    map.geoObjects.add(it._placeMark);

                    //noinspection JSUnusedLocalSymbols
                    it._placeMark.events.add('dragend', function (e) {
                        //noinspection JSUnresolvedVariable,JSUnresolvedFunction
                        var point = it._placeMark.geometry.getCoordinates();

                        it._point = point;
                        it.getMapCoordinates().val(point);
                        it.getMetroStation(point);
                    });
                },

                /**
                 * $.regforum.profile.companies.map.getPlaceMark
                 * @returns {*}
                 */
                getPlaceMark: function () {
                    return this._placeMark;
                },

                /**
                 * $.regforum.profile.companies.map.setCoordinatesByAddress
                 * @param address
                 * @param zoom
                 * @param fn
                 * @returns {boolean}
                 */
                setCoordinatesByAddress: function (address, zoom, fn) {

                    var it = this;

                    //noinspection JSUnresolvedVariable,JSUnresolvedFunction,JSLint
                    ymaps.geocode(address, {results: 1}).then(function (result) {

                        //noinspection JSUnresolvedVariable,JSUnresolvedFunction
                        var point = result.geoObjects.get(0).geometry.getCoordinates();
                        it._point = point;

                        //noinspection JSUnresolvedVariable,JSUnresolvedFunction
                        it.getPlaceMark().geometry.setCoordinates(point);

                        it.getMapCoordinates().val(point);

                        //noinspection JSUnresolvedFunction
                        it.getMap().panTo(point, {delay: 10, duration: 1000, callback: function () {
                            //noinspection JSUnresolvedFunction
                            it.getMap().setZoom(zoom);
                        }
                            });

                        it.getMetroStation(point);
                        //noinspection JSLint
                        !fn || fn.call({});
                    });
                    return false;

                },

                /**
                 * $.regforum.profile.companies.map.getMetroStation
                 * @param point
                 */
                getMetroStation: function (point) {

                    //noinspection JSUnresolvedVariable,JSUnresolvedFunction,JSLint
                    ymaps.geocode(point, { kind: 'metro', results: 3 }).then(function (result) {

                        var nm = null,
                            metro,
                            words,
                            geoObjects;

                        try {
                            //noinspection JSUnresolvedVariable
                            geoObjects = result.geoObjects.get(0);
                        } catch (e) {}

                        if (!!geoObjects) {
                            metro = geoObjects.properties.get('name');
                            words = metro.split(" ");
                            words.splice(0, 1);
                            nm = words.join(" ");
                        }
                        $('#nearest_metro').val(nm);
                    });
                }
            },

            /**
             * $.regforum.profile.companies.add
             */
            add: {

                /**
                 * $.regforum.profile.companies.add._dialogOptions
                 * jQuery UI dialog settings by default
                 */
                _dialogOptions : {
                    modal: true,
                    autoOpen: false,
                    closeOnEscape: true,
                    resizable: false,
                    height: 500,
                    show: 'fade',
                    hide: 'fade',
                    zIndex: $.regforum.settings().zi.dialog
                },

                /**
                 * $.regforum.profile.companies.add.init
                 */
                init: function (fn) {

                    var it = $.regforum.profile.companies.add,
                        ns = $.regforum.profile.companies.map,
                        slider,
                        ti,
                        _checkboxes,
                        _checkAll,
                        _checkHandler;

                    //noinspection JSUnresolvedVariable,JSLint
                    ymaps.ready(function () {
                        ns.init(it.setCompanyCoordinates);
                    });

                    slider = $.regforum.profile.companies.officeHours();

                    if (!$.isEmptyObject(slider)) {

                        $.regforum.profile.companies.sliderPostRender();
                        $.regforum.profile.companies.officeDaysPostRender();

                    }

                    it.formHandlers();

                    //noinspection JSLint
                    !fn || fn.call({});

                },

                /**
                 * $.regforum.profile.companies.add.setCompanyCoordinates
                 * @param point
                 */
                setCompanyCoordinates: function (point) {

                    var ns = $.regforum.profile.companies.map;

                    point = point || ns.getMapCoordinates().val().split(',');


                    if (point.length > 1) {
                        //noinspection JSUnresolvedVariable,JSUnresolvedFunction
                        ns.getPlaceMark().geometry.setCoordinates(point);
                        //noinspection JSUnresolvedFunction
                        ns.getMap().setCenter(point, 17);
                    }

                },

                /**
                 * $.regforum.profile.companies.add.editorCounter
                 */
                editorCounter: function () {
                    var editorId = $("#" + tinyMCE.activeEditor.id + "_counter"),
                        max = editorId.attr('max'),
                        x = tinyMCE.activeEditor.getContent(), //replace(/<[^>]+>/g, '')
                        available = max - x.length;
                    if (x.length < max - 200) {
                        editorId.removeClass('indianred').html('�� ������� ' + x.length + ' ��. � ������ ��������');
                    } else {
                        editorId.addClass('indianred').html('�������� ' + available + ' ��.');
                    }
                    //noinspection JSLint
                    (x.length > max) && editorId.html('�� ������� ������������ ���������� ��������');

                    return true;
                },

                /**
                 * $.regforum.profile.companies.add.formHandlers
                 */
                formHandlers: function () {

                    //noinspection SpellCheckingInspection
                    var it = this,
                        ns = $.regforum.profile.companies.map,
                        rDialog = $('#select_region_dialog'),
                        paDialog = $('#select_pa_dialog'),
                        sDialog = $('#select_street_dialog'),
                        warning = $(".message"),
                        phones,
                        name_company = $('#name_company'),
                        name_personal = $('#name_personal'),
                        topics = $('.topics'),
                        company_address,
                        kladr_region = $('#kladr_region'),
                        kladr_region_id = $('#kladr_region_id'),
                        kladr_population_aggregate = $('#kladr_population_aggregate'),
                        kladr_population_aggregate_id = $('#kladr_population_aggregate_id'),
                        kladr_street = $('#kladr_street'),
                        kladr_street_id = $('#kladr_street_id'),
                        street_manual_data = $('#street_manual_data'),
                        region = kladr_region.val(),
                        pa = kladr_population_aggregate.val(),
                        street = kladr_street.val(),
                        street_manual = street_manual_data.val(),
                        dom = $('#dom').val(),
                        str = $('#str').val(),
                        korp = $('#korp').val(),
                        form,
                        rcoh,
                        rcohInputCtrl,
                        _rcohInputCtrlHandler,
                        _clickHandler,
                        _submitHandler,
                        _submitHandlers = [],
                        _schedule,
                        remove_upload_file = $('.remove_upload_file');

                    phones = $('div .phones > div').length - 1;
                    form = name_company.parents('form');
                    rcoh = $('.rcoh');

                    _submitHandler = function (e) {

                        _schedule = [];
                        rcoh = $('.rcoh');
                        //noinspection JSLint
                        rcoh.length && $.each(rcoh, function () {
                            _schedule.push(
                                $(this).find('.rcoh-days, .rcoh-hours')
                                    .find('input')
                                    .filter(function () {
                                        return this.type != 'hidden';
                                    })
                                    .serializeObject()
                            );
                        });

                        $('#_schedule_serialized').val(JSON.stringify(_schedule));

                        return true;
                    };

                    _rcohInputCtrlHandler = function () {

                        var lastField;

                        rcoh = $('.rcoh');
                        rcoh.find('.input_ctrl').each(function () {
                            $(this).html('');
                        });

                        lastField = rcoh.last();
                        rcohInputCtrl = lastField.find('.input_ctrl');
                        rcohInputCtrl.append('<span class="add add_button" title="��������"></span>');
                        if (rcoh.length > 1) {
                            rcohInputCtrl.append('<span class="remove" title="�������"></span>');
                        }

                        rcohInputCtrl.find('.remove').on('click', function () {
                            $(this).parents('.rcoh').remove();
                            _rcohInputCtrlHandler();
                        });

                        rcohInputCtrl.find('.add').on('click', function () {

                            var pathData = $.regforum.parsePathname().slice(0, -1);
                            pathData.push('getofficehoursfield');

                            $.post(
                                '/' + pathData.join('/') + '/',
                                {'widgetId': $(this).parents('.rcoh').find('.rcoh-days div').attr('id')},
                                function (response) {
                                    var slider,
                                        newWidgetId,
                                        js,
                                        to;
                                    if (response && response.data) {
                                        lastField.after(response.data);
                                        slider = $.regforum.profile.companies.officeHours();

                                        if (!$.isEmptyObject(slider)) {
                                            $.regforum.profile.companies.sliderPostRender();
                                        }

                                        if (response.js) {
                                            js = response.js;
                                            newWidgetId = '#' + $('.rcoh').last().find('.rcoh-days div').attr('id');
                                            js = js.replace(/input/g, newWidgetId + ' input');

                                            eval(js);
                                            to = setTimeout(function () {
                                                $.regforum.profile.companies.officeDaysPostRender(true);
                                                clearTimeout(to);
                                            }, 50);
                                        }
                                        _rcohInputCtrlHandler();

                                    }
                                }
                            );
                            return false;
                        });
                    };

                    _rcohInputCtrlHandler();

                    if ($.regforum.events.handlersExist(form, 'submit')) {
                        _submitHandlers = $.regforum.events.unregister(form, 'submit');
                    }
                    _submitHandlers.unshift(_submitHandler);
                    $.regforum.events.register(form, 'submit', _submitHandlers);

                    company_address = region;
                    //noinspection JSLint
                    (pa != '') && (company_address = company_address + ', ' + pa);
                    //noinspection JSLint
                    (street != '') && (company_address = company_address + ', ' + street);
                    (street_manual != '') && (company_address = company_address + ', ' + street_manual);

                    _clickHandler = function (e) {
                        if ($(this).multiselect("widget").find("input:checked").length > 3) {
                            warning.addClass("error").removeClass("success").html("�� ������ ������� �� ����� ���� ��������");
                            return false;
                        } else {
                            warning.addClass("success").removeClass("error").html("Check a few boxes.");
                        }
                        return true;
                    };

                    $('#rubricator').multiselect({

                        noneSelectedText: '�������...',
                        show: ['fade', 300],
                        hide: ['fade', 300],
                        selectedText: '������� # �� 3',
                        minWidht: 160,
                        header: "�� ������ ������� �� ����� ���� ��������",
                        click: _clickHandler

                    });

                    it.dialogHandlers._buttons.locality = {
                        '�� ���������': function () {
                            kladr_population_aggregate.val('');
                            kladr_population_aggregate_id.val('');
                            $(this).dialog('close');
                        }
                    };

                    it.dialogHandlers._buttons.street = {

                        '��� � ������': function () {
                            kladr_street.val('');
                            kladr_street_id.val('');
                            $('#street_manual').show('fast');
                            $(this).dialog('close');
                        }
                    };

                    it.dialogHandlers.regionDialogInit(
                        rDialog,
                        kladr_region,
                        kladr_region_id,
                        kladr_population_aggregate,
                        kladr_population_aggregate_id,
                        kladr_street,
                        kladr_street_id,
                        function () {
                            $('#street_hint').text('');
                            $('#population_aggregate_hint').text('');
                            region = $('#' + kladr_region.attr('id')).val();
                            company_address = region;
                            $.regforum.profile.companies.map.setCoordinatesByAddress(company_address, 4);
                        }
                    );

                    it.dialogHandlers.localityDialogInit(
                        paDialog,
                        kladr_population_aggregate,
                        kladr_population_aggregate_id,
                        kladr_region_id,
                        $('#population_aggregate_hint'),
                        function () {
                            kladr_street.val('');
                            kladr_street_id.val('');
                            street = '';
                            street_manual = '';
                            company_address = $('#' + kladr_region.attr('id')).val() + ', ' + $('#' + kladr_population_aggregate.attr('id')).val();
                            $.regforum.profile.companies.map.setCoordinatesByAddress(company_address, 11);
                        }
                    );

                    it.dialogHandlers.streetDialogInit(
                        sDialog,
                        kladr_street,
                        kladr_street_id,
                        kladr_region,
                        kladr_region_id,
                        kladr_population_aggregate,
                        kladr_population_aggregate_id,
                        $('#street_hint'),
                        function () {

                            street_manual_data.val('');
                            region = $('#' + kladr_region.attr('id')).val();
                            street = $('#' + kladr_street.attr('id')).val();
                            pa = $('#' + kladr_population_aggregate.attr('id')).val();
                            company_address = (!!pa && pa.length) ?
                                (region + ', ' + pa + ', ' + street) :
                                (region + ", " + street);
                            $.regforum.profile.companies.map.setCoordinatesByAddress(company_address, 17);
                        }
                    );

                    $('#clear_population_aggregate').click(function () {

                        var pa_id = $('#kladr_population_aggregate_id');

                        if (pa_id.val() != '') {
                            kladr_population_aggregate.val('');
                            pa_id.val('');
                            kladr_street.val('');
                            kladr_street_id.val('');
                        }
                    });

                    $('#clear_street').click(function () {

                        var street_id;
                        street_id = kladr_street_id;

                        if (street_id.val() != '') {
                            kladr_street.val('');
                            street_id.val('');
                        }
                    });

                    $('#close_street_manual').click(function () {

                        street_manual_data.val('');
                        $(this).parent('#street_manual').hide('fast');

                    });

                    $('.add').live('click', function () {

                        var _phones;

                        if (phones < 11) {

                            phones++;

                            $('#phone1').clone(false, false).attr('id', 'phone' + phones).appendTo(".phones");

                            _phones = $('#phone' + phones);
                            _phones.find('input[type="text"],input[type="number"]').each(function (C, D) {
                                D.value = ""
                            });
                            //$('#phone'+phones).find('.hint').remove();

                            $('#phone' + (phones - 1)).find('.add, .remove').remove();

                            _phones.find('.input_ctrl').html(
                                '<span class="add add_button" title="��������"></span>' +
                                '<span class="remove" title="�������"></span>'

                            );

                        }
                    });

                    $('.remove').live('click', function () {

                        var _phones;

                        phones--;
                        _phones = $('#phone' + phones);

                        _phones.find('.input_ctrl').html('<span class="add add_button" title="��������"></span>');
                        (phones != 1) && _phones.find('.add').after('<span class="remove" title="�������"></span>');

                        //console.info(phones);
                        $(this).parent('.input_ctrl').parent('div').remove();
                    });

                    $('#Company_bc_type').children('input').click(function () {
                        if ($(this).val() == 'company') {
                            topics.show();
                            name_personal.hide();
                            name_company.show();
                            $('#dsc_personal').hide();
                            $('#dsc_company').show();
                            $('#description').html('�������� �������� - �������� ����������� ������������, �������, ��������� � �.�.<br />������� ����������: <a href="http://regforum.ru/companies/8/" target="_blank">����������������� �� ��� �� ��������� �������</a>, <a href="http://regforum.ru/companies/10/" target="_blank">�������������� ����� ���������</a>, <a href="http://regforum.ru/companies/37/" target="_blank">����������� ����� ��������� ������</a>');
                        }
                        else {
                            topics.hide();
                            name_company.hide();
                            name_personal.show();
                            $('#dsc_company').hide();
                            $('#dsc_personal').show();
                            $('#description').text('���������� � ����, � ����� ������� � �����������.');
                        }
                    });

                    $('#Company_upload_file, #Company_upload_file_banner').change(function () {
                        $(this).parent().find('#uploadbutton').text('���� ������');
                        $(this).parent().parent().find('#uploadfilename').text('����� �������� ���� ' + $(this).val().replace(/.+[\\\/]/, ""));
                    });

                    $('#removebanner').on('click', function () {
                        var _this = $(this);
                        $.ajax('/profile/companies/deletecompanybanner', {
                            type: "GET",
                            data: { id: _this.attr('company_id') },
                            cache: false,
                            async: false,
                            success: function (response) {
                                var answer = $.parseJSON(response);
                                if (answer.status && answer.status=='ok') {
                                    _this.parent().hide();
                                    _this.parent().parent().find('#uploadbutton').text('������� ����');
                                    _this.parent().parent().find('img').hide();
                                    _this.parent().parent().find('label').text('��������� ������');
                                }
                            }
                        });
                    });

                    $('#dontshowmap').change(function () {
                        var maparea = $('#maparea');

                        if ($(this).prop("checked")) {
                            maparea.hide('fast');
                        }
                        else
                            maparea.show('fast');
                    });

                    street_manual_data.keyup(function () {
                        street_manual = $(this).val();
                        company_address = region + ', ' + street_manual;
                        ns.setCoordinatesByAddress(company_address, 17);
                    });

                    $('#dom, #str, #korp, #street_manual_data').keyup(function () {
                        var _dom = $('#dom'),
                            _str = $('#str'),
                            _korp = $('#korp');

                        (_dom.val() != '') && (dom = ', ��� ' + _dom.val());
                        (_str.val() != '') && (str = ', �' + _str.val());
                        (_korp.val() != '') && (korp = ', �' + _korp.val());

                        var x = dom + korp + str;
                        ns.setCoordinatesByAddress(company_address + x, 17);
                    });

                    topics.on('click','.select_all input', function() {
                        if($(this).is(':checked')) {
                            topics.find('input').prop( "checked", true );
                        } else {
                            topics.find('input').prop( "checked", false );
                        }
                    });

                    $.regforum.profile.companies.editorInit("company_about, company_service_description");

                    remove_upload_file && remove_upload_file.on('click' , function() {
                        var _this = $(this),
                        parent = _this.parents('div.upload');
                        parent.find('img').hide();
                        _this.parent().hide();
                        parent.find('span#uploadbutton').text('������� ����');
                        parent.find('label').text('��������� �������:');
                        parent.find('input#remove_upload_file').val(1);
                    });
                },

                /**
                 * $.regforum.profile.companies.add.dialogHandlers
                 */
                dialogHandlers: {

                    /**
                     * Init UI dialog
                     * @param dialogElement
                     * @param title
                     * @param options
                     * @param buttons
                     * @private
                     */
                    _dialogInit: function (dialogElement, title, buttons, options) {

                        buttons = !!buttons ? buttons : {};
                        options = options || $.regforum.profile.companies.add._dialogOptions;

                        !dialogElement || dialogElement.dialog($.extend({
                            title: title,
                            buttons: buttons
                        }, options));

                    },

                    /**
                     * Buttons for dialogs
                     */
                    _buttons: {
                        locality: {},
                        street: {},
                        taxOffice: {}
                    },

                    /**
                     * $.regforum.profile.companies.add.dialogHandlers.regionDialogInit
                     */
                    regionDialogInit: function (regionDialog, regionField, regionIdField, localityField, localityIdField, streetField, streetIdField, fn) {

                        this._dialogInit(regionDialog, '����� ��������');

                        streetField = streetField || {};
                        streetIdField = streetIdField || {};

                        regionField.length && regionField.on('focus', function () {

                            $("div.top_filter").addClass('dlg_loading');

                            if (!regionDialog.dialog('option', 'disabled')) {

                                //�������� ���� ������
                                $.getJSON('/companies/company/getregions/',
                                    function (obj) {
                                        if (obj) {
                                            //noinspection JSUnresolvedVariable
                                            $("#region_dialog_content").html(obj.region_content);
                                            $("div.top_filter").removeClass('dlg_loading');
                                            regionField.blur();
                                        }
                                    });
                                regionDialog.dialog("open");
                            }

                            /* ����� */
                            $(".dialog_filter").live('keyup', function () {

                                var fastSearch = $(this).val();

                                $("div.top_filter").addClass('dlg_loading');
                                $.getJSON('/companies/company/getregions/', { q: fastSearch},
                                    function (obj) {
                                        if (obj) {
                                            //noinspection JSUnresolvedVariable
                                            $("#region_dialog_content").html(obj.region_content);
                                            $("div.top_filter").removeClass('dlg_loading');
                                        }
                                    });
                            });

                            /* ������� ��� ������ */
                            $('div #region_dialog_content p').live('click', function () {

                                var dataId = $(this).attr("id"),
                                    region = $(this).text();

                                $('#' + regionField.attr('id')).val(region);
                                $('#' + regionIdField.attr('id')).val(dataId);
                                $('div[data-depends="' + regionIdField.attr('id') + '"]').removeClass('d-n');

                                localityField.length && localityField.val('');
                                localityIdField.length && localityIdField.val('');

                                streetField.length && streetField.val('');
                                streetIdField.length && streetIdField.val('');

                                //$('#region_result').html('�� ������� ' + dataId);
                                //$('#city_result').html('� ����������� �� ���������� �������');

                                regionDialog.dialog("close");

                                $(".dialog_filter").val('');

                                $("div #region_dialog_content p").die("click");

                                !fn || fn.call({});
                            });

                        });

                    },

                    /**
                     * $.regforum.profile.companies.add.dialogHandlers.localityDialogInit
                     */
                    localityDialogInit: function (localityDialog, localityField, localityIdField, regionIdField, hintField, fn) {

                        var it = this,
                            regionId;
                        it._dialogInit(localityDialog, '����� ����������� ������', it._buttons.locality);

                        localityField.length && localityField.on('focus', function () {

                            $("div.top_filter").addClass('dlg_loading');

                            regionId = $('#' + regionIdField.attr('id')).val();

                            if (!regionId) {
                                !hintField || hintField.text('������� ���������� ������� �������');
                                return;
                            }

                            if (!localityDialog.dialog('option', 'disabled')) {

                                //�������� ���� ������
                                $.getJSON('/profile/companies/getpopulationaggregate/', {region: regionId},
                                    function (obj) {
                                        if (obj) {
                                            $("#pa_dialog_content").html(obj.pa_content);
                                            $("div.top_filter").removeClass('dlg_loading');
                                        }
                                        localityField.blur();
                                    });
                                localityDialog.dialog("open");
                            }

                            // �����
                            $('.dialog_filter').live('keyup', function () {

                                var fastSearch = $(this).val();

                                $('div.top_filter').addClass('dlg_loading');
                                $.getJSON('/profile/companies/getpopulationaggregate/', { region: regionId, q: fastSearch},
                                    function (obj) {
                                        if (obj) {
                                            //noinspection JSUnresolvedVariable
                                            $("#pa_dialog_content").html(obj.pa_content);
                                            $("div.top_filter").removeClass('dlg_loading');
                                        }
                                    });
                            });

                            // ������� ��� ������
                            $('div #pa_dialog_content p').live('click', function () {

                                var gnimnb = $(this).data('gnimnb'),
                                    dataId = $(this).attr('id'),
                                    pa = $(this).text();

                                localityField.val(pa);
                                localityIdField.val(dataId);
                                if (!!gnimnb && gnimnb.toString().length) {
                                    localityIdField.data('gnimnb', gnimnb);
                                }
                                localityDialog.dialog("close");

                                $(".dialog_filter").val('');
                                $("div #pa_dialog_content p").die("click");

                                !fn || fn.call({});
                            });

                        });

                    },

                    /**
                     * $.regforum.profile.companies.add.dialogHandlers.streetDialogInit
                     */
                    streetDialogInit: function (streetDialog, streetField, streetIdField, regionField, regionIdField, localityField, localityIdField, hintField, fn) {

                        var it = this;
                        it._dialogInit(streetDialog, '����� �����',  it._buttons.street);

                        streetField.length && streetField.on('focus',function () {

                            var regionId = $('#' + regionIdField.attr('id')).val(),
                                localityId = $('#' + localityIdField.attr('id')).val();

                            if (!regionId) {
                                !hintField || hintField.text('������� ���������� ������� �������');
                                return;
                            }

                            $('div.top_filter').addClass('dlg_loading');
                            if (!streetDialog.dialog('option', 'disabled')) {

                                //�������� ���� ������
                                $.getJSON('/profile/companies/getstreet/', {region: regionId, population_aggregate: localityId},
                                    function (obj) {
                                        if (obj) {
                                            //noinspection JSUnresolvedVariable
                                            $('#street_dialog_content').html(obj.s_content);
                                            $('div.top_filter').removeClass('dlg_loading');
                                            streetField.blur();
                                        }
                                    });
                                streetDialog.dialog("open");
                            }

                            //�����
                            $(".dialog_filter").live('keyup', function () {

                                var fastSearch = $(this).val();
                                $("div.top_filter").addClass('dlg_loading');
                                $.getJSON('/profile/companies/getstreet/', { region: regionId, population_aggregate: localityId, q: fastSearch},
                                    function (obj) {
                                        if (obj) {
                                            //noinspection JSUnresolvedVariable
                                            $("#street_dialog_content").html(obj.s_content);
                                            $("div.top_filter").removeClass('dlg_loading');
                                        }
                                    });
                            });

                            //������� ��� ������
                            $('div #street_dialog_content p').live('click', function () {

                                var dataId = $(this).attr('id'),
                                    street = $(this).text();

                                streetField.val(street);
                                streetIdField.val(dataId);
                                $('#street_manual').hide('fast');

                                streetDialog.dialog('close');
                                $('.dialog_filter').val('');

                                $('div #street_dialog_content p').die('click');

                                !fn || fn.call({});
                            });

                        });

                    },

                    /**
                     * $.regforum.profile.companies.add.dialogHandlers.taxOfficeDialogInit
                     */
                    taxOfficeDialogInit: function (taxOfficeDialog, taxOfficeField, taxOfficeIdField, regionIdField, hintField, fn) {

                        var it = this;
                        it._dialogInit(taxOfficeDialog, '����� ����', it._buttons.taxOffice, $.extend({}, $.regforum.profile.companies.add._dialogOptions, { width: 700}));

                        taxOfficeField.length && taxOfficeField.on('focus', function () {

                            var regionId = $('#' + regionIdField.attr('id')).val();

                            if (!regionId) {
                                !hintField || hintField.text('������� ���������� ������� �������');
                                return;
                            }

                            $('div.top_filter').addClass('dlg_loading');

                            if (!taxOfficeDialog.dialog('option', 'disabled')) {

                                //�������� ���� ������
                                $.getJSON('/kladr/gettaxofficesbyregioncode/' + regionId,
                                    function (obj) {
                                        var _container = $($.regforum.ce('div'));
                                        if (obj && obj.length) {

                                            $.each(obj, function () {
                                                _container.append($($.regforum.ce('p')).attr('data-gnimnb', this.KOD).text(this.NAIMK));
                                            });

                                            //noinspection JSUnresolvedVariable
                                            $('#tax_office_dialog_content').html(_container.html());
                                            $('div.top_filter').removeClass('dlg_loading');

                                        }
                                        else {
                                            _container.html('�� ��������� �������� ��� ������');
                                        }
                                        taxOfficeField.blur();
                                    });
                                taxOfficeDialog.dialog("open");
                            }

                            //������� ��� ������
                            $('div #tax_office_dialog_content p').live('click', function () {

                                var dataId = $(this).data('gnimnb'),
                                    taxOffice = $(this).text();

                                taxOfficeField.val(taxOffice);
                                taxOfficeIdField.val(dataId);

                                taxOfficeDialog.dialog('close');

                                $('div #tax_office_dialog_content p').die('click');

                                !fn || fn.call({});
                            });

                        });
                    },

                    /**
                     * $.regforum.profile.companies.add.dialogHandlers.metroStationDialogInit
                     */
                    metroStationDialogInit: function (metroStationDialog, metroStationField, metroStationIdField, regionIdField, hintField, fn) {
                        var it = this,
                            dialogFilter;
                        it._dialogInit(metroStationDialog, '����� ������� �����');

                        metroStationField.length && metroStationField.on('focus', function () {

                            var regionId = $('#' + regionIdField.attr('id')).val();

                            dialogFilter = metroStationDialog.find('.dialog_filter');

                            if (!regionId) {
                                !hintField || hintField.text('������� ���������� ������� �������');
                                return;
                            }

                            $('div.top_filter').addClass('dlg_loading');

                            if (!metroStationDialog.dialog('option', 'disabled')) {

                                //�������� ���� ������
                                $.getJSON('/profile/default/metrostations/region/' + regionId,
                                    function (obj) {
                                        var _container = $($.regforum.ce('div')),
                                            _stations;
                                        if (obj && obj.success) {

                                            _stations = obj.stations;

                                            $.each(_stations, function () {
                                                _container.append($($.regforum.ce('p')).css({borderLeft: '3px solid ' + this.color}).attr('data-station', this.id).text(this.title));
                                            });

                                            //noinspection JSUnresolvedVariable
                                            $('#metro_station_dialog_content').html(_container.html());
                                            $('div.top_filter').removeClass('dlg_loading');

                                        }
                                        else {
                                            _container.html('�� ��������� �������� ��� ������');
                                        }
                                        metroStationDialog.blur();
                                    });
                                metroStationDialog.dialog('open');
                            }

                            //�����
                            dialogFilter.live('keyup', function () {

                                var dialogFilterField = $(this).val().toLowerCase();
                                $('#metro_station_dialog_content').find('p').show().filter(function () {
                                    return !($(this).text().toLowerCase().indexOf(dialogFilterField) == 0)
                                }).hide();

                            });

                            //������� ��� ������
                            $('div #metro_station_dialog_content p').live('click', function () {

                                var dataId = $(this).data('station'),
                                    station = $(this).text();

                                metroStationField.val(station);
                                metroStationIdField.val(dataId);

                                dialogFilter.val('');
                                metroStationDialog.dialog('close');

                                $('div #metro_station_dialog_content p').die('click');

                                !fn || fn.call({});
                            });

                        });
                    }
                }
            },

            /**
             * $.regforum.profile.companies.addresses
             */
            addresses: {

                /**
                 * $.regforum.profile.companies.addresses.init
                 */
                init: function (modelName, fn) {

                    var it = this,
                        prefix = '#' + modelName + '_',
                        mapNs = $.regforum.profile.companies.map,
                        uploadNs = $.regforum.upload,
                        companyNs = $.regforum.profile.companies,
                        timeout_input,
                        mapArea = $('#maparea'),
                        mapReady, //������� �������������, ��� ����� ����������������
                        reg = $(prefix + 'region'),
                        mas = $(prefix + 'address_short'),
                        maf = $(prefix + 'address_full'),
                        pointField = $(prefix + 'map_point');

                    /**
                     * init tinyMCE
                     */
                    companyNs.editorInit(modelName + '_description');

                    /**
                     * init YMaps
                     */
                    function initYMaps() {
                        try{
                            ymaps.ready(function () {
                                var point;
                                try {
                                    point = JSON.parse(pointField.val());
                                    if ((point instanceof Array) && point.length == 2) {
                                        mapNs._point = point;
                                    }
                                }
                                catch(e) {}

                                mapNs.init(function () {

                                    var pm;

                                    if (!!point) {
                                        mapNs.getMap().setZoom(17);
                                    }

                                    pm = mapNs.getPlaceMark();
                                    mapNs.getPlaceMark().events.add('dragend', function (e) {

                                        mapNs._point = mapNs.getPlaceMark().geometry.getCoordinates();
                                        pointField.val(JSON.stringify(mapNs._point));

                                    });

                                });
                                mapReady = true;
                            });
                        }
                        catch (e) {
                            console.log('YMaps �� ���������������');
                            mapReady = false;
                        }
                    }

                    //�������������� ����� ������ ���� ������� �����
                    if($('input[name*="map_visible"]:checkbox').is(':checked')) {
                        initYMaps();
                    }
                    /**
                     * toggle map
                     */
                    $('input[name*="map_visible"]:checkbox').on('click', function () {
                        $(this).prop('checked') ? mapArea.show() : mapArea.hide();
                        if(!mapReady){ //���� ����� �� ���������������� ���� �������� ��
                            var ma = maf.val() ? maf.val() : mas.val(),
                                region = reg.val(),
                                address = region + ", " + ma;
                            initYMaps();
                            if(mapReady) {
                                mapNs.setCoordinatesByAddress(address, 17);
                            }
                        }
                    });

                    //���� �� ������� ��� ����� �������.
                    $.merge(maf, mas).on('keyup change', function() {
                        var ma = maf.val() ? maf.val() : mas.val(),
                            region = reg.val(),
                            address = region + ", " + ma;
                        if(!ma || !region || !mapReady) return;
                        clearTimeout(timeout_input);
                        timeout_input = setTimeout(function() {
                            mapNs.setCoordinatesByAddress(address, 17);
                            pointField.val(JSON.stringify(mapNs._point));
                        }, 500);
                    });

                    it.formHandlers(modelName);

                    uploadNs.modelName = uploadNs.modelName || modelName;
                    uploadNs.handleUploaderControls();
                    uploadNs.handlePhotosField();

                    !fn || fn.call({});

                },

                /**
                 * $.regforum.profile.companies.addresses.formHandlers
                 */
                formHandlers: function (modelName) {
                    var it = this,
                        mapNs = $.regforum.profile.companies.map,
                        idPfx = '#' + modelName + '_',
                        _dialogHandlers = $.regforum.profile.companies.add.dialogHandlers,
                        regionField = $(idPfx + 'region'),
                        regionIdField = $(idPfx + 'region_code'),
                        regionDialog = $('#select_region_dialog'),
                        addressShortField = $(idPfx + 'address_short'), // ��� ������� ���������� �� �������
                        addressFullField = $(idPfx + 'address_full'),
                        localityField = $(idPfx + 'locality'),
                        localityIdField = $(idPfx + 'locality_code'),
                        localityDialog = $('#select_pa_dialog'),
                        taxOfficeDialog = $('#tax_office_dialog'),
                        taxOfficeField = $(idPfx + 'tax_office'),
                        taxOfficeIdField = $(idPfx + 'tax_office_id'),
                        metroStationDialog = $('#metro_station_dialog'),
                        metroStationField = $('#_metro_station'),
                        metroStationIdField = $(idPfx + 'metro_station'),
                        mapPoint = $(idPfx + 'map_point'),
                        addressType = $(idPfx + 'type'),
                        contractTypeContent = $(idPfx + 'contract_type').parents('.address_col'),
                        postIncludedField = $(idPfx + 'post_included'),
                        pricePostField = $(idPfx + 'price_post'),
                        postDurationMinLabel = $('label[for="' + modelName +'_post_duration_min"]'),
                        _labelData;

                    _labelData = {
                        'default': postDurationMinLabel.text(),
                        alt: postDurationMinLabel.data('alt-label')
                    };

                    addressType.find('input').on('click', function () {
                        if (this.value == 'sale') {
                            contractTypeContent.hide().find(':checkbox:checked').click();
                        }
                        else {
                            contractTypeContent.is(':visible') || contractTypeContent.show();
                        }
                    });

                    addressType.find('input:checked').click();

                    postIncludedField.on('click', function () {
                        pricePostField.toggleDisabled().parents('.elements').toggle();
                        postDurationMinLabel.html(
                            postDurationMinLabel.html() != _labelData['default'] ? _labelData['default'] : _labelData.alt
                        );
                    });

                    if (postIncludedField.is(':checked')) {
                        pricePostField.attr('disabled', 'disabled').val('');
                        pricePostField.parents('.elements').hide();
                        postDurationMinLabel.html(_labelData.alt);
                    }

                    _dialogHandlers._buttons.locality = {
                        '�� ���������' : function () {
                            localityField.val('');
                            localityIdField.val('');
                            $(this).dialog('close');
                        }
                    };

                    _dialogHandlers.regionDialogInit(
                        regionDialog,
                        regionField,
                        regionIdField,
                        localityField,
                        localityIdField,
                        {},
                        {},
                        function () {
                            taxOfficeIdField.val('');
                            taxOfficeField.val('');
                            metroStationField.val('');
                            metroStationIdField.val('');
                            addressShortField.val('');
                            addressFullField.val('');
                            mapNs.setCoordinatesByAddress(
                                $('#' + regionField.attr('id')).val(),
                                9,
                                function () {
                                    mapPoint.val(JSON.stringify(mapNs._point));
                                });

                            $.post('/profile/default/metrostations/region/' + $('#' + regionIdField.attr('id')).val() +'/exists/1/', function (data) {
                                var metroGroupCtrl = metroStationIdField.parents('.group_ctrl');
                                (!!data.success && !!data.metro) ? metroGroupCtrl.removeClass('d-n') : metroGroupCtrl.addClass('d-n');
                            });
                        }
                    );

                    _dialogHandlers.localityDialogInit(
                        localityDialog,
                        localityField,
                        localityIdField,
                        regionIdField,
                        null,
                        function () {
                            mapNs.setCoordinatesByAddress(
                                $('#' + regionField.attr('id')).val() + ', ' + $('#' + localityField.attr('id')).val(),
                                11,
                                function () {
                                    mapPoint.val(JSON.stringify(mapNs._point));
                                });
                        }
                    );

                    _dialogHandlers.taxOfficeDialogInit(
                        taxOfficeDialog,
                        taxOfficeField,
                        taxOfficeIdField,
                        regionIdField
                    );

                    _dialogHandlers.metroStationDialogInit(
                        metroStationDialog,
                        metroStationField,
                        metroStationIdField,
                        regionIdField
                    );
                },

                /**
                 * $.regforum.profile.companies.addresses.list
                 */
                list: function () {
                    var switcher = $('.switcher li+li'),
                        listView = $('.list-view');

                    switcher.on('click', function () {

                        switcher.removeClass('selected');
                        $(this).addClass('selected');

                        if (this.getAttribute('data-mode') == 'list') {
                            listView.addClass('list');
                        }
                        else {
                            listView.removeClass('list');
                        }
                    });
                }
            },


            /**
             * $.regforum.profile.companies.services
             */
            services: {
                init: function(){
                    var categories,
                        subcategories,
                        drafts,
                        eyes;

                    categories = $('#_MSC_categoryId');
                    subcategories = $('#_MSC_subCategoryId');

                    drafts = $('.service-status span[data-status="draft"]');
                    eyes = $('.service-visible i');

                    drafts.addClass('pointer').attr('title', '������������').on('click', function(){
                        var self;
                        if(confirm('������������ ������?')){
                            self = $(this);
                            $.post('/profile/services/status/', { 'id' : self.data('id')}, function(response){
                                if(response.success){
                                    self.off('click')
                                        .attr('class', 'sign sign_green')
                                        .removeAttr('title').html('������������');
                                }
                            });
                        }
                    });

                    if(eyes.length){
                        eyes.on('click', function(){
                            var self = $(this);
                            $.post('/profile/services/visible/', { 'id' : self.data('id')}, function(response){
                                if(response.success){
                                    self.toggleClass('closed', !response.visible).attr('title', response.visible ? '������ ������' : '�������� ������');
                                }
                            });
                        });
                    }
                }
            },
            /**
             * $.regforum.profile.companies.proposeCategory
             */
            proposeCategory : function(parent) {
                var dialog = $('#categoryPropose');
                dialog.dialog({
                    modal: true,
                    autoOpen: true,
                    closeOnEscape: true,
                    resizable: false,
                    title: "���������� ���������",
                    height: 300,
                    width: 450,
                    zIndex: $.regforum.settings().zi.dialog,
                    buttons: {
                        "���������": function () {
                            var description = dialog.find('#description'),
                                fm = $('#flashMessage');

                            if(!description.val()) {
                                $(this).dialog("close");
                                return;
                            }

                            $.ajax('/market/default/categorypropose/', {
                                type: "POST",
                                data: { description: description.val(), parent: parent | null },
                                cache: false,
                                success: function (result) {
                                    description.val('');
                                    fm.addClass('sign_green').removeClass('sign_ruby').html('���� ����������� ������� �������� �������������!');
                                    fm.show().delay(1800).fadeOut(200);
                                },
                                error: function() {
                                    fm.addClass('sign_ruby').removeClass('sign_green').html('��������, �������� �������������� ������!');
                                    fm.show().delay(1800).fadeOut(200);
                                }
                            });
                            $(this).dialog("close");
                        },
                        "������": function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }

        },

        /**
         * $.regforum.profile.files
         */
        files: {

            /**
             * $.regforum.profile.files.init
             */
            init: function () {

            },

            /**
             * $.regforum.profile.files.add
             */
            add: {

                /**
                 * $.regforum.profile.files.add.init
                 */
                init: function () { // todo: refactor
                    $('#Files_upload_file, #upload_image').change(function () {
                        var _this = $(this),
                            parent = _this.parents('.upload'),
                            fileName = _this.val().replace(/.+[\\\/]/, ""),
                            fileArr = fileName.split('.');
                        fileArr.pop();

                        if (fileName) {
                            parent.find('#uploadbutton').text('���� ������');
                            parent.find('#uploadfilename').text('����� �������� ���� '+fileName);
                            parent.find('#Files_file_title').val() ? '' : $('#Files_file_title').val(fileArr.join('.'));
                        } else {
                            parent.find('#uploadbutton').text('�������� ����');
                            parent.find('#uploadfilename').text('');
                            parent.find('.errorMessage').text('');
                            $('#Files_file_title').val('');
                        }
                    });

                    $('#FilesUpdate_upload_file').change(function () {
                        $('#uploadbutton').text('���� ������');
                        $('#uploadfilename').text('����� �������� ���� '+$(this).val().replace(/.+[\\\/]/, ""));
                    });

                    $('.arrow_down').on('click', function () {
                        $(this).siblings('.razdel').removeClass('collapse');
                        $(this).css('display', 'none');
                    });

                    $(document).click(function (e) {
                        if (!$(e.target).closest('.razdels').length) {
                            $('.razdels > .razdel').addClass('collapse');
                            $('span.arrow_down').css('display', 'inline-block');
                        }
                    });

                    $('.razdels span.plus').on('click', function () {
                        var _this = $(this);
                        if (_this.hasClass('plus')) {
                            _this.siblings('.razdel').removeClass('collapse');
                            _this.parent().siblings().find('.razdel:not(.collapse)').addClass('collapse')
                                .parent().find('span:not(.open)').removeClass().addClass('plus');
                        } else {
                            _this.siblings('.razdel').addClass('collapse');
                        }
                        _this.toggleClass("minus").toggleClass("plus");
                    });

                    $('.razdels a').on('click', function () {
                        $('.razdels > .razdel').addClass('collapse');
                        $('span.select_razdel').text($(this).text()).show();
                        $('span.arrow_down').css('display', 'inline-block');
                        $('#Files_razdel_id').val($(this).attr('id'));
                        $('#FilesUpdate_razdel_id').val($(this).attr('id'));
                        return false;
                    });
                    $.regforum.helpers.tinyMCE("Files_file_description");
                }
            }
        },

        /**
         * $.regforum.profile.subscribe
         */
        subscribe: {

            /**
             * $.regforum.profile.subscribe.init
             */
            init: function () {
                var postsField = $('#_subscribe_type_posts'),
                    authorField = $('#_subscribe_type_author'),
                    categoryField = $('#_subscribe_type_category'),
                    addressesField = $('#_subscribe_type_addresses'),
                    allServicesField = $('#_subscribe_type_all_services'),
                    servicesByRegionField = $('#_subscribe_type_services_by_regions'),
                    periodFields = $(":radio[name='subscribe_period']"),
                    subscribePostsField = $('#_subscribe_posts'),
                    subscribeMarketField = $('#_subscribe_market'),
                    adminEmail = $('#adminEmail'),
                    userDialog = $('#user_dialog'),
                    authorsContainer = $('.authors-container'),
                    authorsList = authorsContainer.find('ul'),
                    addUserButton = authorsContainer.find('button'),
                    subscribePostsIsChecked,
                    subscribeMarketIsChecked,
                    chosenSelect,
                    url = '/profile/subscribe/update/',
                    notification,
                    selectHandler;


                chosenSelect = $('.chosen-select');
                chosenSelect.chosen({'width': '50%'});

                /* Posts section */
                subscribePostsIsChecked = function () {
                    var subsections = subscribePostsField.parents('.elements').nextAll();
                    $('#' + subscribePostsField.attr('id')).is(':checked') ? subsections.show() : subsections.hide();
                };

                /* Market section */
                subscribeMarketIsChecked = function () {
                    var subsections = subscribeMarketField.parents('.elements').nextAll();
                    $('#' + subscribeMarketField.attr('id')).is(':checked') ? subsections.show() : subsections.hide();
                };

                notification = function (success, show) {
                    var fm = $('#flashMessage');

                    success = (typeof success == 'undefined') ? true : success;
                    show = (typeof show == 'undefined') ? true : show;

                    if (!success) {
                        fm.addClass('sign_ruby').removeClass('sign_green').html('��������� �� ���������');
                    }
                    else {
                        fm.addClass('sign_green').removeClass('sign_ruby').html('��������� ���������');
                    }

                    show && fm.show().delay(800).fadeOut(200);
                };

                if (userDialog.length) {
                    userDialog.dialog({
                        modal: true,
                        autoOpen: false,
                        closeOnEscape: true,
                        resizable: false,
                        title: '������ �� ���������',
                        height: 500,
                        width: 320,
                        zIndex: $.regforum.settings().zi.dialog,
                        buttons: {
                            '�������': function () {
                                $(this).dialog('close');
                            }
                        }
                    });

                    addUserButton.on('click', function () {
                        $.regforum.helpers.autocomplete.findUsers(userDialog, function () {

                            $.post(url, {'type': 'author', 'action': true, 'item_id': this.id}, function (response) {
                                if (response.success) {
                                    authorsList.append(response.author);
                                }
                                notification(response.success);
                            });
                        });
                    });
                }

                selectHandler = function () {
                    chosenSelect.on('change.options', function () {
                        var type,
                            values = $(this).val(),
                            category = $(this).parent().parent().find(categoryField),
                            service = $(this).parent().parent().find(servicesByRegionField);
                        if(category.length) {
                            type = 'category';
                            category.prop('checked', $.isArray(values))
                        }
                        else if(service.length) {
                            type = 'services';
                            service.prop('checked', $.isArray(values))
                        }

                        $.post(url, {'type': type, 'values': values}, function (response) {
                            notification(response.success, $.isArray(values));
                        });
                    });
                };

                subscribePostsIsChecked();
                subscribeMarketIsChecked();
                selectHandler();

                subscribePostsField.on('click', function () {

                    if (!$(this).prop('checked')) {
                        $.post(url, {'subscribe': {'posts': false }}, function (response) {
                            !response.success || document.location.reload();
                        });
                    }
                    subscribePostsIsChecked();
                });

                subscribeMarketField.on('click', function(){

                    if (!$(this).prop('checked')) {
                        $.post(url, {'subscribe': {'market': false }}, function (response) {
                            !response.success || document.location.reload();
                        });
                    }
                    subscribeMarketIsChecked();

                });

                periodFields.on('click', function () {
                    $.post(url, {'period': (this.value | 0)}, function (response) {
                        notification(response.success);
                    });
                });

                addressesField.on('click', function(){
                    var value = $(this).prop('checked');

                    $.post(url, {'type': 'addresses', 'action': value}, function (response) {

                        if (response.success) {
                            notification(response.success);
                        }
                    });

                });

                postsField.on('click', function () {

                    var _this = $(this),
                        relative = $(this).parent().parent(),
                        value = _this.prop('checked');

                    relative.find(chosenSelect).off('change.options');
                    relative.find('.search-choice-close').click();
                    relative.find('.chosen-choices input').blur();
                    selectHandler();

                    $.post(url, {'type': 'posts', 'action': value}, function (response) {

                        if (response.success) {

                            if (value) {

                                authorField.prop('checked', false);
                                categoryField.prop('checked', false);

                                relative.find('.chosen-container').hide();
                                relative.find('.authors-container').hide().find('i.remove').parent().remove();

                            }
                            notification(response.success);
                        }
                    });
                });

                allServicesField.on('click', function () {

                    var _this = $(this),
                        relative = $(this).parent().parent(),
                        value = _this.prop('checked');

                    relative.find(chosenSelect).off('change.options');
                    relative.find('.search-choice-close').click();
                    relative.find('.chosen-choices input').blur();
                    selectHandler();

                    $.post(url, {'type': 'services', 'action': value}, function (response) {

                        if (response.success) {

                            if (value) {

                                servicesByRegionField.prop('checked', false);

                                relative.find('.chosen-container').hide();
                                relative.find('.authors-container').hide().find('i.remove').parent().remove();

                            }
                            notification(response.success);
                        }
                    });
                });

                servicesByRegionField.on('click', function () { //TODO ������������ � categoryField

                    var _this = $(this),
                        value = _this.prop('checked'),
                        relative = _this.parent().parent();

                    if (value) {
                        allServicesField.prop('checked', false);
                        relative.find(chosenSelect).change();
                        relative.find('.chosen-container').parent().andSelf().show();
                    }
                    else {
                        (function () {
                            _this.parent().find('.search-choice-close').click();
                        })();
                    }
                });

                authorField.on('click', function () {

                    var value = $(this).prop('checked');

                    if (value) {
                        postsField.attr('checked', false);
                        $.post(url, {'type': 'author'}, function (response) {
                            response.success || notification(response.success);
                        });

                        authorsContainer.show();
                    }
                    else {
                        authorsContainer.hide();
                    }

                });

                $('.authors-container i.remove').live('click', function () {
                    var item = this;
                    $.post(url,
                        {'type': 'author', 'action': false, 'item_id': $(item).data('user-id')},
                        function (response) {
                            if (response.success) {
                                $(item).parents('li').remove();
                            }
                            notification(response.success);
                        });
                });

                categoryField.on('click', function () {

                    var _this = $(this),
                        value = _this.prop('checked'),
                        relative = _this.parent().parent();

                    if (value) {
                        postsField.prop('checked', false);
                        relative.find(chosenSelect).change();
                        relative.find('.chosen-container').parent().andSelf().show();
                    }
                    else {
                        (function () {
                            _this.parent().find('.search-choice-close').click();
                        })();
                    }
                });

                adminEmail.on('click', function () {
                    var checked = $(this).prop('checked') ? 1 : 0;
                    $.post(url, {'adminEmail': checked }, function (response) {
                        notification(response.success);
                    });
                });
            }

        },
        /**
         * $.regforum.profile.unsubscribe
         */
        unsubscribe: {

            /**
             * $.regforum.profile.unsubscribe.init
             */
            init: function () {
                var subscribeFields = $('form#profile-form input.checkbox'),
                    unsubscribeReason = $('#unsubscribe_reason'),
                    unsubscribeFields = unsubscribeReason.find('input:checkbox'),
                    unsubscribeText = unsubscribeReason.find('textarea'),
                    submitBtn = $('form#profile-form input.button');
                subscribeFields.on('click', function () {
                    if ($(this).prop('checked')) {
                        unsubscribeReason.show();
                    } else {
                        if (!($('form#profile-form input.checkbox:checked').length)) {
                            unsubscribeReason.hide();
                        }
                    }
                });
                submitBtn.on('click', function () {
                    if (!($('#unsubscribe_reason').find('input:checked').length) && !(unsubscribeText.val())) {
                        unsubscribeReason.find('.error').show();
                        return false;
                    } else {
                        unsubscribeReason.find('.error').hide();
                    }
                });
                unsubscribeFields.on('click', function () {
                    unsubscribeReason.find('.error').hide();
                })
            }
        },

        /**
         * $.regforum.profile.favourites
         */
        favourites: {

            /**
             * $.regforum.profile.favourites.editfavourite
             */
            editfavourite: function (url, fn) {

                var url = '/profile/editfavourite/' + url,
                    block = $('.myfavourites'),
                    inputBlock = 'div.editBlock',
                    nameBlock = '.title',
                    a = 'a',
                    edit = '.edit',
                    submit = '.submit',
                    error = '.error';

                block.on('click', edit, function () {
                    var _this = $(this);
                    _this.parent().siblings(inputBlock).show();
                    _this.siblings(a).andSelf().hide();
                });

                block.on('click', submit, function () {
                    var _this = $(this),
                        relative = _this.parent(),
                        input = relative.find('input.title'),
                        title = input.val(),
                        id = input.attr('data-id');

                    if (!title) return;

                    $.ajax(url, {
                        type: 'GET',
                        data: { title: title, id: id },
                        cache: false,
                        success: function (response) {
                            if (response.success) {
                                //relative.find(error).hide();
                            }
                            if (response.error) {
                                //relative.find(error).text(response.error).show();
                            }
                            var sibling = relative.siblings(nameBlock);
                            relative.hide();
                            sibling.find(a).text(title).show();
                            sibling.find(edit).show();
                        }
                    });
                });

                $(document).click(function (e) {
                    var target = $(e.target),
                        editBtn = block.find(edit);
                    if (!target.closest(editBtn).length && !target.closest(inputBlock).length) {
                        $(inputBlock).hide();
                        block.find(nameBlock + ' ' + a).show();
                        editBtn.show();
                    }
                });

                !fn || fn.call({});
            },

            /**
             * $.regforum.profile.favourites.threads
             */
            threads : {

                /**
                 * $.regforum.profile.favourites.threads.init
                 * @param fn
                 */
                init: function (fn) {
                    $.regforum.profile.favourites.editfavourite('thread');

                    !fn || fn.call({});
                }
            },

            /**
             * $.regforum.profile.favourites.posts
             */
            posts : {

                /**
                 * $.regforum.profile.favourites.posts.init
                 * @param fn
                 */
                init: function (fn) {
                    $.regforum.profile.favourites.editfavourite('post');

                    !fn || fn.call({});
                }
            }
        },

        /**
         * $.regforum.profile.init
         * @param fn
         */
        init: function (fn) {
            var url = '/profile/default/changeprofilename/',
                selector = $('.reject'),
                editBtn = $('h1.name .edit'),
                saveBtn = $('.editBlock .submit'),
                name = $('h1.name'),
                inputBlock = $('div.editBlock');
            selector.poshytip({
                className: 'tip-yellowsimple',
                showOn: 'none',
                showTimeout: 200,
                alignTo: 'target',
                alignX: 'center',
                alignY: 'bottom',
                offsetY: 7,
                liveEvents: true,
                allowTipHover: false
            });

            selector.click(function() { selector.poshytip('show'); });

            editBtn.on('click', function() {
                inputBlock.show();
                name.hide();
            });

            saveBtn.on('click', function() {
                var first_name = inputBlock.find('input#first_name').val(),
                    last_name = inputBlock.find('input#last_name').val();
                $.ajax(url, {
                    type: 'POST',
                    data: { first_name: first_name, last_name: last_name },
                    cache: false,
                    success: function(response){
                        var answer = $.parseJSON(response);
                        if(answer.name) {
                            name.find('.profile_name').text(answer.name);
                            inputBlock.hide();
                            name.show();
                            inputBlock.find('.error').hide();
                        }
                        if(answer.error){
                            inputBlock.find('.error').text(answer.error).show();
                        }
                    }
                });
            });

            $(document).click(function(e) {
                if(!$(e.target).closest('.tip-yellowsimple').length && !$(e.target).closest(selector).length) {
                    selector.poshytip('hide');
                }
                if(!$(e.target).closest(editBtn).length && !$(e.target).closest(inputBlock).length) {
                    inputBlock.hide();
                    name.show();
                }
            });
            !fn || fn.call({});
        }
    },

    /**
     * $.regforum.members
     */
    members: {
        member: function () {
            var photos,
                photo,
                gallery,
                tabs,
                _tabs = '.member-activity .tabs',
                boxBottom = $('.box-bottom'),
                callback;

            tabs = $(_tabs);

            if (tabs.length) {

                tabs.find('dt:first').next().andSelf().addClass('selected');

                callback = function () {
                    var p = $(_tabs).find('dd.selected p');
                    !p.length ? boxBottom.hide() : boxBottom.show();
                };
                callback();

                $.regforum.upload.handleUploaderControls(callback);
            }

            photos = $('#member').find('.photos');
            photo = photos.find('.profile-photo');
            gallery = photos.find('.gallery img');

            if (photo.length && gallery.length) {
                gallery.on('click',function () {
                    photo.attr('src', this.src);
                    $(this).css('cursor', 'default');
                }).on('mouseover', function () {
                        $(this).css('cursor', (this.src != photo[0].src) ? 'pointer' : 'default');
                    });
            }
        },
        init: function () {
            //������ ����� ��� �������� ��������
            (function () {
                var sortObject = getSortObject(),
                    search = window.location.search.match(/page=(\d*)/i),
                    page = search && search[1] !='undefined' ? search[1] : 0;
                getAjaxMembersData(null, null, sortObject.sortName, sortObject.sortOrder, page);
            })();
            //������� �� ����� �� ����������� ����
            $('.member-activity thead tr th').on('click', function () {
                var el = $(this);
                changeSortClass(el);
                var page = getPageNum(),
                    sortObject = getSortObject(),
                    searchObject = getSearchObject();
                getAjaxMembersData(searchObject.name, searchObject.company, sortObject.sortName, sortObject.sortOrder, page);
            });
            //������� �� ����� �� ���������
            $('.pagination a').live('click', function () {
                var page = $(this).attr('id'),
                    sortObject = getSortObject(),
                    searchObject = getSearchObject();
                getAjaxMembersData(searchObject.name, searchObject.company, sortObject.sortName, sortObject.sortOrder, page);
                history.pushState({}, '', '?page=' + page);
                return false;
            });
            //������� ��� ������
            $('.member-search #member_name, .member-search #company_name').on('keypress', function (e) {
                var c = e.which ? e.which : e.keyCode;
                if (c == 13) $('.member-search #search').click();
            });
            $('.member-search #search').on('click', function () {
                var name = $('.member-search #member_name').val(),
                    company = $('.member-search #company_name').val();
                if (name || company) {
                    clearSortClass();
                    getAjaxMembersData(name, company);
                    $('#hidden_member_name').val(name);
                    $('#hidden_company_name').val(company);
                }
            });
            //������� ��� �������
            $('.member-search #clear').on('click', function () {
                changeSortClass('#thanks', 'DESC');
                $('.member-search #member_name, .member-search #company_name').val('');
                var sortObject = getSortObject();
                getAjaxMembersData(null, null, sortObject.sortName, sortObject.sortOrder);
                $('#hidden_member_name, #hidden_company_name').val('');
            });
            //������� ��� ����� �� ��������
            $('.member_company').live('click', function () {
                $(this).siblings('.members_companies').toggle();
            });
            //ajax ������� ��������� � ���������� �������
            function getAjaxMembersData(name, company, sortName, sortOrder, page) {
                var sortName = sortName || '',
                    sortOrder = sortOrder || '',
                    page = page || 1,
                    name = name || '',
                    company = company || '';
                name =  decodeURIComponent(name); //�������� � ����������
                company =  decodeURIComponent(company);
                $('.members.big_loader').show();
                $.ajax('/members/getmembers/', {
                    type: "POST",
                    data: { name: name, company: company, sortName: sortName, sortOrder: sortOrder, page: page },
                    cache: false,
                    success: function (response) {
                        $('.members.big_loader').hide();
                        var answer = $.parseJSON(response);
                        if (answer.results) {
                            $('.member-activity tbody').html(answer.results);
                            $('.members_pagination').html($.regforum.ajaxPagination(answer.pages, answer.currentPage));
                        }
                    }
                });
            }
            //��������� ������� ��������� ����������
            function changeSortClass(elem, elClass) {
                var el = $(elem),
                    elClass = elClass || null;
                el.siblings().removeClass('DESC').removeClass('ASC');
                if (elClass) {
                    el.removeClass('DESC').removeClass('ASC').addClass(elClass);
                }
                else if (el.hasClass('DESC')) {
                    el.removeClass('DESC').addClass('ASC');
                }
                else {
                    el.removeClass('ASC').addClass('DESC');
                }
            }
            //������� ����� ��� ����������
            function clearSortClass() {
                $('.member-activity thead tr th').removeClass('ASC').removeClass('DESC');
            }
            //�������� ������� �� �������� ���������
            function getSortObject() {
                var ellsArray = $('.member-activity th.ASC, .member-activity th.DESC');
                if (ellsArray) {
                    var el = $(ellsArray[0]),
                        sortName = el.attr('id'),
                        sortOrder = el.hasClass('DESC') ? 'DESC' : 'ASC';
                } else {
                    var sortName = '',
                        sortOrder = '';
                }
                return {sortName : sortName, sortOrder : sortOrder};
            }
            //�������� ����� �������� ���������
            function getPageNum() {
                return $('.pagination').find('span.current').text();
            }
            //�������� ����� ��������� ��������� ������
            function getSearchObject() {
                var name = $('#hidden_member_name').val(),
                    company = $('#hidden_company_name').val();
                return {name : name, company : company};
            }
        },
        /**
         * $.regforum.members.commenters
         */
        commenters: {
            init: function() {
                $('.member_company').live('click', function () {
                    $(this).siblings('.members_companies').toggle();
                });
            }
        },
        /**
         * $.regforum.members.authors
         */
        authors : {
            init: function() {
                $.regforum.members.commenters.init();
            }
        }
    },

    /**
     * $.regforum.companies
     */
    companies: {

        /**
         * $.regforum.companies.init
         */
        init: function (fn, goalName) {
            var node,
                phone;

            node = $('.ico.phone.masked');
            goalName = goalName || 'NUMBER_CLICK_PROFILE';

            node.on('click', function(){
                phone = $(this).data('phone');
                if(!!phone){
                    try {
                        node.html(phone);
                        ga('send', 'event', goalName);
                        yaCounter10978084.reachGoal(goalName);
                        $.post('/companies/company/goal/', {'id' : $(this).data('id')});
                        node.off('click');
                    }
                    catch(e){}
                }
                return false;
            });

            !fn || fn.call({});
        }
    },

    /**
     * $.regforum.market
     */
    market: {

        /**
         * $.regforum.market.init
         */
        init: function () {

        },

        /**
         * $.regforum.market.address
         */
        address: {

            /**
             * $.regforum.market.address.init
             */
            init: function () {
                var sorters = $('.sort-link'),
                    defaultSorter,
                    helpHint = $('#ag_bc_name').find('.help.top_hint'),
                    gridUrl,
                    ga;

                if (sorters.length && !(sorters.hasClass('asc') || sorters.hasClass('desc'))) {
                    defaultSorter = sorters.eq(0);
                    if (defaultSorter.parent().attr('id') == 'ag_tax_office_id') {
                        defaultSorter.addClass('asc').attr('href', defaultSorter.attr('href').replace('tax_office_id','tax_office_id.desc'));
                    }
                }

                !(helpHint.length && $.browser.mozilla) || (helpHint.offset(
                    {'top': Math.round((helpHint.parent().offset().top))}
                ).css({'margin-top' : '14px', 'margin-right' : '14px'}));


                if(typeof history.pushState !== 'undefined'){
                    gridUrl = $('#address_grid').find('div.keys[title]').prop('title');
                    ga = $.regforum.ce('a');
                    ga.href = gridUrl;

                    !ga.search || history.pushState({'update' : true }, document.title, gridUrl);
                }
                $.regforum.market.address.updateFilter();
            },

            /**
             * $.regforum.market.address.view
             */
            view: function () {
                var favorite;

                favorite = $('.add_favorite, .remove_favorite');
                favorite = !favorite.length ? [] : favorite.filter(function (i) { return favorite[i].hasAttribute('data-address-id')});

                if (favorite.length) {
                    favorite.each(function () {

                        var th = this;
                        $(th).on('click', function () {
                           var id = $(this).data('address-id'),
                               action = $(this).is('.add_favorite') | 0,
                               ctrl = $(this);

                            $.get('/market/juridicheskie_adresa/favorite/', { 'targetId' : id, 'action' : action},
                                function (response) {
                                    if (response.success) {
                                        ctrl.is('.add_favorite')
                                            ? ctrl.removeClass('add_favorite')
                                                .addClass('remove_favorite')
                                            : ctrl.removeClass('remove_favorite')
                                                .addClass('add_favorite');
                                    }
                                }
                            );
                        });
                    });
                }

                (document.location.hash != '#vote') || $('#add_response').click();
                return false;

            },

            /**
             * $.regforum.market.address.order
             */
            order: function () {
                var regtypes = $('#AddressOrderForm_regtype').find('input'),
                    options = $('.address-existing'),
                    hints = $('.elements p.hint span'),
                    docsDelivery = $('.address-docs-delivery');

                regtypes.on('change', function (e) {
                    switch (this.value) {
                        case 'primary':
                            options.slideUp();
                            break;
                        default:
                            options.slideDown();
                            break;
                    }
                });

                hints.length && hints.on('click', function () {
                    $(this).parents('.elements').find('input[type="text"]').val(this.innerHTML);
                });

                $('#AddressOrderForm_deliveryIncluded').on('click', function () {
                    $(this).prop('checked') ? docsDelivery.removeClass('d-n') : docsDelivery.addClass('d-n');
                });
            },

            /**
             * $.regforum.market.address.updateFilter
             */
            updateFilter: function(){

                var param,
                    searchParams,
                    id,
                    value;

                searchParams = document.location.search.substring(1).split('&');

                for(var i = 0, m = searchParams.length; i < m; i++ ){
                    param = searchParams[i].split('=');
                    id = decodeURI(param[0]).replace('[]', '');
                    value = param[1];
                    //console.log(id, value);
                    $('#' + id).find('input[value="' + value + '"]').prop('checked', true);
                }
            }
        },
        /**
         * $.regforum.market.services
         */
        services: {
            /**
             * $.regforum.market.services.init
             */
            init: function() {
                var regions = $('#regions'),
                    categories = $('#categories'),
                    button = $('input.button');
                button.on('click', function(){
                    var region = regions.find('li.selected > div').attr('region'),
                        category = categories.find('li.selected > div').attr('url');
                    if(region && category) {
                        document.location = '/market/services_'+ region + '/' + category;
                    }
                });

                categories.find('li div').on('click', function(){
                    var code = $(this).attr('regions');
                        if(code) {
                            regions.find('span.select').hide();
                            regions.find('li').removeClass('selected');
                            regions.find('span.default').show();
                            $.each(regions.find('li div'), function(index, element){
                                var element = $(element),
                                    check = new RegExp(element.attr('code'));
                                if(check.test(code)){
                                    element.parent().show();
                                } else {
                                    element.parent().hide();
                                }
                            })
                        } else {
                            regions.find('li div').parent().show();
                        }
                    regions.val(0);
                });

                regions.on('change', function(){});

            },

            /**
             * $.regforum.market.services.view
             */
            view: function () {
                var favorite;

                favorite = $('.add_favorite, .remove_favorite');
                favorite = !favorite.length ? [] : favorite.filter(function (i) { return favorite[i].hasAttribute('data-service-id')});

                if (favorite.length) {
                    favorite.each(function () {

                        var th = this;
                        $(th).on('click', function () {
                            var id = $(this).data('service-id'),
                                action = $(this).is('.add_favorite') | 0,
                                ctrl = $(this);

                            $.get('/market/services/favorite/', { 'targetId' : id, 'action' : action},
                                function (response) {
                                    if (response.success) {
                                        ctrl.is('.add_favorite')
                                            ? ctrl.removeClass('add_favorite')
                                            .addClass('remove_favorite')
                                            : ctrl.removeClass('remove_favorite')
                                            .addClass('add_favorite');
                                    }
                                }
                            );
                        });
                    });
                }

            },

            /**
             * $.regforum.market.services.region
             */
            region: function(region) {
                var cat = $('.market_categories');
                cat.find('.select_button').on('click', function(){
                    var _this = $(this);
                    if (_this.parent().hasClass('disabled')) return;
                    if (_this.hasClass('active')) {
                        _this.removeClass('active');
                        _this.find('span.arrow_down').show();
                        _this.siblings('div.categories_list').hide();
                    } else {
                        _this.addClass('active');
                        _this.find('span.arrow_down').hide();
                        _this.siblings('div.categories_list').show();
                    }
                });
                $(document).on('click', function(e){
                    if(!$(e.target).closest('.market_categories').length){
                        cat.find('div.select_button').removeClass('active');
                        cat.find('div.categories_list').hide();
                        cat.find('span.arrow_down').show();
                    }
                });

                cat.on('click', function() {
                    $.regforum.market.services.closeCategoriesList($(this).siblings('.market_categories'));
                });

            },
            /**
             * $.regforum.market.services.categoriesHandler
             */
            categoriesHandler: function (region, ellsArr, parentBlockId, childBlockId, fn) {
                var parentBlock = $(parentBlockId),
                    childBlock = $(childBlockId);

                for(var key in ellsArr) {
                    (function (key) {
                        if (ellsArr[key]) {
                            parentBlock.find('li#' + key).bind('click', function () {
                                var obj = ellsArr[key],
                                    content = '',
                                    parentUrl = $(this).attr('url');
                                for(var j in obj) {
                                    if (obj[j].id && obj[j].url && obj[j].name) {
                                        content += '<li id="'+obj[j].id+'" url="'+obj[j].url+'" data-id="'+obj[j].id+'">'+obj[j].name+'</li>';
                                    }
                                }
                                $.regforum.market.services.closeCategoriesList(this);
                                $.regforum.market.services.setCategoryTitle(this);
                                childBlock.removeClass('disabled').find('ul').html(content);
                                childBlock.find('li').bind('click', function () {
                                    $.regforum.market.services.closeCategoriesList(this);
                                    var url = $(this).attr('url');
                                    $.regforum.market.services.setCategoryTitle(this);
                                    if(!fn){
                                        document.location = '/market/services_' + region + '/' + parentUrl + '/' + url + '/';
                                    }
                                    else {
                                        fn.call(this);
                                    }
                                });
                            });
                        } else {
                            parentBlock.find('li#' + key).bind('click', function () {
                                $.regforum.market.services.closeCategoriesList(this);
                                var url = $(this).attr('url');
                                $.regforum.market.services.setCategoryTitle(this);
                                if(!fn){
                                    document.location = '/market/services_' + region + '/' + url + '/';
                                }
                                else {
                                    fn.call(this);
                                }
                            });
                        }
                    })(key);
                }

            },
            /**
             * for RMarketServicesRubrics widget
             * $.regforum.market.services.marketServicesRubrics
             */
            marketServicesRubrics: function() {
                var cat = $('.right_side .market_categories');
                cat.find('.select_button').on('click', function(){
                    var _this = $(this);
                    if (_this.parent().hasClass('disabled')) return;
                    if (_this.hasClass('active')) {
                        _this.removeClass('active');
                        _this.find('span.arrow_down').show();
                        _this.siblings('div.categories_list').hide();
                    } else {
                        _this.addClass('active');
                        _this.find('span.arrow_down').hide();
                        _this.siblings('div.categories_list').show();
                    }
                });
                $(document).on('click', function(e){
                    if(!$(e.target).closest(cat).length){
                        cat.find('div.select_button').removeClass('active');
                        cat.find('div.categories_list').hide();
                        cat.find('span.arrow_down').show();
                    }
                });

                cat.find('.categories_list').on('click', 'li div', function(){
                    var _this = $(this),
                        region = _this.closest(cat).attr('region'),
                        url = '',
                        parents = _this.parents('li');
                    $.regforum.market.services.closeCategoriesList(this);
                    for(var i = 0, len = parents.length; i < len; i++) {
                        url = $(parents[i]).attr('url') ?  $(parents[i]).attr('url') + '/' + url : '';
                    }
                    $.regforum.market.services.setCategoryTitle(_this);
                    document.location = '/market/services_' + region + '/' + url;
                });

            },
            /**
             * $.regforum.market.services.closeCategoriesList
             */
            closeCategoriesList: function (list) {
                var list = $(list).closest('.market_categories');
                list.find('div.select_button').removeClass('active');
                list.find('div.categories_list').hide();
                list.find('span.arrow_down').show();
            },
            /**
             * $.regforum.market.services.setCategoryTitle
             */
            setCategoryTitle: function (list) {
                var parent = $(list).closest('.market_categories');
                parent.find('span.select').text($(list).text());
            },

            /**
             * $.regforum.market.services.categoryView
             * @param url
             * @param form
             * @param ctrl
             * @param grid
             */
            categoryView: function (url, form, ctrl, grid) {
                var searcher = $(form),
                    remover,
                    input;

                remover = searcher.find(ctrl);
                input = searcher.find('input:text');

                searcher.on('submit', function () {
                    return false;
                });

                //remover.hide();
                remover.on('click', function () {
                    document.location.replace(url);
                });

                input.on('keyup',function () {
                    var qs = input.val(),
                        dl = document.location,
                        _url;
                    if (qs.length) {
                        searcher.find('.loader').css('visibility', 'visible');
                        remover.show();
                        _url = dl.protocol + '//' + dl.host + dl.pathname + '?qs=' + (encodeURIComponent(qs));
                        $.fn.yiiGridView.update(grid, {'url': _url});
                    }
                }).on('blur', function () {
                        var to;
                        if (input.val().length) {
                            to = setTimeout(function () {
                                remover.show();
                                clearTimeout(to);
                            }, 100);
                        }
                        else {
                            remover.hide();
                        }
                });
            }
        }
    },

    /**
     * alias for frontpage
     * $.regforum.site
     */
    site: {

        /**
         * $.regforum.site.init
         */
        init: function () {
            $.regforum._initFrontpage();
        }

    },

    /**
     * alias for frontpage
     * $.regforum.site
     */
    preview: {

        /**
         * $.regforum.preview.init
         */
        init: function () {
            $.regforum._initFrontpage();
        }

    },

    /**
     * $.regforum.moderator
     */
    moderator: {

        /**
         * $.regforum.moderator.addresses
         */
        addresses: {

            /**
             * $.regforum.moderator.addresses.init
             */
            init: function () {
                var rejected = [],
                    published = [],
                    handlers,
                    overridden = [],
                    dialogForm = $('#dialog-form'),
                    datePickerDialogForm = $('#datepicker-form'),
                    ti,
                    selector;

                /**
                 * override rejected elements click handlers
                 */
                if ($('.items .view.address').length) {
                    ti = setInterval(function () {

                        var field = $('#_address_comment'),
                            dateBilledField = $('#_date_billed');

                        handlers = $.regforum.events.getHandlers('body', 'click');
                        rejected = $('a[data-status="rejected"]');
                        published = $('a[data-status="published"]');

                        if (!!handlers) {
                            clearInterval(ti);
                            rejected.each(function (i) {
                                    var selector = '#' + this.id;
                                    $('body').off('click', selector).on('click', selector, function () {


                                        field.val('');
                                        field.next().addClass('d-n');

                                        dialogForm.dialog({
                                            autoOpen: true,
                                            height: 270,
                                            width: 350,
                                            modal: true,
                                            zIndex: $.regforum.settings().zi.dialog,
                                            buttons: {
                                                '���������': function () {

                                                    var data;
                                                    if (!$.trim(field.val())) {
                                                        field.next().removeClass('d-n');
                                                    }
                                                    else {
                                                        $(this).dialog('close');

                                                        data = $(selector).data();
                                                        data['comment'] = $('#' + field.attr('id')).val();

                                                        $.yii.submitForm(
                                                            this,
                                                            '/moderator/addresses/updatestatus/id/' + data['id'] + '/',
                                                            data
                                                        );
                                                    }
                                                }
                                            }
                                        });
                                        return false;
                                    });
                            });

                            published.each(function (i) {
                                var selector = '#' + this.id;
                                $('body').off('click', selector).on('click', selector, function () {

                                    var dateBilled = $(selector).data('date-billed'),
                                        now = new Date();

                                    dateBilled = (typeof dateBilled != 'undefined') ? dateBilled : (now.getDate() + '-' + (parseInt(now.getMonth(), 10) + 1) + '-' + now.getFullYear());
                                    dateBilledField.val(dateBilled);

                                    if(!!$(selector).data('payable')){

                                        datePickerDialogForm.find('.hint span').each(function () {
                                            var preset = $(this);
                                            preset.on('click', function () {
                                                dateBilledField.val(preset.data('date'));
                                            });
                                        });

                                        datePickerDialogForm.dialog({
                                            autoOpen: true,
                                            height: 250,
                                            width: 350,
                                            modal: true,
                                            zIndex: $.regforum.settings().zi.dialog,
                                            buttons: {
                                                '���������': function () {

                                                    var data;
                                                    $(this).dialog('close');

                                                    data = $(selector).data();
                                                    data['date_billed'] = $('#' + dateBilledField.attr('id')).val();

                                                    $.yii.submitForm(
                                                        this,
                                                        '/moderator/addresses/updatestatus/id/' + data['id'] + '/',
                                                        data
                                                    );
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        $.yii.submitForm(
                                            selector,
                                            '/moderator/addresses/updatestatus/id/' + $(selector).data('id') + '/',
                                            $(selector).data()
                                        );
                                    }

                                    return false;
                                });
                            });
                        }
                    }, 10);
                }
            },

            /**
             * $.regforum.moderator.addresses.index
             * alias for $.regforum.moderator.addresses
             */
            index: {
                init: function () {
                    $.regforum.moderator.addresses.init();
                }
            }

        },
        /**
         * $.regforum.moderator.company
         */
        company: {

            /**
             * $.regforum.moderator.company.init
             */
            init: function () {
                var dialog = $('#dialog'),
                    grid = 'company_grid_view',
                    form = $('form.company-search'),
                    input = form.find('input:text'),
                    to;

                getback = function(id) {
                    dialog.dialog({
                        modal: true,
                        autoOpen: true,
                        closeOnEscape: true,
                        resizable: false,
                        title: "������� ����������",
                        height: 300,
                        width: 450,
                        zIndex: $.regforum.settings().zi.dialog,
                        buttons: {
                            "���������": function () {
                                var description = dialog.find('#description').val();

                                $.ajax('/moderator/company/getback/', {
                                    type: "POST",
                                    data: { id: id, description: description },
                                    cache: false,
                                    success: function (result) {
                                        location.reload();
                                    }
                                });
                                $(this).dialog("close");
                            },
                            "������": function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                };

                form.on('submit', function () {
                    return false;
                });

                input.on('keyup',function () {
                    var qs = input.val(),
                        dl = document.location,
                        _url;

                    clearTimeout(to);
                    to = setTimeout(function () {
                        form.find('.loader').css('visibility', 'visible');
                        _url = dl.protocol + '//' + dl.host + dl.pathname + (dl.search.match(/^\?/i) ?  dl.search + '&' : '?') + 'qs=' + (encodeURIComponent(qs));
                        $.fn.yiiGridView.update(grid, {'url': _url, complete: function () {
                            form.find('.loader').css('visibility', 'hidden');
                        }});
                    }, 1000);
                });
            },

            /**
             * $.regforum.moderator.company.index
             * alias for $.regforum.moderator.company
             */
            index: {
                init: function () {
                    $.regforum.moderator.company.init();
                }
            }

        },
        /**
         * $.regforum.moderator.default
         */
        'default':{
            /**
             * $.regforum.moderator.default.authors
             */
            authors: {
                init: function () {
                    $('.freelance_author .author .ll').on('click', function () {
                        if (!$(this).parent().parent().siblings('table.authors_topics').is(":visible")) {
                            $('.freelance_author .author .ll').removeClass('bold');
                            $(this).addClass('bold');
                            $('table.authors_topics').hide();
                            $(this).parent().parent().siblings('table.authors_topics').show();
                        } else{
                            $('table.authors_topics').hide();
                            $('.freelance_author .author .ll').removeClass('bold');
                        }

                    });
                    $('.author_options span.show').on('click', function () {
                        $(this).hide().siblings().show();
                        $('.freelance_author .author .ll').removeClass('bold');
                        $('table.authors_topics').show();
                    });
                    $('.author_options span.hide').on('click', function () {
                        $(this).hide().siblings().show();
                        $('table.authors_topics').hide();
                    });
                    $('.print').on('click', function() {
                        $('.freelance_author .author .ll').removeClass('bold');
                        $('.freelance_author .author .ll').off();
                        $('table.authors_topics').show();
                        $('.author_avatar, #top, .right_side, footer, .breadcrumbs, h1, .author_options').toggle();
                    });
                }
            },
            /**
             * $.regforum.moderator.default.authors
             */
            topicstatistics:{
                init: function () {
                    $('.additional').on('click', function () {
                        $('table.additional_info').toggle();
                    });
                }
            }
        },
        /**
         * $.regforum.moderator.profile
         */
        profile: {
            /**
             * $.regforum.moderator.profile.update
             */
            update: {
                init: function () {

                    var uploadNs = $.regforum.upload,
                        ti,
                        validatableFields = $('#UserProfile_phone, #Users_icq');

                    ti = setInterval(function () {
                        if (uploadNs.uploadDir !== undefined) {
                            uploadNs.handleUploaderControls();
                            uploadNs.handlePhotosField();
                            clearInterval(ti);
                        }

                    }, 10);

                    validatableFields.on('keyup', function (e) {
                        var _value = $(this).val();
                        //noinspection JSLint
                        /^\d+$/.test(_value) || $(this).val(_value.slice(0,-1));
                    });
                }
            }
        }

    },

    /**
     * $.regforum.admin
     */
    admin: {

        init: function (fn) {
            // todo if necessary
            !fn || fn.call({});
        },

        /**
         * $.regforum.admin.rubricator
         */
        rubricator : {

            init: function (fn) {

                var list,
                    notifier,
                    view = '.view',
                    fader = 'slow';

                list = '.draggable .items';
                notifier = $('#notifier');

                $(list).dragsort({
                    dragSelector: view,
                    placeHolderTemplate: '<div class="view placeholder"></div>',
                    dragEnd: function () {
                        var m = [];
                        $(list).find(view).each(function (i) {
                            m[i] = $(this).data('id');
                        });
                        console.log(m);
                        $.post('/admin/rubricator/updatesort/', {'data' : m }, function (response) {
                            var to;
                            notifier.attr('class', 'sign');
                            if (response.success) {
                                notifier.addClass('sign_green').html('��������� ���������').show();
                            }
                            else {
                                notifier.addClass('sign_ruby').html('��������� �� ������� ���������').show();
                            }

                            to = setTimeout(function () {
                                notifier.fadeOut(fader);
                            }, 1000);
                        });
                    }
                });

                !fn || fn.call({});
            },
            /**
             * $.regforum.admin.rubricator.update
             */
            update: {
                init: function (fn) {

                    $.regforum.helpers.tinyMCE('PostsRubricator_rubric_preamble');
                    $.regforum.helpers.tinyMCE('PostsRubricator_rubric_info');

                    !fn || fn.call({});
                }
            }
    },

        /**
         * $.regforum.admin.marketrubricator
         */
        marketrubricator : {
            init: function (fn) {
                var notifier = $('#notifier');
                $('.rubrics_list, .rubrics_list .child_category').sortable({
                    placeholder: "placeholder",
                    cursor: "move",
                    start: function (e, ui) {
                        ui.placeholder.height(ui.item.height());
                    },
                    update: function () {
                        var m = [];
                        var _this = $(this);
                        _this.children('li').each(function (i) {
                            m[i] = $(this).attr('id');
                        });
                        console.log(m);
                        $.post('/admin/marketrubricator/updatesort/', {'data' : m }, function (response) {
                            var to;
                            notifier.attr('class', 'sign');
                            if (response.success) {
                                notifier.addClass('sign_green').html('��������� ���������').show();
                            }
                            else {
                                notifier.addClass('sign_ruby').html('��������� �� ������� ���������').show();
                            }

                            to = setTimeout(function () {
                                notifier.fadeOut('slow');
                            }, 1000);
                        });
                    }
                });
                !fn || fn.call({});
            },
            /**
             * $.regforum.admin.marketrubricator.create
             */
            create: {
                init: function () {
                    $('#MarketServiceCategory_category_name').on('change', function () {
                        var url = $('#MarketServiceCategory_url');
                        if (!url.val()) url.val($.regforum.helpers.transliteration.rus2translit($(this).val().toLowerCase()));
                    });
                }
            }
        },
        /**
         * $.regforum.admin.filesrazdels
         */
        filesrazdels : {
            init: function (fn) {
                var notifier = $('#notifier');
                $('.rubrics_list').sortable({
                    connectWith: "ul",
                    placeholder: "placeholder",
                    cursor: "move",
                    start: function (e, ui) {
                        ui.placeholder.height(ui.item.height());
                    },
                    update: function () {
                        var children = [],
                            parent,
                            parentId,
                            _this = $(this);
                        parent = _this.parent('li');
                        parentId = parent.length ? parent.attr('id') : null;
                        _this.children('li').each(function (i) {
                            children[i] = $(this).attr('id');
                        });
                        $.post('/admin/filesrazdels/updatesort/', {'parent': parentId, 'children' : children }, function (response) {
                            var to;
                            notifier.attr('class', 'sign');
                            if (response.success) {
                                notifier.addClass('sign_green').html('��������� ���������').show();
                            }
                            else {
                                notifier.addClass('sign_ruby').html('��������� �� ������� ���������').show();
                            }

                            to = setTimeout(function () {
                                notifier.fadeOut('slow');
                            }, 1000);
                        });
                    }
                });
                !fn || fn.call({});
            },
            /**
             * $.regforum.admin.filesrazdels.create
             */
            create: {
                init: function () {
                    $('#MarketServiceCategory_category_name').on('change', function () {
                        var url = $('#MarketServiceCategory_url');
                        if (!url.val()) url.val($.regforum.helpers.transliteration.rus2translit($(this).val().toLowerCase()));
                    });
                }
            }
        },
        /**
         * $.regforum.admin.mailer
         */
        mailer: {

            /**
             * $.regforum.admin.mailer.message
             */
            message: {

                init: function (fn) {
                    var select,
                        editorId = 'mailer_message',
                        editorArea,
                        submitButton,
                        submitValue,
                        draftField;

                    select = $('#_mailer_templates');
                    submitButton = $('#mailer-messages-message-form').find('input[type="submit"]');
                    submitValue = submitButton.val();
                    draftField = $('#MailerMessages_status');

                    draftField.on('click', function () {
                        submitButton.val(this.checked ? submitValue : '���������');
                    });

                    select.on('change', function () {
                        var id = this.value | 0;

                        select.prevAll().show().filter(!id ? '#tplSelected' : ':not(#tplSelected)').hide();

                        if (tinyMCE && (typeof tinyMCE.editors != 'undefined') && tinyMCE.editors.length) {
                            editorArea = (tinyMCE.editors[0].editorId == editorId) ? tinyMCE.editors[0] : null;
                            id > 0 && $.post('/admin/mailer/gettemplate/', { id: id }, function (response) {
                                if (!!response.success) {
                                    editorArea.setContent(response.template);
                                }
                            });
                        }
                    });

                    $('.attach-more:last').live('click', function () {
                        $.regforum.admin.mailer.message.handleAttachMore(this);
                    });

                },

                handleAttachMore: function (el) {
                    var it = $(el),
                        container = it.parent();

                    container.clone().appendTo(container.parent());
                    $('.attach-more:not(:last)').hide();
                }
            },

            /**
             * $.regforum.admin.mailer.admin
             */
            admin: {
                init: function () {
                    var grid = $('#mailer-templates-grid').find('table'),
                        th,
                        index;

                    th = grid.find('th');
                    if (th.length) {
                        index = th.index(th.find('a[href$="system_name"]').parent());
                        grid.find('tbody tr').find('td:eq(' + index + ')').each(
                            function (i) {
                            var text = $.trim($(this).text());
                                if (text.length) {
                                    $(this).parents('tr').find('td.button-column a.delete').remove();
                                }
                            }
                        );
                    }

                }
            },

            /**
             * $.regforum.admin.mailer.create
             */
            create: {
                init: function () {
                    var tplElements = $('#mailerTplVars'),
                        dl,
                        ctrl;

                    if (tplElements.length) {
                        dl = tplElements.find('dl');
                        ctrl = tplElements.find('span');

                        ctrl.addClass('ctrl').attr('title', '�������� ��� ������ ������');
                        ctrl.on('click', function () {
                            dl.parent().toggleClass('d-n');
                        });

                        dl.find('code').on('mouseover',function () {
                            $(this).attr('title', '�������� ���������� � ��������');
                        }).on('mouseout',function () {
                                $(this).removeAttr('title');
                            }).on('click', function () {
                                var it = this;
                                try {
                                    var content = tinyMCE.activeEditor.getContent();
                                    tinyMCE.activeEditor.setContent(
                                        tinyMCE.activeEditor.getContent()
                                        + '<p>' + it.innerHTML + '</p>'
                                    )
                                }
                                catch (e) {
                                    alert('�� ������� �������� ���������� � ��������.');
                                }
                            });
                    }
                }
            }
        },
        /**
         * $.regforum.admin.frontpage
         */
        frontpage: {

            /**
             * $.regforum.admin.frontpage.init
             */
            init: function () {
                //todo
            },

            /**
             * $.regforum.admin.frontpage.forum
             */
            forum: {
                init: function () {
                    var chosenSelect = $('.chosen-select');

                    chosenSelect.chosen({'width': '100%'});
                }
            },

            /**
             * $.regforum.admin.frontpage.posts
             */
            posts : {
                init: function () {

                }
            }
        },
        /**
         * admin.users
         */
        users: {
            /**
             * admin.users.update
             */
            update: {
                init: function(){
                    $.regforum.moderator.profile.update.init();
                }
            }
        }

    },

    /**
     * $.regforum.kladr
     */
    kladr: {
        init: function () {
            ajaxPagination = $.regforum.ajaxPagination;
        }
    },

    /**
     * $.regforum.files
     */
    files: {
        init: function () {
            $.regforum._initFrontpage();
        },
        razdels: function (box) {
            $('.expandingRazdels .current').parents().show();
            $(box).find('a').on('click', function () {
                _this = $(this);
                _this.siblings('.razdel').toggle(200);
                _this.parent().siblings().find('.razdel').hide(200);
            });
        }
    },

    /**
     * $.regforum.ajaxPagination
     * ���������� ������ �� �������������� � ��� �������� HTML ������ ��� ���������
     * pagesCount - ����� ���������� �������, currentPage ������� �������� id - id- ���� ������������� ��������
     */
    ajaxPagination: function (pagesCount, currentPage, id) {
        var startElsCount = 1,//���������� ��������� ��� ����������� � ������ � � �����
            endElsCount = 1,
            numDisplayentries = 6,
            ellipseText = '...';
        id = id ? id : '';
        currentPage = currentPage ? parseInt(currentPage) : 0;
        pagesCount = pagesCount ? parseInt(pagesCount) : 0;
        var interval = getInterval();
        if (pagesCount < 2) return ''; //���� 1 ��� 0 �� ���������� ���������.

        function getInterval() {
            var ne_half = Math.ceil(numDisplayentries / 2);
            var np = pagesCount || 0;
            var upper_limit = np - numDisplayentries;
            var start = currentPage > ne_half ? Math.max(Math.min(currentPage - ne_half, upper_limit), 1) : 1;
            var end = currentPage > ne_half ? Math.min(currentPage + ne_half, np) : Math.min(numDisplayentries, np);
            return [start, end];
        }

        var startEls = '';
        if (pagesCount > startElsCount && interval[0] > 0) {
            var count = Math.min(interval[0], startElsCount + 1);
            for(var i = 1; i < count; i++) {
                startEls += '<a id="' + i + '" href="#">' + i + '</a>';
            }
            if (startElsCount < interval[0] && ellipseText) {
                startEls += "<span>" + ellipseText + "</span>";
            }
        }

        var mainElls = '';
        for (var i = interval[0]; i <= interval[1]; i++) {
            if (currentPage == i) {
                mainElls += '<span class="current">' + currentPage + '</span>';
            } else {
                mainElls += '<a id="' + i + '" href="#">' + i + '</a>';
            }
        }

        var endEls = '';
        if (pagesCount > startElsCount + endElsCount && interval[1] < pagesCount) {
            var  count = pagesCount + 1 - Math.min(endElsCount, pagesCount - interval[1]);
            for(var i = count; i <= pagesCount; i++) {
                endEls += '<a id="' + i + '" href="#">' + i + '</a>';
            }
            if (pagesCount - interval[1] > endElsCount  && ellipseText) {
                endEls = "<span>" + ellipseText + "</span>" + endEls;
            }
        }

        var leftArrow = currentPage - 1 > 0 ? '<a id="' + (currentPage - 1) + '" href="#" class="next">&larr; �����</a>': '';
        var rightArrow = currentPage < pagesCount ? '<a id="' + (currentPage +1) + '" href="#" class="next">������ &rarr;</a>' : '';
        return '<div id="' + id + '" class="pagination">' + leftArrow + startEls + mainElls + endEls + rightArrow + '</div>'

    },

    /**
     * $.regforum.login
     */
    login : {
        init: function () {
            $('#mainmenu a.login, .guest-info a.login').on('click', function () {
                $('div.loginBox').stop();
                $('div.loginBox').show("explode", {}, 100);
                return false;
            });
            $('.close').on('click', function () {
                $(this).parent().parent().hide();
            });
            $(document).click(function (e) {
                if (!$(e.target).closest('div.loginBox').length && !$(this).hasClass('login')) {
                    $('div.loginBox').hide("explode", {}, 100);
                    $('div.loginBox').stop();
                }
            });
            $('div.loginForm input#login').on('change', function () {
                var _this = $(this),
                    val = $('input#login').val(),
                    errorBlock = $('div.loginForm #loginFormError'),
                    button = $('div.loginForm button');
                if (val =='') {
                    errorBlock.hide();
                    _this.removeClass('success').addClass('error');
                    button.attr('disabled', 'disabled');
                    return;
                }
                var ansver = $.regforum.login.checkUserName(val);
                if (!ansver) {
                    errorBlock.show();
                    _this.removeClass('success').addClass('error');
                    button.attr('disabled', 'disabled');
                } else {
                    errorBlock.hide();
                    _this.removeClass('error').addClass('success');
                    button.removeAttr('disabled');
                }
            });
            $('div.loginBox').draggable();
            /* $(window).resize(function () {
             var loginBox = $('.loginBox');
             //loginBox.css("position","absolute");
             //loginBox.css("top", (($(window).height() - loginBox.outerHeight()) / 2) + $(window).scrollTop() + "px");
             //loginBox.css("left", (($(window).width() - loginBox.outerWidth()) / 2) + $(window).scrollLeft() + "px");
             });*/
        },
        checkUser: function (name, email) {
            var result;
            if(name && name.length < 3) return {username : '������� �������� ���'};
            $.ajax('/login/auth/checkuser', {
                type: "GET",
                data: { username: name, email: email },
                cache: false,
                async: false,
                success: function (response) {
                    var answer = $.parseJSON(response);
                    if (answer) {
                        result = answer;
                    }
                }
            });
            return result;
        },
        checkUserName: function (name) {
            var result = false;
            $.ajax('/login/auth/checkusername', {
                type: "GET",
                data: { username: name },
                cache: false,
                async: false,
                success: function (response) {
                    var answer = $.parseJSON(response);
                    if (answer) {
                        result = answer;
                    }
                }
            });
            return result;
        },
        /**
         * $.regforum.login.auth
         */
        auth : {
            /**
             * $.regforum.login.auth.add
             */
            add: {
                init: function () {
                    var username = $('#username'),
                        email = $('#email'),
                        userNameMessage = $('#userNameMessage'),
                        emailMessage = $('#emailMessage'),
                        badAdressEmailMessage = $('#badAdressEmailMessage'),
                        loader = $('.loader'),
                        button = $('.button');

                    username.on('change', function () {
                        var _this = $(this),
                            errors = $.regforum.login.checkUser(_this.val(), null);
                        _this.removeClass('success, error');
                        if (!errors) return;
                        if (errors.username) {
                            _this.addClass('error');
                            userNameMessage.show().html(errors.username);
                        } else {
                            _this.addClass('success');
                            userNameMessage.hide();
                        }
                    });
                    email.on('change', function () {
                        var _this = $(this);
                        _this.removeClass('success, error');
                        if (!$.regforum.login.auth.add.validateEmail(_this.val())) {
                            badAdressEmailMessage.show();
                            emailMessage.hide();
                            return false;
                        } else {
                            badAdressEmailMessage.hide();
                        }
                        var errors = $.regforum.login.checkUser(null, $('#email').val());
                        if (!errors) return;
                        if (errors.email) {
                            _this.addClass('error');
                            emailMessage.show().html(errors.email);
                        } else {
                            _this.addClass('success');
                            emailMessage.hide();
                        }
                    });
                    username.change();
                    email.change();
                    if (!email.val()) $(emailMessage, badAdressEmailMessage).hide();
                    button.on('click', function () {
                        loader.show();
                        button.attr('disabled', 'disabled');
                        if (!$.regforum.login.auth.add.validateEmail(email.val())) {
                            badAdressEmailMessage.show();
                            loader.hide();
                            button.removeAttr('disabled');
                            return false;
                        } else {
                            badAdressEmailMessage.hide();
                        }
                        var errors = $.regforum.login.checkUser(username.val(), email.val());
                        if (errors.email || errors.username) {
                            if (errors.email) {
                                emailMessage.show().html(errors.email);
                            } else {
                                emailMessage.hide();
                            }
                            if (errors.username) {
                                userNameMessage.show().html(errors.username);
                            } else {
                                userNameMessage.hide();
                            }
                            loader.hide();
                            button.removeAttr('disabled');
                            return false;
                        } else {
                            $('form').submit();
                            return true;
                        }
                    });
                },
                validateEmail: function (emailaddress) {
                    var emailReg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    return emailReg.test(emailaddress);

                }
            }
        }
    },
    /**
     * $.regforum.posts.add
     */
    posts: {
        add: {
            init: function () {
                //�������� ��� �������� ��������
                $(window).bind(
                    "beforeunload",
                    function () {
                        var content = tinyMCE.activeEditor.getContent(),
                            ids = [];
                        $(content).find('img[id]').each(function () {
                            ids.push($(this).attr('id'));
                        });
                        if (ids.length) {
                            $.ajax({
                                type: "POST",
                                url: "/fotki/remove/",
                                data: { ids: ids },
                                async: false //����������! ��� ����� �� ������������
                            });
                        }
                    }
                );
                //�������� ��� submit-e
                $('form').submit(function () {
                    $(window).unbind("beforeunload");

                });
            }
        }
    },
    advertising: {
        init: function () {
            $.regforum.helpers.checkInputPhone();
            $('#ajax_submit').on('click', function () {
                var phone = $('#your_phone').val(),
                    url = '/site/feedback/';
                if (phone) {
                    $.post(url, {'phone': phone}, function (success) {
                        if (success) {
                            $('.mailManager').hide();
                            $('.mailComplete').show();
                        }
                        }
                    );
                }
            });
        }
    },

    /**
     * $.regforum.helpers
     */
    helpers: {

        /**
         * $.regforum.helpers.autocomplete
         */
        autocomplete: {

            /**
             * $.regforum.helpers.autocomplete.findUsers
             */
            findUsers: function (userDialog, fn) {

                if (!userDialog.dialog('option', 'disabled')) {

                    //�������� ���� ������
                    $.getJSON('/posts/getusers/', { fastsearch: '' },
                        function (obj) {
                            if (obj) {
                                //noinspection JSUnresolvedVariable
                                $('#user_dialog_content').html(obj._content);
                            }
                        });
                    userDialog.dialog('open');

                }

                $('.dialog_filter').live('keyup', function () {
                    var q = $(this).val();
                    //$("div.top_filter").addClass('load');
                    $.getJSON('/posts/getusers/', { fastsearch: q },
                        function (obj) {
                            if (obj) {
                                //noinspection JSUnresolvedVariable
                                $('#user_dialog_content').html(obj._content);
                                //$("div.top_filter").removeClass('load');
                            }
                        });
                });

                //������� ��� ������
                $('#user_dialog_content').one('click', 'p', function () {
                    !fn || fn.call({'id': $(this).attr('id'), 'text': $(this).text() });
                    userDialog.dialog('close');
                });
            }
        },
        /**
         * $.regforum.helpers.datepicker
         */
        datepicker: function () {
            $(".datepicker").datepicker({
                changeYear: true,
                clearText: '��������', clearStatus: '',
                closeText: '�������', closeStatus: '',
                prevText: '&#x3c;����',  prevStatus: '',
                prevBigText: '&#x3c;&#x3c;', prevBigStatus: '',
                nextText: '����&#x3e;', nextStatus: '',
                nextBigText: '&#x3e;&#x3e;', nextBigStatus: '',
                currentText: '�������', currentStatus: '',
                monthNames: ['������','�������','����','������','���','����','����','������','��������','�������','������','�������'],
                monthNamesShort: ['���','���','���','���','���','���','���','���','���','���','���','���'],
                monthStatus: '', yearStatus: '',
                weekHeader: '��', weekStatus: '',
                dayNames: ['�����������','�����������','�������','�����','�������','�������','�������'],
                dayNamesShort: ['���','���','���','���','���','���','���'],
                dayNamesMin: ['��','��','��','��','��','��','��'],
                dayStatus: 'DD', dateStatus: 'D, M d',
                dateFormat: 'dd.mm.yy', firstDay: 1,
                initStatus: '', isRTL: false,
                showMonthAfterYear: false, yearSuffix: ''
            });
        },
        /**
         * $.regforum.helpers.checkInputPhone
         */
        checkInputPhone: function () {//specialchars : -
            $('.check_phone').on('keypress', function (event) {
                if (event.charCode == 0) return true;
                //var max = $(this).attr('max') || 9; || this.value.length +1 > max
                if (isNaN(String.fromCharCode(event.keyCode)) && isNaN(String.fromCharCode(event.charCode)) && event.charCode !=45 && event.charCode !=40 && event.charCode != 41 && event.charCode !=32) return false;
            });
        },
        /**
         * $.regforum.helpers.tinyMCE
         */
        tinyMCE: function (elem) {
            tinyMCE.init({
                mode: "exact",
                elements: elem,
                theme: "advanced",
                language: 'ru',
                plugins: "paste,searchreplace,inlinepopups",
                width: "100%",
                height: "200",
                gecko_spellcheck: true,
                // Theme options
                theme_advanced_buttons1: "bold,italic,underline,blockquote,inquotes,upper_�ase,lower_�ase,|,search,replace,|,bullist,numlist,|,undo,redo,|,link,unlink", //
                theme_advanced_buttons2: "",
                theme_advanced_buttons3: "",
                theme_advanced_buttons4: "",
                theme_advanced_toolbar_location: "top",
                theme_advanced_toolbar_align: "left",
                theme_advanced_statusbar_location: "bottom",
                theme_advanced_resizing: false,
                content_css: "/xcss/tinymce_body.css?v12",
                setup: function (ed) {
                    ed.addButton('upper_�ase', {
                        title : '������������� � ������� ��������',
                        image : '/ximg/toolbar/edit-uppercase.png',
                        onclick : function () {
                            ed.focus();
                            ed.selection.select();
                            ed.selection.setContent(ed.selection.getContent().toUpperCase());
                        }
                    });
                    ed.addButton('lower_�ase', {
                        title : '������������� � ������ ��������',
                        image : '/ximg/toolbar/edit-lowercase.png',
                        onclick : function () {
                            ed.focus();
                            ed.selection.select();
                            ed.selection.setContent(ed.selection.getContent().toLowerCase());
                        }
                    });
                },
                'formats' : {
                    'alignleft' : {'selector' : 'h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-left'},
                    'aligncenter' : {'selector' : 'h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-center'},
                    'alignright' : {'selector' : 'h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-right'},
                    'alignfull' : {'selector' : 'h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-justify'},
                    'italic' : {'inline' : 'em'},
                    'bold' : {'inline' : 'strong'},
                    'underline' : {'inline' : 'u'},
                    'strikethrough' : {'inline' : 'del'}
                }
            })
        },
        /**
         * $.regforum.helpers.transliteration
         */
        transliteration: {
            rus2translit: function (string) {
                var converter = {
                        '�' : 'a',   '�' : 'b',   '�' : 'v',
                        '�' : 'g',   '�' : 'd',   '�' : 'e',
                        '�' : 'e',   '�' : 'zh',  '�' : 'z',
                        '�' : 'i',   '�' : 'y',   '�' : 'k',
                        '�' : 'l',   '�' : 'm',   '�' : 'n',
                        '�' : 'o',   '�' : 'p',   '�' : 'r',
                        '�' : 's',   '�' : 't',   '�' : 'u',
                        '�' : 'f',   '�' : 'h',   '�' : 'c',
                        '�' : 'ch',  '�' : 'sh',  '�' : 'sch',
                        '�' : '',    '�' : 'y',   '�' : '',
                        '�' : 'e',   '�' : 'yu',  '�' : 'ya',

                        '�' : 'A',   '�' : 'B',   '�' : 'V',
                        '�' : 'G',   '�' : 'D',   '�' : 'E',
                        '�' : 'E',   '�' : 'Zh',  '�' : 'Z',
                        '�' : 'I',   '�' : 'Y',   '�' : 'K',
                        '�' : 'L',   '�' : 'M',   '�' : 'N',
                        '�' : 'O',   '�' : 'P',   '�' : 'R',
                              '�' : 'S',   '�' : 'T',   '�' : 'U',
                              '�' : 'F',   '�' : 'H',   '�' : 'C',
                              '�' : 'Ch',  '�' : 'Sh',  '�' : 'Sch',
                              '�' : '',    '�' : 'Y',   '�' : '',
                        '�' : 'E',   '�' : 'Yu',  '�' : 'Ya',
                        ' ' : '_',   '-' : '_'
                    },
                    r = '',
                    k;
                for (k in converter) r += k;
                r = new RegExp('[' + r + ']', 'g');
                k = function (a) {
                    return a in converter ? converter[a] : '';
                };
                string = string.replace(/^\s+|\s+$/g, '');
                string = string.replace(r, k);
                string = string.replace(/(\W)+/gm, '_');
                return string.replace(/(_){2,}/gm, '_');
            }
        },
        /**
         * $.regforum.helpers.getSelectedValues
         */
        getSelectedValues: function(ells){
            var _ells = $(ells),
                checkboxes = _ells.find('input:checkbox'),
                vals = [];
            if(checkboxes.length) {
                checkboxes.each(function(){
                    if($(this).is(':checkbox') && $(this).is(':checked')) vals.push($(this).val());
                });
            } else if (_ells.is('select')){
                vals.push(_ells.val());
            }
            return vals;
        },
        /**
         * $.regforum.helpers.flash
         */
        flash: function(message, success, delay){
            var fm = $('#flashMessage'),
                delay = delay ? delay : 1800;

            if(success){
                fm.addClass('sign_green').removeClass('sign_ruby').html(message);
                fm.show().delay(delay).fadeOut(200);
            } else {
                fm.addClass('sign_ruby').removeClass('sign_green').html(message);
                fm.show().delay(delay).fadeOut(200);
            }
        }
    },
    month_author: {
        init: function () {
            $('.slide').click(function () {
                if ($.browser.msie) {
                    $('div.slide_content').not(this).hide();
                    $(this).next('div.slide_content').show();
                }else{
                    $('.content div.slide_content').removeClass('active');
                    $(this).next('div.slide_content').addClass('active');
                    $('.content div.slide_content').not('.active').hide("blind", { direction: "up" }, "slow");
                    $('.content div.slide_content').find('div.slide_content').stop(true);
                    if ( $(this).next('div.slide_content').is(":visible")) {
                        $(this).next('div.slide_content').hide("blind", { direction: "up" }, "slow");
                    }else{
                        $(this).next('div.slide_content').show("blind", { direction: "up" }, "slow");
                    }
                }
            });
        }
    },
    advertising_rules: {
        init: function () {
            $.regforum.month_author.init();
        }
    },
    rules: {
        init: function () {
            $.regforum.advertising_rules.init();
        }
    },
    /**
     * $.regforum.complaint
     */
    /**
     * ������ �� ���-����
     * @param complaintDialog - ������� ����� ���� ������
     * @param params - ��������� ���������
     * @param options - ������ � ����������� ���������� ����
     */
    complaint: function (complaintDialog, params, options, handler) {
        var complaintDialog = $(complaintDialog),
            handler = $(handler);
        if(!complaintDialog.length) return;
        var options = options ? options : {},
            params = params ? params : {},
            descriptionEl = complaintDialog.find('.description'),
            motiveEl = complaintDialog.find('#motive'),
            min_description_length = 5,
            model = params.model ? params.model : null,
            target_id = params.target_id ? params.target_id : null,
            dialog_options = {
                modal: options.modal ? options.modal : true,
                autoOpen: options.autoOpen ? options.autoOpen : false,
                closeOnEscape: options.closeOnEscape ? options.closeOnEscape : true,
                resizable: options.resizable ? options.resizable : false,
                title: options.title ? options.title : '������',
                height: options.height ? options.height : 'auto',
                width: options.width ? options.width : 'auto',
                zIndex: $.regforum.settings().zi.dialog,
                buttons: options.buttons ? options.buttons :
                {
                    "���������": function () {
                        var description = descriptionEl.val(),
                            motive = $.regforum.helpers.getSelectedValues(motiveEl),
                            fm = $('#flashMessage');;
                        if (description.length > min_description_length) {
                            complaintDialog.find('.errorMessage').hide();
                            $.ajax('/complaints/complaint/', {
                                type: 'POST',
                                data: {target_id: target_id, description: description, model: model, motive: motive},
                                cache: false,
                                success: function (result) {
                                    if(handler.length){
                                        handler.removeClass('ll');
                                        handler.addClass('success');
                                        handler.off();
                                        handler.removeAttr('onclick');
                                        if(handler.text()) handler.text(result)
                                    }
                                    fm.addClass('sign_green').removeClass('sign_ruby').html(result);
                                    fm.show().delay(1800).fadeOut(200);
                                },
                                error: function() {
                                    fm.addClass('sign_red').removeClass('sign_ruby').html('��������, �������� �������������� ������!');
                                    fm.show().delay(1800).fadeOut(200);
                                }

                            });
                            $(this).dialog("close");
                        }
                        else {
                            complaintDialog.find('.errorMessage').show();
                        }
                    },
                    "������": function () {
                        $(this).dialog("close");
                    }
                }
        }

        complaintDialog.dialog(dialog_options);

        if (!complaintDialog.dialog('option', 'disabled')) {
            complaintDialog.dialog('open');
        }
    }

});

$(document).ready(function () {

    var moveTop = $('#movetop'),
        topHint = $('.top_hint'),
        cursorHint = $('.cursor_hint'),
        loggable = $('a.loggable'),
        hintOptions = {
            className: 'tip-yellowsimple',
            showTimeout: 200,
            alignTo: 'target',
            alignX: 'center',
            alignY: 'top',
            offsetY: 7,
            liveEvents: true,
            allowTipHover: false
        };

    $.regforum.init();

    /**
     * common part
     */
    moveTop.hide();

    if ($.regforum.legacy) {
        $("#mainmenu").find(".sub").mouseenter(function (e) {
            $(".submenu", this).fadeIn(0);
        }).mouseleave(function (e) {
            // todo: need to fix blinking
            var _this = this,
                to = setTimeout(function () {
                $(".submenu", _this).fadeOut(150);
                clearTimeout(to);
            }, 700);
        });
    }
    else {
        if ($.regforum.touch) {
            var _te = $('#mainmenu').find('.sub > a, .sub > span');

            _te.each(function (i) {
                var th = this,
                    _handler,
                    _sv;

                _handler = function (el, e) {

                    if ($(el).next().is(':visible')) {

                        $(el).click();
                        return true;
                    }
                    else {

                        _sv = $('#mainmenu').find('.submenu:visible');
                        _sv.hide();
                        _sv.prev().on('click.touched', function (e) {
                            e.preventDefault();
                            _handler(this, e);
                        });

                        $(el).off('click.touched');
                        $(el).next().show();

                        return false;
                    }
                };

                $(th).on('click.touched', function (e) {
                    e.preventDefault();
                    _handler(this, e);
                });
                th.addEventListener('ontouchstart', function (e) {
                    e.preventDefault();
                    _handler(this, e);
                });

            });
        }
        else{
            $("#mainmenu").find(".sub").hover(function () {
                $(".submenu", this).fadeIn(150)
            }, function () {
                $(".submenu", this).fadeOut(150)
            });
        }

    }

    topHint.length && topHint.poshytip(hintOptions);

    loggable.length && loggable.off('click').on('click', function(e){

        var it = this,
            data;

        data = !$(it).closest('.attention').length ? $(it).data() : $('h1').data();
        data['url'] = it.href;

        $.ajax({
            type: 'POST',
            url: '/site/loglink/',
            data: data,
            async: false
        });
    });

    if(cursorHint.length) {
        hintOptions.alignTo = 'cursor';
        cursorHint.poshytip(hintOptions);
    }

    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            moveTop.fadeIn();
        } else {
            moveTop.fadeOut();
        }
    });
    moveTop.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 200);
        return false;
    });
});