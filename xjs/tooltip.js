$(function () {
    var distance = 15;
    var time = 200;
    var hideDelay = 0;
    var hideDelayTimer = null;
    var beingShown = false;
    var shown = false;

    $('.rating_label').mouseover(function() {
        var response_id = $(this).attr("id");
        var info = $('#tip'+response_id);
        if (hideDelayTimer) clearTimeout(hideDelayTimer);
        if (beingShown || shown) {
            return true;
        } else {
            beingShown = true;
            info.css({
                top:-58,
                left:76,
                display: 'block'
            }).animate({
                left: '-=' + distance + 'px',
                opacity: 1
            }, time, 'swing', function() {
                beingShown = false;
                shown = true;
            });
        }
        return false;
    }).mouseout(function () {
        var response_id = $(this).attr("id");
        var info = $('#tip'+response_id);
        if (hideDelayTimer) clearTimeout(hideDelayTimer);
        hideDelayTimer = setTimeout(function () {
            hideDelayTimer = null;
            info.animate({
                left: '-=' + distance + 'px',
                opacity: 0
            }, time, 'swing', function () {
                shown = false;
                info.css('display', 'none');
            });
        }, hideDelay);
        return false;
    });
});