/**
 * Created with JetBrains PhpStorm.
 * User: Vasilevs
 * Date: 22.12.12
 * Time: 20:28
 */

$(document).ready(function () {
    var favorite = $('#company .add_favorite, #company .remove_favorite');

    $('input.star').rating(); //TODO �������?

    $('ul.company_tabs').delegate('li:not(.current)', 'click', function() {
        $(this).addClass('current').siblings().removeClass('current')
            .parents('div.main_data').find('div.company_tabs_content').hide().eq($(this).index()).fadeIn(500);
    });

    $('#add_response').click(function() {
        var rform = $('#add_references_form');
        if(rform.is(':visible')){
            $('div.form, .topic_comment').show();
            rform.hide(200);
        }
        else {
            $('div.form, .topic_comment').hide();
            rform.show(300);
        }
        return false;
    });

    favorite = !favorite.length ? [] : favorite.filter(function (i) { return favorite[i].hasAttribute('data-company-id')});

    if (favorite.length) {
        favorite.each(function () {

            var th = this;
            $(th).on('click', function () {
                var id = $(this).data('company-id'),
                    action = $(this).is('.add_favorite') | 0,
                    ctrl = $(this);

                $.get('/companies/favorite/', { 'targetId' : id, 'action' : action},
                    function (response) {
                        if (response.success) {
                            ctrl.is('.add_favorite')
                                ? ctrl.removeClass('add_favorite')
                                .addClass('remove_favorite')
                                : ctrl.removeClass('remove_favorite')
                                .addClass('add_favorite');
                        }
                    }
                );
            });
        });
    }
});