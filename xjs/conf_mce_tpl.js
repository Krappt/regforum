/**
 * Created by JetBrains PhpStorm.
 * User: Vasilevs
 * Date: 14.08.12
 * Time: 16:23
 */

$.fn.getStyleObject = function(){
    var dom = this.get(0),
        style,
        returns = {},
        rx = /^(Webkit|Moz|MS|O)/;
    if(window.getComputedStyle){
        var camelize = function(a,b){
            return b.toUpperCase();
        };
        style = window.getComputedStyle(dom, null);
        for(var i = 0, l = style.length; i < l; i++){
            var prop = style[i];
            var camel = prop.replace(/\-([a-z])/, camelize);
            var val = style.getPropertyValue(prop);

            // remove vendor properties
            if(!rx.test(camel)){
                returns[camel] = val;
            }
            else {
                console.log(camel);
            }
        }
        return returns;
    }
    if(style = dom.currentStyle){
        for(var prop in style){
            returns[prop] = style[prop];
        }
        return returns;
    }
    if(style = dom.style){
        for(var prop in style){
            if(typeof style[prop] != 'function'){
                returns[prop] = style[prop];
            }
        }
        return returns;
    }
    return returns;
};

$(document).ready(function () {

    tinyMCE.init({
        mode : "exact",
        elements : "mailer_template, mailer_message",
        theme : "advanced",
        language : "ru",
        plugins : "paste,style,searchreplace,inlinepopups,table,fullscreen,preview",
        width: "664",
        height: "400",
        theme_advanced_buttons1 : "bold,italic,underline,blockquote,inquotes,link,unlink,|,fontselect,fontsizeselect,|,bullist,numlist,|,forecolor,backcolor,|,inlinecss,removeformat", //
        theme_advanced_buttons2 : "undo,redo,|,tablecontrols,|,search,replace,|,code,fullscreen,preview",
        theme_advanced_buttons3 : "",
        theme_advanced_buttons4 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : false,
        theme_advanced_blockformats : "p,h2,blockquote",
        content_css : "/xcss/tinymce_body.css",
        onchange_callback: $.regforum.profile.companies.add.editorCounter,
        handle_event_callback: $.regforum.profile.companies.add.editorCounter,
        relative_urls: false,
        remove_script_host : false,
        convert_urls : false,
        setup : function(ed) {
            ed.addButton('inquotes', {
                title : '� ��������',
                image : '/ximg/toolbar/in_quotes.png',
                onclick : function() {
                    ed.focus();
                    ed.selection.select();
                    ed.selection.setContent('&laquo;'+ed.selection.getSel()+'&raquo;');
                }
            });
            ed.addButton('inlinecss', {
                title: '�������� �����',
                image: '/ximg/icons/css_20x20.png',
                onclick: function () {
                    $(ed.contentDocument).find('body').find('*')
                        .each(function () {
                            var s,
                                p,
                                styleObject = $(this).getStyleObject(),
                                inlineStyles = {};

                            if($(this).attr('style')){
                                s = $(this).attr('style').split(';');
                                if(s.length){
                                    for(var i = 0, m = s.length; i < m; i++ ){
                                        p = s[i].split(':');
                                        if(p.length == 2){
                                            inlineStyles[p[0]] = p[1];
                                        }
                                    }
                                }
                                $.isEmptyObject(inlineStyles) || (styleObject = $.extend({}, inlineStyles, styleObject));
                            }
                            $(this).css(styleObject);
                        });
                    return false;
                }
            });
        },
        apply_source_formatting : true,
        convert_fonts_to_spans : true,
        valid_elements : "@[id|class|style|title|dir<ltr?rtl|lang|xml::lang],"
            + "a[rel|rev|charset|hreflang|tabindex|accesskey|type|"
            + "name|href|target|title|class],strong/b,em/i,strike,u,"
            + "#p[style],-ol[type|compact],-ul[type|compact],-li,br,img[longdesc|usemap|"
            + "src|border|alt=|title|hspace|vspace|width|height|align],-sub,-sup,"
            + "-blockquote,-table[border=0|cellspacing|cellpadding|width|frame|rules|"
            + "height|align|summary|bgcolor|background|bordercolor],-tr[rowspan|width|"
            + "height|align|valign|bgcolor|background|bordercolor],tbody,thead,tfoot,"
            + "#td[colspan|rowspan|width|height|align|valign|bgcolor|background|bordercolor"
            + "|scope],#th[colspan|rowspan|width|height|align|valign|scope],caption,-div,"
            + "-span,-code,-pre,address,-h1,-h2,-h3,-h4,-h5,-h6,hr[size|noshade],-font[face"
            + "|size|color],dd,dl,dt,cite,abbr,acronym,del[datetime|cite],ins[datetime|cite],"
            + "object[classid|width|height|codebase|*],param[name|value|_value],embed[type|width"
            + "|height|src|*],map[name],area[shape|coords|href|alt|target],bdo,"
            + "button,col[align|char|charoff|span|valign|width],colgroup[align|char|charoff|span|"
            + "valign|width],dfn,fieldset,form[action|accept|accept-charset|enctype|method],"
            + "input[accept|alt|checked|disabled|maxlength|name|readonly|size|src|type|value],"
            + "kbd,label[for],legend,noscript,optgroup[label|disabled],option[disabled|label|selected|value],"
            + "q[cite],samp,select[disabled|multiple|name|size],small,"
            + "textarea[cols|rows|disabled|name|readonly],tt,var,big",

        extended_valid_elements : "p[style]",
        inline_styles : true,
        verify_html : false,
        invalid_elements : "iframe,script,applet,object",
        'formats' : {
            'italic' : {'inline' : 'em'},
            'bold' : {'inline' : 'strong'},
            'underline' : {'inline' : 'u'},
            'strikethrough' : {'inline' : 'del'}
        },
        removeformat : [
            {selector : 'font,u,strike', remove : 'all', split : true, expand : false, block_expand : true, deep : true}
            //{selector : 'span', attributes : ['style', 'class'], remove : 'empty', split : true, expand : false, deep : true},
            //{selector : '*', attributes : ['style', 'class'], remove: 'all', split : true, expand : true, deep : true}
        ],
        protect: [
            /\<\/?(if|endif)\>/g, // Protect <if> & </endif>
            /\<xsl\:[^>]+\>/g, // Protect <xsl:...>
            /<\?php.*?\?>/g // Protect php code
        ]
    });

});