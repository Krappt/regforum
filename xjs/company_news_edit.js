/**
 * Created with JetBrains PhpStorm.
 * User: Vasilevs
 * Date: 30.01.13
 * Time: 16:59
 */

function calculate() {
    //console.info(tinyMCE.activeEditor.id);
    var editor_id=$("#"+tinyMCE.activeEditor.id+"_counter");
    var max = editor_id.attr('max');
    var x=tinyMCE.activeEditor.getContent(); //replace(/<[^>]+>/g, '')

    var available = max - x.length;
    if(x.length < max-200)
        editor_id.html('�� ������� ' + x.length + ' ��. � ������ ��������');
    else
        editor_id.html('�������� ' + available + ' ��.');
    if(x.length > max) editor_id.html('�� ������� ������������ ���������� ��������');
    return true;
}

function morenews(id,company) {
    $.ajax('/profile/companies/getnews', {
        type: "GET",
        data: { id: id, company: company },
        cache: false,
        success: function(response){
            $('#m'+id).after(response);
            $('#m'+id).remove();
        }
    });
}

$(document).ready(function () {

    $('#add_news').click(function() {
        var mcform = $('#news_form');
        if(mcform.is(':visible'))
            mcform.hide(200);
        else {
            mcform.show(300);
        }
    });

    var user_dialog = $('#user_dialog'),
        timeout;

    if (user_dialog.length ){
        user_dialog.dialog({
            modal: true,
            autoOpen: false,
            closeOnEscape: true,
            resizable: false,
            title: "������ �� ���������",
            height: 500,
            buttons: {
                "�������": function() {
                    $(this).dialog("close");
                }
            }
        });
    }

    tinyMCE.init({
        mode : "exact",
        elements : "news_content",
        theme : "advanced",
        language : "ru",
        plugins : "paste,preview,searchreplace,inlinepopups",
        width: "664",
        height: "260",
        theme_advanced_buttons1 : "bold,italic,underline,inquotes,user,|,search,replace,|,pasteword,|,undo,redo,|,link,unlink,|,preview", //
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_buttons4 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : false,
        convert_urls : false,
        theme_advanced_blockformats : "p",
        content_css : "/xcss/tinymce_body.css?12",
        setup : function(ed) {
            ed.addButton('inquotes', {
                title : '� ��������',
                image : '/ximg/toolbar/in_quotes.png',
                onclick : function() {
                    ed.focus();
                    ed.selection.select();
                    ed.selection.setContent('&laquo;'+ed.selection.getSel()+'&raquo;');
                }
            });
            ed.addButton('user', {
                title : '������ �� ���������',
                image : '/ximg/toolbar/user-add.png',
                onclick : function() {
                    ed.focus();
                    if (!user_dialog.dialog('option', 'disabled')) {
                        user_dialog.dialog("open");
                    }

                    $(".dialog_filter").live('keyup', function() {
                        var _this = $(this);
                        clearTimeout(timeout);
                        timeout = setTimeout(function() {
                            var fastsearch = _this.val();
                            $.getJSON('/posts/getusers/', { fastsearch: fastsearch },
                                function(obj) {
                                    if (obj) {
                                        $("#user_dialog_content").html(obj._content);
                                    }
                                });
                        }, 500);
                    });
                    //������� ��� ������
                    $('#user_dialog_content').one('click', 'p', function() {
                        var _this = $(this),
                            parents = _this.parent(),
                            data_id = _this.attr("id"),
                            span = _this.find('span'),
                            data_text;
                        if(span.length) {
                            _this.find('span').remove();
                        }

                        data_text = _this.text();
                        ed.selection.setContent('<a class="member" href="http://regforum.ru/members/' + data_id + '/">' + data_text + '</a>');
                        user_dialog.dialog("close");
                        parents.html('').siblings('.top_filter').find('input').val('');
                    });
                }

            });
        },
        verify_html : true,
        inline_styles : false,
        onchange_callback : "calculate",
        handle_event_callback : "calculate",
        apply_source_formatting : true,
        convert_fonts_to_spans : true,
        //valid_children : "-div[style,class],-p[style],-span[style,class]",
        valid_elements : "a[href|target|class],strong,br,em,p[align|class],u,del",
        invalid_elements : "iframe,script,applet,object,h3,h4,h5,h6,h7,h8,h9,cite,button,section,head,header,footer,article,embed,pre,img,span,div,ol,ul,li",
        removeformat_selector : 'b,strong,em,i,span,ins,div,li,ul,h2',
        'formats' : {
            'italic' : {'inline' : 'em'},
            'bold' : {'inline' : 'strong'},
            'underline' : {'inline' : 'u'},
            'strikethrough' : {'inline' : 'del'}
        },
        removeformat : [
            {selector : 'div,b,strong,em,i,font,u,strike', remove : 'all', split : true, expand : false, block_expand : true, deep : true},
            {selector : 'span', attributes : ['style', 'class'], remove : 'empty', split : true, expand : false, deep : true},
            {selector : '*', attributes : ['style', 'class'], remove: 'all', split : true, expand : true, deep : true}
        ],
        protect: [
            /\<\/?(if|endif)\>/g, // Protect <if> & </endif>
            /\<xsl\:[^>]+\>/g, // Protect <xsl:...>
            /<\?php.*?\?>/g // Protect php code
        ]
    });

});