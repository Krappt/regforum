/**
 * Created with JetBrains PhpStorm.
 * User: RegforumTeam
 * Date: 28.08.14
 * Time: 13:59
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function () {

    var tagsBlock = $('.tags_form'),
        tagsList = tagsBlock.find('.additional_block'),
        block = tagsBlock.find('.main_block'),
        timeout_input, //id ������� �������
        search_timeout = 50; //�������� ��. �� ������



    block.find('.select_button').on('click', function(){
        var _this = $(this);
        if (_this.hasClass('active')) {
            _this.removeClass('active');
            _this.find('span.arrow_down').show();
            _this.siblings('div.objects_list').hide();
        } else {
            _this.addClass('active');
            _this.find('span.arrow_down').hide();
            _this.siblings('div.objects_list').show().find('input').focus();
        }
    });

    $(document).on('click', function(e){
        if(!$(e.target).closest(block).length){
            block.find('div.select_button').removeClass('active');
            block.find('div.objects_list').hide();
            block.find('span.arrow_down').show();
        }
    });

    block.on('click', '.objects_list li', function(){
        var _this = $(this);
        _this.addClass('hidden');
        addTag(_this, tagsList);
        _this.remove();
        cleanInputForm(this);
        closeObjectList(this);
    });

    block.find('div.objects_list').on('keyup', 'input', function(){
        var text = $(this).val(),
            ells = $(this).parents('.objects_list').find('ul li:not(.hidden)');
        clearTimeout(timeout_input);
        timeout_input = setTimeout(function() {
            ells.each(function(){
                var re = new RegExp('(^|\\s)' + text, 'ig'),
                    liText = $(this).text();
                var result = liText.match(re);
                if(result) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        }, search_timeout);
    });

    tagsList.on('click', '.remove', function() {
        moveBackTag(this, block);
        $(this).parent().remove();
    });

    block.find('.new_tag .ll').on('click', function() {
        $(this).parent().find('.add_tag').toggle();
    });

    block.find('.add_tag span.button').on('click', function() {
        var _this = this,
            name = $(_this).siblings('input').val();
        if(name){
            $.ajax('/moderator/tags/create/', {
                type: "POST",
                data: { name: name },
                cache: false,
                success: function(response) {
                    var answer = $.parseJSON(response);
                    if(answer.id && answer.name) {
                        $(_this).parents('.add_tag').toggle();
                        $(_this).siblings('input').val('');
                        addNewTag(answer.id, answer.name, block.find('.objects_list ul'));
                        $(_this).siblings('.errorMessage').text('');
                    } else if(answer.error) {
                        $(_this).siblings('.errorMessage').text(answer.error.name[0]);
                    }
                }
            });
        }
    });

    tagsList.sortable();

    /**
     * �� ����������� this (�� �������) ������� ������������ ���� � �������� ��� � �������������� ���������.
     * @param el - this
     */
    function closeObjectList(el) {
        var block = $(el).closest('.main_block');
        block.find('div.select_button').removeClass('active');
        block.find('div.objects_list').hide();
        block.find('span.arrow_down').show();
    }

    /**
     * �� ����������� this (�� �������) ������� ������������ ���� � ������� ���� ������.
     * @param el - this
     */
    function cleanInputForm(el) {
        var block = $(el).closest('.main_block').find('div.objects_list');
        block.find('input').val('');
        block.find('li').css('display', '');
    }

    /**
     * �� ����������� this (�� �������) ������� ��� � ��� �����
     * @param el - this
     * @param addTo - ���� ���������
     */
    function addTag(el, addTo) {
        var id = el.attr('id'),
            name = el.text(),
            html = '<div class="selected_tag border"><input type="hidden" value="'+ id + '" name="tags[]"><span class="selected">' + name + '</span><span class="remove right"></span><span class="sort right"></span></div>';
        addTo.append(html);
    }

    /**
     * ������� ����� ��� � ������ �����
     * @param id - id ������ ����
     * @param name - ���
     * @param block - ���� ���� ���������
     */
    function addNewTag(id, name, block) {
        if(id && name && block){
            var html = '<li id="' + id +'" class="new">' + name + '</li>';
            block.append(html);
            return true;
        } else {
            return false;
        }
    }

    /**
     * �� ����������� this (�� �������) ������� ��� � ��� ����� � ���������� � ������
     * @param el - this
     * @param backTo - ���� ���������
     */
    function moveBackTag(el, backTo) {
        var id = $(el).siblings('input').val();
        backTo.find('#'+id).removeClass('hidden');
    }

});