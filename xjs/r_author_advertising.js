/**
 * Created with JetBrains PhpStorm.
 * User: RegforumTeam
 * Date: 25.09.14
 * Time: 11:56
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function() {
    var fm = $('#flashMessage'),
        subscribeBlock = $('.author_advertising .subscribe-block');

    subscribeBlock.on('click', 'button', function(){
        var _this = $(this),
            authorId = _this.attr('author_id'),
            action = _this.hasClass('greens'),
            buttonClass = action ? 'alfa': 'greens';
        $.ajax('/profile/subscribe/update/', {
            type: "POST",
            data: { type: 'author', action: action, item_id: authorId },
            success: function(response){
                if(response && response.success){
                    _this.attr('class', 'button'); //������� ��� ������.
                    _this.addClass(buttonClass);
                    fm.addClass('sign_green').removeClass('sign_ruby').html('�� ������� ' + (action ? '����������� ��' : '���������� ��') + ' ������� ������');
                    fm.stop(true, true).fadeIn(200).delay(3000).fadeOut(200);
                } else {
                    fm.addClass('sign_ruby').removeClass('sign_green').html('��������, ��������� �������������� ������');
                    fm.stop(true, true).fadeIn(200).delay(3000).fadeOut(200);
                }
            }
        });
    });

    fm.hover(function(){fm.stop(true, true)}, function(){fm.fadeOut(200)});

});