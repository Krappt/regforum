/**
 * Created with JetBrains PhpStorm.
 * User: RegforumTeam
 * Date: 03.09.13
 * Time: 15:32
 * To change this template use File | Settings | File Templates.
 */
// Load the Visualization API and the piechart package.
google.load('visualization', '1.0', {'packages':['corechart']});

function metrikaInit(metrics_data, selector, title, chart_type, width, height) {

    //alignSiblingEls($("div.metrikaWrap div.head"));
    //alignSiblingEls($("div.metrikaWrap p"));
    //������� ��� ������������ ������
    function alignSiblingEls(els) {
        for(var i = 0; i < els.length; i++) {
            if(i % 2) {
                var firstEl = els[i - 1],
                    secondEl = els[i];
                $(firstEl).height() > $(secondEl).height() ? $(secondEl).height($(firstEl).height()) : $(firstEl).height($(secondEl).height());
            }
        }
    }
    //������ �������
    var blockSize  = {
        width: width || 450,
        height: height || 400
    };
    var metrics_data = JSON.parse(metrics_data);
    if(!metrics_data) return;
    var data = new google.visualization.DataTable();
    //������ � �������� ����������
    if(chart_type == 'PieChart') {
        var options = blockSize;
        options.pieSliceText = 'label';
        options.chartArea = {top:15,width:"95%",height:"85%"};
        data.addColumn('string', 'Name');
        data.addColumn('number', 'Percent');
        if(selector == 'data_gender') {
            for(var i = 0; i < metrics_data.data_gender.length; i++) {
                data.addRow([metrics_data.data_gender[i].name, metrics_data.data_gender[i].visits_percent])
            }
        }
        if(selector == 'data_age') {
            for(var i = 0; i < metrics_data.data.length; i++) {
                data.addRow([metrics_data.data[i].name, metrics_data.data[i].visits_percent])
            }
        }
        if(selector == 'search_engines') {
            for(var i = 0; i < metrics_data.data.length; i++) {
                var name = metrics_data.data[i].name,
                    index = name.indexOf(',');
                name = index == -1 ? name : name.substring(0, name.indexOf(','));
                data.addRow([name, metrics_data.data[i].visits])
            }
        }
        if(selector == 'poll_397') {
            options.pieSliceText = '';
            for(var i = 0; i < metrics_data.options.length; i++) {
                var name = metrics_data.options[i],
                    quantity = parseInt(metrics_data.votes[i]);
                data.addRow([name, quantity]);
            }
        }

        var chart = new google.visualization.PieChart(document.getElementById(selector));
        chart.draw(data, options);

    }
    //������ � ��������
    else if(chart_type == 'AreaChart'){
        var options = blockSize;
        //title: title,
        //isStacked: true,
        options.legend = 'none';
        options.pointSize = 8;
        options.vAxis = {minValue:0};
        if(options.width > 900) {
            options.chartArea = {left:60,top:20,width:"89%",height:"80%"};
        } else {
            options.chartArea = {left:50,width:"82%",height:"82%"};
        }
        //vAxis: {title: "����������", format:'###'},
        //hAxis: {title: "������"}
        data.addColumn('string', '����');
        data.addColumn('number', '����������');
        if(selector !='traffic')
            for(var i = 0; i < metrics_data.length; i++) {
                data.addRow([metrics_data[i].date, parseInt(metrics_data[i].quantity)]);
        } else if(selector =='traffic') {
            options.hAxis = {direction:-1};
            for(var i = 0; i < metrics_data.data.length; i++) {
                data.addRow([metrics_data.data[i].date.replace(/(\d{4})(\d{2})(\d{2})/g,'$3.$2.$1'), parseInt(metrics_data.data[i].visitors)]);
            }
        }

        var chart = new google.visualization.AreaChart(document.getElementById(selector));
        chart.draw(data, options);
    }
    //������ � ���������
    else if(chart_type == 'ChartWrapper') {
        var name = new Array(''),
            quantity = new Array(''),
            userid = new Array(),
            options = blockSize;
        options.vAxis = {minValue:0};
        if(options.width > 900) {
            options.chartArea = {left:65,width:"70%",height:"85%"};
        } else {
            options.chartArea = {left:65,width:"45%",height:"85%"};
        }
        for(var i = 0; i < metrics_data.length; i++) {
            if(metrics_data[i].first_name && metrics_data[i].last_name){
                name.push(metrics_data[i].first_name + ' ' + metrics_data[i].last_name);
            } else {
                name.push(metrics_data[i].username);
            }
            quantity.push(parseInt(metrics_data[i].quantity));
            userid.push(parseInt(metrics_data[i].userid));
        }
        var wrapper = new google.visualization.ChartWrapper({
            chartType: 'ColumnChart',
            dataTable: [name, quantity],
            options: options,
            containerId: selector
        });
        wrapper.draw();

        //��������� ��������� �������� � ���������� � ������� �� id
        function redirectLocation(e) {
            var regExp = /legendentry/;
            if(regExp.test(e.targetID)) {
                var id = e.targetID.replace(/\D/g, '');
                window.location = 'http://' + document.location.host + '/members/' + userid[id];
            }
        }
        google.visualization.events.addListener(wrapper.getChart(), 'click', redirectLocation); //moved for firefox named function bug

    } else if(chart_type == 'ColumnChart') {
        /*
        var options = blockSize;
        options.legend = 'none';
        options.vAxis = {minValue:0};
        options.hAxis = {direction:-1};
        if(options.width > 900) {
            options.chartArea = {left:50,top:20,width:"92%",height:"80%"};
        } else {
            options.chartArea = {left:50,width:"82%",height:"82%"};
        }
        data.addColumn('string', '����');
        data.addColumn('number', '��������');
        for(var i = 0; i < metrics_data.data.length; i++) {
            data.addRow([metrics_data.data[i].date.replace(/(\d{4})(\d{2})(\d{2})/g,'$3.$2.$1'), parseInt(metrics_data.data[i].visitors)]);
        }
        var chart = new google.visualization.ColumnChart(document.getElementById(selector));
        chart.draw(data, options);

         var date = metrics_data.data[i].date.replace(/(\d{4})(\d{2})(\d{2})/g,'$1.$2.$3');
         date = new Date(date);
         data.addRow([date, parseInt(metrics_data.data[i].visitors)]);
         */
    }

    //�������������� ���������
    $('.chartWrapper').find('text[text-anchor=start]').css({'cursor': 'pointer', 'fill' : '#4444ba'});
    $('.chartWrapper').find('text[text-anchor=start]').on('mouseover', function() {
        $(this).css({'text-decoration' : 'underline', 'fill' : '#5555e6'});
    }).on('mouseout', function() {
            $(this).css({'text-decoration' : 'none', 'fill' : '#4444ba'});
        });

}