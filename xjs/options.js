$(document).ready(function () {
    $('#UserProfile_opt_comments_view_mode input').click(function() {
        if($(this).val() == 'line') {
            $('#comments_order').show(300);
            $('#comments_collaps').hide(300);
        }
        else {
            $('#comments_order').hide(300);
            $('#comments_collaps').show(300);
        }
    });

});