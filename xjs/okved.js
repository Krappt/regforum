/**
 * Created with JetBrains PhpStorm.
 * User: RegforumTeam
 * Date: 20.06.13
 * Time: 10:16
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function() {
    var fm = $('#flashMessage');

    /*--------------------------------------Binding-----------------------------------------------------------------------*/
    $("ul.okvedTabs li:eq(0)").click(function() {
        window.location = '/okved/';
    });

    $("ul.okvedTabs li:eq(1)").click(function() {
        window.location = '/okved/razdel/';
    });

    $("form.okvedSearch button").click(function() {
        $("form.okvedSearch input").val() != '' ? $("form.okvedSearch").submit() : '';
        return false;
    });
//����� �������� ������� �������� � �� ��������� ����������� �����. ��������� ������ � �������
    $("tr.currOkved td.okvedText, div.currOkved div.okvedText").each(function() {
        var el = $(this),
            elText = el.html();
        var link = new RegExp('\\#+([0-9.]+)\\#+', 'gi');
        var linkText = elText.replace(link,'<a href="/okved/kod_okved_$1/">��� ����� $1</a>');
        if($('input.search').val() && !parseInt($('input.search').val())){
            var example = new RegExp('(' + $('input.search').val() + '[�-��-�]*)', 'gi'),
                newText = linkText.replace(example,'<span>$1</span>');
        } else {
            newText = linkText;
        }
        el.html(newText);
    });

    $('div.currOkved span.okvedAdd, tr.currOkved span.okvedAdd').click(function(){
        var elem = $(this).parent().parent();
        setOkvedCookie(elem);
    });

    $('div.currOkved, tr.currOkved').each(function(){
        checkCoocieOkvedId(this);
    });

    $('span.clearOkveds').click(function(){
        $('div.usersOkveds div').remove();
        setOkvedCookie(null, true);
        $('.clearOkvedsFooter').hide();
        $('.noOkvedSelectedInfo').show();
        $('.copyText').hide().find('textarea').text('');
    });

    $('.userOkved .remove').live('click', function(){
        var id = $(this).parent().attr('id'),
            el = $('.currOkved#'+id);
        $(this).parent().remove();
        if(el.length) { //���� ������� �� �������� ������� �� ���� � �� ��������
            setOkvedCookie(el);
        } else {
            removeUsersOkvedsCookies(id);   //������ �� ����
        }
        $('.copyText').hide();
    });

    $('.printOkveds').live('click', function() {
        if(typeof(yaCounter10978084) == 'object')
            yaCounter10978084.reachGoal('OKVED_PRINT');
        window.open('/okved/print/');
    });

    $( ".usersOkveds" ).sortable({
        update: function() {
            var ells = $('.usersOkveds .userOkved'),
                arr = new Array();
            for (var i = 0; i < ells.length; i++){
                arr.push($(ells[i]).attr('id'));
            }
            saveOkvedCookie(arr);
            $('.copyText').hide();
        }
    });

    $('.copyToClipboard').click(function() {
        if(typeof(yaCounter10978084) == 'object')
            yaCounter10978084.reachGoal('OKVED_COPY');
        var ells = $('.usersOkveds .userOkved'),
            text = '';
        for (var i = 0; i < ells.length; i++){
            text += $(ells[i]).find('a').text() + ', ';
        }
        if (text){
            var copyText = $('.copyText');
            text = text.slice(0, -2);
            copyText.show().find('textarea').text(text);
            $('.erasePoints').show();
            copyText.find('textarea').focus().select();
        }
    });

    $('.erasePoints').on('click', function() {
        var copyText = $('.copyText').find('textarea');
        copyText.text(copyText.text().replace(/\./g, ''));
        copyText.focus().select();
    });

    $('table.classes tr td:not(.star), table.searchResult tr td:not(.star)').on('click', function(){
        if(!getSelectedText())
            window.location.assign($(this).parent().find('a').attr('href'));
    });

    fm.hover(function(){fm.stop(true, true)}, function(){fm.fadeOut(200)});

    $('.user').on('click', function() {
        $(this).next().toggle();
    });

/*------------------------------------functions-----------------------------------------------------------------------*/
//�������������/������� id ������ � ����
// elem - ����������� �������, erase - ������� ������� �����(elem ����� ���� ������)
    function setOkvedCookie (elem, erase) {
        var name = 'selectedOkveds',
            id = elem ? $(elem).attr('id') : null,
            cookieExp = 100,
            value = null, //�� ��������� ������� ����
            cookies = $.cookie(name),
            okvedIds = cookies ? JSON.parse(cookies) : new Array(),
            idPosition = okvedIds.indexOf(id);
        if(elem && idPosition > -1  && !erase) {
            okvedIds.splice(idPosition, 1);
            value = JSON.stringify(okvedIds);
            $(elem).find('span.okvedAdd').removeClass('okvedSet').addClass('okvedNotSet');
            removeUserOkved(id);
            notification(elem, false);
        } else if (elem && !erase){
            okvedIds.push(id);
            value = JSON.stringify(okvedIds);
            $(elem).find('span.okvedAdd').removeClass('okvedNotSet').addClass('okvedSet');
            addUserOkved(id, elem);
            notification(elem, true);
        } else {
            $('.currOkved span.okvedAdd').removeClass('okvedSet');
        }
        value = value ? value : null;
        $.cookie(name, value, {expires: cookieExp, path: '/'});
    }
//��������� �� ������� � ����� id ������
// elem - ����������� �������
    function checkCoocieOkvedId(elem) {
        var id = $(elem).attr('id'),
            name = 'selectedOkveds',
            cookies = $.cookie(name),
            okvedIds = cookies ? JSON.parse(cookies) : new Array(),
            idPosition = okvedIds.indexOf(id);
        if (idPosition > -1) {
            $(elem).find('span.okvedAdd').addClass('okvedSet');
        }
    }
//��������� ���������������� ��� � ���� ����
// elem - ����������� �������, id ����� ������ � ��
    function addUserOkved(id, elem){
        if(!id) return;
        var link = $(elem).find('.okvedNum a').attr('href'),
            linkId = $(elem).find('.okvedNum a').attr('id'),
            linkText = linkId;
        var newElem = $('div.userOkvedModel').clone(true).removeClass('userOkvedModel');
        $(newElem).attr('id', id);
        $(newElem).find('a').attr('href', link);
        $(newElem).find('a').text(linkText);
        $('div.usersOkveds').append(newElem);
        $('.clearOkvedsFooter').show();
        $('.noOkvedSelectedInfo').hide();
    }
//������� ����� �� ���� ����� � � ������� ��������, ���� �����.
//id ����� ������ � ��
    function removeUserOkved(id){
        $('div.usersOkveds').find('div#'+id).remove();
        if($('div.usersOkveds div').length < 1) {
            $('.noOkvedSelectedInfo').show();
            $('.clearOkvedsFooter').hide();
        }
    }
//������� ����� �� �������
//id ����� ������ � ��
    function removeUsersOkvedsCookies(id) {
        var name = 'selectedOkveds',
            id = id || null,
            cookieExp = 100,
            okvedIds = JSON.parse($.cookie(name)) || new Array(),
            idPosition = okvedIds.indexOf(id);
        okvedIds.splice(idPosition, 1);
        var value = JSON.stringify(okvedIds);
        value = value ? value : null;
        $.cookie(name, value, {expires: cookieExp, path: '/'});
    }
//�������� ������ ������� ����� � �����
//arr - ������ ���� [101,2,32]
    function saveOkvedCookie(arr) {
        var name = 'selectedOkveds',
            cookieExp = 100,
            arr = typeof arr == "object" ? arr : new Array(),
            value = JSON.stringify(arr);
        value = value ? value : null;
        $.cookie(name, value, {expires: cookieExp, path: '/'});
    }
//�����������
//el ������������ ������� this, add (bool) ������� ��������
    function notification(el, add) {
        var el = $(el),
            id = el.find('a').attr('id');
        if(add){
            fm.addClass('sign_green').removeClass('sign_ruby').html('� ��� ������ ����� ����� �������� ��� ' + id + '<br \>�� ������ <a style="color:white;text-decoration: underline;" href="#selectedOkveds">������� � ������</a> ��� ���������� ����� �����.');
            fm.stop(true, true).fadeIn(200).delay(3000).fadeOut(200);
        } else {
            fm.addClass('sign_ruby').removeClass('sign_green').html('��� ' + id + ' ������� ������ �� ���������.');
            fm.stop(true, true).fadeIn(200).delay(3000).fadeOut(200);
        }
    }

    /**
     * getSelectedText
     * @returns {*}
     */
    function getSelectedText() {

        var result;

        if (window.getSelection) {
            result = window.getSelection().toString();
        } else if (document.getSelection) {
            result = document.getSelection();
        } else if (document.selection) {
            result = document.selection.createRange().text;
        }
        return result;
    }
});
