/**
 * Created with JetBrains PhpStorm.
 * User: Vasilevs
 * Date: 27.12.12
 * Time: 17:09
 */

function SaveMembersSort(company_id) {
    //console.info($('#sort_data').text());
    var members_list = $('#sort_data').text();
    $.ajax('/profile/companies/setmemberssort/', {
        type: "GET",
        data: { company_id: company_id, indexes: members_list },
        cache: false,
        success: function(response){
            $('#sort_button').hide();
            $('#add_button').show();
        }
    });
}

function EditMember(member, company, status) {
    if(!status) status = '';
    //console.info('edit');
    $('div #'+member).find('.member_status').html('<input size="30" maxlength="100" id="statusedit" type="text" value="'+status+'"><div class="jsb member_save ssave" onclick="SaveMember('+member+','+company+')"></div>');
    //$('div #'+member).find('.member_controls').html('');

}

function SaveMember(member, company) {
    //console.info('save');
    var status = $('#statusedit').val();
    var caption = status;
    $.ajax('/profile/companies/savemember/', {
        type: "GET",
        data: { m: member, s: status, c: company },
        cache: false,
        success: function(response) {
            if(!response.result) {
                caption = 'Не удалось сохранить должность';
            }
            $('div #'+member).find('.member_status').html('<span class="ll" onclick="EditMember('+member+','+company+',\''+status+'\')">'+caption+'</span>');
        }
    });
}

function DeleteMember(member, company) {
    //console.info('del');
    if (confirm("Вы уверены что хотите удалить этого участника из компании ?"))
        $.ajax('/profile/companies/deletemember/', {
            type: "GET",
            data: { company_id: company, user_id: member },
            cache: false,
            success: function(response){
                $('#'+member).fadeOut(400);
            }
        });
}

$(document).ready(function () {

    var user_dialog = $('#user_dialog');

    if (user_dialog.length ){
        user_dialog.dialog({
            modal: true,
            autoOpen: false,
            closeOnEscape: true,
            resizable: false,
            title: "Ссылка на участника",
            height: 500,
            width: 320,
            buttons: {
                "Закрыть": function() {
                    $(this).dialog("close");
                }
            }
        });
    }

$("#member_name").focus(function() {
    var user_dialog = $('#user_dialog');
    // диалог для пользователей
    if (!user_dialog.dialog('option', 'disabled')) {

        //получаем весь список
        $.getJSON('/posts/getusers/', { fastsearch: '' },
            function(obj) {
                if (obj) {
                    $("#user_dialog_content").html(obj._content);
                }
            });
        user_dialog.dialog("open");
    }

    $(".dialog_filter").live('keyup', function() {
        var fastsearch = $(this).val();
        //$("div.top_filter").addClass('load');
        $.getJSON('/posts/getusers/', { fastsearch: fastsearch },
            function(obj) {
                if (obj) {
                    $("#user_dialog_content").html(obj._content);
                    //$("div.top_filter").removeClass('load');
                }
            });
    });

    $('#user_dialog_content p').live('click', function() {
        var data_id = $(this).attr("id");
        var data_text = $(this).text();
        $('#member_name').val(data_text);
        $('#member').val(data_id);
        user_dialog.dialog("close");
    });
});

    $("#fds").sortable({
        listType: 'div',
        stop: function() {
            var pos = new Array;
            $(this).children().each(function(){
                pos.push($(this).attr("id"));
            });
            $('#sort_data').text(pos.join(","));
            if(!$('#sort_button').is(':visible')) $('#sort_button').show();
            $('#add_button').hide();
        }
    });

});