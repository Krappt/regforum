$(function () {

    var getAllAuthors,
        showAllAuthors,
        searchForm = '.user_for_search',
        userInput = $(searchForm + ' .user_search input'),
        userButton = $(searchForm + ' .user_search button'),
        usersTimeout,
        dropdown = $('.dropdown');

    if ($('.best_commenters').length) {
        (function () {
            var bestCommenters = $('.best_commenters');
            bestCommenters.each(function () {
                var widget = $(this),
                    ctrls,
                    contents,
                    loader;

                ctrls = widget.find('.boxfooter span.ll');
                contents = widget.find('.top');
                loader = widget.find('.ajaxLoader');
                ctrls.click(function () {
                    if (!$(this).is('.active')) {
                        ctrls.toggleClass('notactive').toggleClass('active');
                        contents.each(function () {
                            $(this).toggle();
                        });
                        if ($(this).is('.alltime') && !$.trim(widget.find('.all').html())) {
                            loader.show();
                            $.post('/posts/getalltimebestcommenters/', function (response) {
                                if (response.data) {
                                    widget.find('div.all').append($(response.data).find('.month').html());
                                    loader.hide();
                                }
                            });
                        }
                    }
                    return false;
                });
            });
        })();
    }

    showAllAuthors = function () {
        $('.best_authors div.boxcnt div.month').hide();
        $('.best_authors div.boxcnt div.all').show();
        $('.best_authors div.boxfooter span#all').addClass('active').removeClass('notactive');
        $('.best_authors div.boxfooter span#month').removeClass('active').addClass('notactive');
    };

    getAllAuthors = function () {
        $('.best_authors div.boxtitle span.ajaxLoader').show();
        $.ajax('/posts/getallauthors/', {
            type: "GET",
            cache: false,
            success: function (response) {
                var data = response.data;
                if (data) {
                    $('.best_authors div.boxcnt div.all').append(data);
                }
                else {
                    console.log('ERROR: data has some Ajax error');
                }
                $('.best_authors div.boxtitle span.ajaxLoader').hide();
            }
        });
    };

    $('.best_authors div.boxfooter span#all').click(function () {
        if (!$.trim($('.best_authors div.all').html()).length) {
            getAllAuthors();
        }
        showAllAuthors();
    });
    $('.best_authors div.boxfooter span#month').click(function () {
        $('.best_authors div.boxcnt div.all').hide();
        $('.best_authors div.boxcnt div.month').show();
        $(this).addClass('active').removeClass('notactive');
        $('.best_authors div.boxfooter span#all').removeClass('active').addClass('notactive');
    });

    dropdown.on('click', function () {
        $(this).find('.dropdown-menu').toggle();
    });

    userInput.length && userInput.on('keyup', function () {
        var text = $(this).val().replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1"),
            ells = $(searchForm + ' .user');

        text ? userButton.show() : userButton.hide();

        clearTimeout(usersTimeout);
        usersTimeout = setTimeout(function () {
            ells.each(function () {
                var re = new RegExp(text, 'ig'),
                    elText = $(this).text();
                var result = elText.match(re);

                result ? $(this).show() : $(this).hide();
            });
        }, 100);
    });

    userButton.length && userButton.on('click', function () {
        userInput.val('').keyup();
    });

    $(document).on('click', function (e) {
        if (!$(e.target).closest(dropdown).length) {
            dropdown.find('.dropdown-menu').hide();
        }
    });
});