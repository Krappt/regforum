$(document).ready(function () {

    tinyMCE.init({
        mode : "exact",
        elements : "AdminUserProfile_advertising_text, AdminUserProfile[advertising_text_original], "
            + "UserProfile_advertising_text_original, UserProfile_advertising_text, "
            + "Comments_text, "
            + "FrontpageController_announcement, "
            + "FilesUpdate_file_description, "
            + "PaidAuthors_text, "
            + "Responses_good_content, Responses_bad_content",
        theme : "advanced",
        language : "ru",
        plugins : "paste,preview,fullscreen,searchreplace,inlinepopups",
        width: "100%",
        height: "460",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,upper_�ase,lower_�ase,outdent,indent,blockquote,inquotes,|,search,replace,|,pasteword,|,bullist,numlist,|,undo,redo,|,image,insertimage,link,unlink", //
        theme_advanced_buttons2 : "styleselect,formatselect,|,removeformat,preview,code,fullscreen",
        theme_advanced_buttons3 : "",
        theme_advanced_buttons4 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : false,
        convert_urls : false,
        theme_advanced_blockformats : "p,h2,blockquote",
        content_css : "/xcss/tinymce_body.css",
        onchange_callback: $.regforum.profile.companies.add.editorCounter,
        handle_event_callback: $.regforum.profile.companies.add.editorCounter,
        setup : function(ed) {
            ed.addButton('upper_�ase', {
                title : '������������� � ������� ��������',
                image : '/ximg/toolbar/edit-uppercase.png',
                onclick : function() {
                    ed.focus();
                    ed.selection.select();
                    ed.selection.setContent(ed.selection.getContent().toUpperCase());
                }
            });
            ed.addButton('lower_�ase', {
                title : '������������� � ������ ��������',
                image : '/ximg/toolbar/edit-lowercase.png',
                onclick : function() {
                    ed.focus();
                    ed.selection.select();
                    ed.selection.setContent(ed.selection.getContent().toLowerCase());
                }
            });
            ed.addButton('inquotes', {
                title : '� ��������',
                image : '/ximg/toolbar/in_quotes.png',
                onclick : function() {
                    ed.focus();
                    ed.selection.select();
                    ed.selection.setContent('&laquo;'+ed.selection.getSel()+'&raquo;');
                }
            });
        },
        verify_html : true,
        inline_styles : false,
        apply_source_formatting : true,
        convert_fonts_to_spans : true,
        //valid_children : "-div[style,class],-p[style],-span[style,class]",
        valid_elements : "a[href|target|class|title],strong,br,img[src|width|height|align|hspace|vspace],em,p[align|class],h2,blockquote,u,ol,ul,li,del,table,td,tr",
        invalid_elements : "iframe,script,applet,object,h3,h4,h5,h6,h7,h8,h9,cite,button,section,head,header,footer,article,embed,pre",
        removeformat_selector : 'b,strong,em,i,span,ins,div,li,ul,h2',
        style_formats : [
            {title : '����', selector : 'p', classes : 'incut'}
        ],
        'formats' : {
            'alignleft' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-left'},
            'aligncenter' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-center'},
            'alignright' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-right'},
            'alignfull' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-justify'},
            'italic' : {'inline' : 'em'},
            'bold' : {'inline' : 'strong'},
            'underline' : {'inline' : 'u'},
            'strikethrough' : {'inline' : 'del'}
        },
        removeformat : [
            {selector : 'div,b,strong,em,i,font,u,strike', remove : 'all', split : true, expand : false, block_expand : true, deep : true},
            {selector : 'span', attributes : ['style', 'class'], remove : 'empty', split : true, expand : false, deep : true}
            //{selector : '*', attributes : ['style', 'class'], remove: 'all', split : true, expand : true, deep : true}
        ],
        protect: [
            /\<\/?(if|endif)\>/g, // Protect <if> & </endif>
            /\<xsl\:[^>]+\>/g, // Protect <xsl:...>
            /<\?php.*?\?>/g // Protect php code
        ]
    });

});