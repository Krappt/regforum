$(document).ready(function () {
    var mainBlock = $('.my_companies .nav.nav-tabs'),
        tabContent = $('.tab-content'),
        addMember = '.add-member',
        selectMember = addMember + ' .select-member',
        selectMemberInput = selectMember + ' input',
        companyId,
        sortData = [],
        userId,
        timeout;

    mainBlock.on('click', 'li', function () {
        var _this = $(this),
            parentBlock = _this.closest(mainBlock),
            companyBlock = parentBlock.parent(),
            id = companyBlock.data('company-id'),
            a = _this.find('a'),
            type = a.data('type'),
            relatedBlock = parentBlock.siblings('.tab-content').find(a.attr('href'));

        if (relatedBlock.is(':empty')) {
            relatedBlock.addClass('loading');
            $.ajax('/profile/companies/getloopdata/', {
                type: "GET",
                data: { id: id, type: type },
                success: function (response) {
                    relatedBlock.html(response);
                    relatedBlock.find('.col-md-8').dragsort({
                        dragSelector: "div",
                        dragEnd: function() {
                            $(this).siblings().andSelf().each(function(){
                                sortData.push($(this).data("id"));
                            });
                            companyId = id;
                            companyBlock.find('.sort-button').show();
                        }
                    });
                },
                statusCode: {
                    404: function (xhr) {
                        if (window.console) console.error(xhr.responseText);
                    }
                }
            }).done(function () {
                    relatedBlock.removeClass('loading');
                });
        }
    });

    tabContent.on('click', '.sort-button', function(){
        var _this = $(this);
        $.ajax('/profile/companies/setmemberssort/', {
            type: "GET",
            data: { company_id: companyId, indexes: sortData },
            cache: false,
            success: function(response){
                _this.hide();
            }
        });
    });

    tabContent.on('keyup', selectMemberInput, function() {
        var _this = $(this);
        clearTimeout(timeout);
        timeout = setTimeout(function() {
            var name = _this.val();

            if(name.length < 2 )
                return;

            $.getJSON('/posts/getusers/', { fastsearch: name },
                function(obj) {
                    if (obj) {
                        _this.siblings('div').html(obj.content);
                        _this.siblings('.data').show();
                    }
                });
        }, 1000);
    });

    tabContent.on('click', selectMember + ' .data > div', function() {
        var _this = $(this);
        _this.parent().hide();
        $(selectMemberInput).val(_this.text());
        userId = _this.data('id');
    });

    tabContent.on('click', addMember + '>.btn', function () {
        var _this = $(this),
            status = _this.parents(addMember).find('input.status').val(),
            companyId = _this.parents('.company').data('company-id');
        $.ajax('/profile/companies/addmember/', {
            type: "GET",
            data: { member_id: userId, company_id: companyId, status: status },
            success: function (response) {
                if(!response.warning) {
                    _this.parents('.tab-pane').find('.members > input').before(response);
                } else if(response.warning){
                    console.log(response.warning);
                }
                _this.parents(addMember).find('input[type="text"]').val('');
            },
            statusCode: {
                404: function (xhr) {
                    if (window.console) console.error(xhr.responseText);
                },
                500: function (xhr) {
                    if (window.console) console.error(xhr.responseText);
                }
            }
        }).done(function () {
                //relatedBlock.removeClass('loading');
            });

    });

    tabContent.on('click', '.members .glyphicon-remove-circle', function () {
        var _this = $(this),
            companyId = _this.parents('.company').data('company-id'),
            companyName = _this.parents('.company').find('>a').text(),
            userId = _this.parent().data('id'),
            name = _this.siblings('a').text();
        if (confirm("Вы уверены что хотите удалить участника " + name + " из компании " + companyName + "?"))
            $.ajax('/profile/companies/deletemember/', {
                type: "GET",
                data: { companyId: companyId, userId: userId },
                cache: false,
                success: function(response){
                    _this.parent().fadeOut(400);
                }
            });
    });

});