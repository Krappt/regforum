/**
 * Created with JetBrains PhpStorm.
 * User: RegforumTeam
 * Date: 12.09.14
 * Time: 10:30
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function () {
    var spoiler = $('.spoiler'),
        toggle = spoiler.find('.toggle'),
        data = spoiler.find('.data');

    toggle.on('click', function() {
        var main = $(this).closest(spoiler);
        main.toggleClass('open');
        main.find(toggle).toggle();
        main.find(data).toggle();
    });

});