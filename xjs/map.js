/**
 * Created with JetBrains PhpStorm.
 * User: Vasilevs
 * Date: 14.02.13
 * Time: 16:21
 */

ymaps.ready(init);
var myMap;

function init () {
    myMap = new ymaps.Map('map', {
        center: [64,105],
        zoom: 3,
        behaviors: ['default', 'scrollZoom']});

    myMap.controls
        .add('zoomControl')
        .add('mapTools');

    myMap.events.add('mousedown', function (e) {
        //console.info(e.get('coords'));
    });

    jQuery.ajax(
        {
            url:'http://'+ location.host +'/map/getcoordinates',
            cache: false,
            dataType: 'json',
            success: function(data){
                var myClusterer = new ymaps.Clusterer({
                    gridSize: 128
                });

                data.forEach(function (point) {
                    var myGeoObjects = new ymaps.Placemark(point.coordinates.split(','), {
                        //iconContent : point.company_id,
                        name:point.name,
                        address: '<p><strong>�����:</strong> '+point.address+'</p>',
                        link: point.link,
                        id : point.company_id,
                        data: null,
                        phones: point.phones
                    }); //{ preset: 'twirl#greenStretchyIcon' }
                    //myMap.geoObjects.add(myGeoObjects);

                    myClusterer.add(myGeoObjects);
                    myMap.geoObjects.add(myClusterer);

                    //������� ��� ����� �� �����
                    myGeoObjects.events.add('click', function(e) {
                        //�������� id ��������
                        var id = myGeoObjects.properties.get('id');
                        $.ajax(
                            {
                                url:'http://'+ location.host +'/map/getcompanymembers',
                                cache: false,
                                data: {id: id},
                                type: 'GET',
                                success: function(data) {
                                    //console.info('success');
                                    //var member = '<a href="/members/'+data.member_id+'/">'+data.member_name+'</a>';
                                    myGeoObjects.properties.set('data', data);
                                }
                            });
                    });
                    myGeoObjects.events.add('balloonopen', function() {
                        //console.info(myGeoObjects.properties.get('id')+'open');
                        //GetExtBalloonData(myGeoObjects.properties.get('id'),myGeoObjects);
                    });

                });

                //console.info('SetCoordiantes:'+coord);


                // ������� ������ ������
                var myBalloonLayout = ymaps.templateLayoutFactory.createClass(
                    '<div class="company_balloon">' +
                    '<div class="title"><a href="$[properties.link]">$[properties.name]</a></div>' +
                    '$[properties.address]' +
                    '<p><strong>�������:</strong> $[properties.phones]</p>' +
                    //'<p><small><a href="$[properties.link]">��������</a></small></p>' +
                    '<div id="data">$[properties.data]</div>' +
                    '</div>'
                );

                // �������� ��������� ������ � ��������� ��������. ������ ��� ������ �������� �� ����� 'my#superlayout'.
                ymaps.layout.storage.add('my#superlayout', myBalloonLayout);

                // ������ ��� ������ ��� ������� ���������� ���������.
                myMap.geoObjects.options.set({
                    balloonContentBodyLayout:'my#superlayout',
                    // ������������ ������ ������ � ��������
                    balloonMaxWidth: 300
                });
            }
        });
}

function GetExtBalloonData(id,GeoObjects) {
    GeoObjects.properties.set('data', 'test');
}


/*// ���������� ������� � �����
ymaps.route([myGeoObjects.properties.get('metro'),point.coordinates.split(',')], {
    mapStateAutoApply: true
}).then(function(route) {
        //      myMap.geoObjects.add(route);
        routeLength = route.getLength();
        // ����� �������� � ������.
        firtstPathTime = route.getTime();
        // ����� ��� ����� ������
        firstPathFirstSegmentJamsTime = route.getJamsTime();
        // ����� � ��������
        route.getPaths().options.set({
            strokeColor: '0000ffff',
            opacity: 0.9
        });
        console.info('Length: '+routeLength+'m');
        console.info('Time: '+firtstPathTime);
        console.info(route.getHumanLength());
        myMap.geoObjects.add(route);
    }, function(err) {
        alert('������� �� ��������');
    });*/

$(document).ready(function () {

    var points =  {
        77 : [55.66, 37.48],
        78 : [59.93, 30.32],
        54 : [55.02, 82.91],
        66 : [56.83, 60.61],
        43 : [55.79, 49.11],
        63 : [53.19, 50.10]

    };

    $(".r-menu > li[id!='rf']").click(function() {
        var coord = points[$(this).attr("id")];
        myMap.panTo(coord, {delay:10,duration: 1000,callback: function () {
            myMap.setZoom(10);
        }
        });
    });

    $("#rf").click(function() {
        myMap.panTo([64,105], {delay:10,duration: 1000,callback: function () {
            myMap.setZoom(3);
        }
        });
    });

});