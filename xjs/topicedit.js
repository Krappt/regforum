/**
 * Created by JetBrains PhpStorm.
 * User: Vasilevs
 * Date: 21.08.12
 * Time: 12:55
 */

function calculate(obj,max){

    var count = $(obj).val().length,
        available = max - count;

    $('#counter').html(((count < max - 200) ? '�� ������� ' + count : '�������� ' + available) + ' ��.');
}

$(document).ready(function () {

    var topic_form = $('#topic_form'),
        file_list = [],
        files = '',
        list_dialog = $('#list_dialog'),
        user_dialog = $('#user_dialog'),
        topic_editor = $('#topic_editor'),
        fileAdd = $('.addFileForm'),
        imageAdd = $('.addImageForm'),
        fileAddOptions = {
            beforeSubmit:fileBeforeSubmit,
            success: fileResponse
        },
        imageAddOptions = {
            beforeSubmit:imageBeforeSubmit,
            success: imageResponse
        },
        topic_log = $('#Topic_topic_log'),
        saveDataTimeout,
        getUsersTimeout,
        topic_id = $('#Topic_topic_id').val(),
        name_selector = $('#Topic_topic_title'),
        content_selector = $('#topic_content'),
        name = localStorage.getItem('postName'),
        content = localStorage.getItem('postContent'),
        page_preview_dialog= $('#page_preview_dialog'),
        preview = $('span.preview');

    //�������������� ����������� ������
    !topic_id && name && name_selector.val(name);
    !topic_id && content && content_selector.val(content) && $.regforum.helpers.flash('���������� �������������!', true);

    if(topic_id) {
        $(document).on('change', 'input', function(){
            content_selector.text(tinyMCE.get('topic_content').getContent());
            topic_log.length && topic_log.text(tinyMCE.get('Topic_topic_log').getContent());
            topic_form.ajaxSubmit();
        });
    }

    page_preview_dialog.length && preview.on('click', function(){
        var _this = $(this);
        content_selector.text(tinyMCE.get('topic_content').getContent());
        topic_log.length && topic_log.text(tinyMCE.get('Topic_topic_log').getContent());
        topic_form.ajaxSubmit({
            url:'/posts/preview/',
            success: function(responce){
                page_preview_dialog.html($(responce).find('h1, .company_blog, .topic_text, .tags'));
                $(page_preview_dialog).dialog("open");
                _this.removeClass('disabled');
            },
            error:function(responce){
                _this.removeClass('disabled');
            }
        });
    });

    if (page_preview_dialog.length) {
        page_preview_dialog.dialog({
            modal: true,
            autoOpen: false,
            closeOnEscape: true,
            resizable: false,
            title: "��������������� �������� ����������",
            height: 600,
            width: 664,
            show: "fade",
            hide:"fade",
            buttons: {
                "�������": function () {
                    $(this).dialog("close");
                }
            }
        });
    }

    //���������� ����������� �������� ����� ��� POST! ������
    $('.buttons input').on('click', function(e){
        var code = e.keyCode || e.which;
        var _this = $(this);
        if(!_this.hasClass("disabled") || !code) {
            _this.addClass("disabled");
            return true;
        }
        return false;
    });

    if (fileAdd.length) {
        fileAdd.dialog({
            modal: true,
            autoOpen: false,
            closeOnEscape: true,
            resizable: false,
            title: "�������� �����",
            height: 700,
            width: 700,
            show: "fade",
            hide:"fade",
            buttons: {
                "���������": {
                    text:'���������',
                    id: "saveBtn",
                    click: function () {
                        $("#add_form #Files_file_description").val(tinyMCE.activeEditor.getContent());
                        $('#add_form').ajaxSubmit(fileAddOptions);
                    }
                },
                "�������": function () {
                    $(this).dialog("close");
                    clearFileAddForm(fileAdd);
                }
            }
        });
    }

    if (imageAdd.length) {
        imageAdd.dialog({
            modal: true,
            autoOpen: false,
            closeOnEscape: true,
            resizable: false,
            title: "�������� ��������",
            height: 310, //������ ���� ���� ��� explorer-a
            width: 600,
            show: "fade",
            //hide:"fade",
            buttons: {
                "���������": {
                    text:'���������',
                    id: "saveBtn",
                    click: function () {
                        $(this).find('form').ajaxSubmit(imageAddOptions);
                    }
                },
                "�������": function () {
                    $(this).dialog("close");
                    clearFileAddForm(imageAdd);
                }
            }
        });
    }

    if (list_dialog.length) {
        list_dialog.dialog({
            modal: true,
            autoOpen: false,
            closeOnEscape: true,
            resizable: false,
            title: "������ ����������� ������",
            height: 500,
            show: "fade",
            //hide:"fade",
            buttons: {
                "�������": function () {
                    $(this).dialog("close");
                }
            }
        });
    }

    topic_editor.keyup(function () {
        calculate(this, 35000)
    }).change(function () {
        calculate(this, 35000)
    });

    var warning = $(".message");

    $('#rubricator').length && $('#rubricator').multiselect({
        noneSelectedText: '�������...',
        show: ['fade', 300],
        hide: ['fade', 300],
        selectedText: '������� # �� 3',
        minWidht: 160,
        header: "�� ������ ������� �� ����� ���� ��������",
        click: function(e){
            if( $(this).multiselect("widget").find("input:checked").length > 3 ){
                warning.addClass("error").removeClass("success").html("�� ������ ������� �� ����� ���� ��������");
                return false;
            } else {
                warning.addClass("success").removeClass("error").html("Check a few boxes.");
            }
        }
    });

	// ������ ��� ��������
	$("#add_attached").click(function() {
        $(fileAdd).dialog("open");
    });

    // ������ ��� ��������
    $(".select_file").click(function () {
        if (!list_dialog.dialog('option', 'disabled')) {

            //�������� ���� ������
            $.getJSON('/posts/getfiles', { fastsearch: '' },
                function (obj) {
                    if (obj) {
                        $("#dialog_content").html(obj._content);
                    }
                });
            list_dialog.dialog("open");
        }
    });
	
	//������� ��� ������
	$('#dialog_content p').live('click', function() {
		var data_id = $(this).attr("id");
		var data_text = $(this).text();
		files = $('#attached_files').val();
		file_list = files.split(' ');
		if (file_list.join(' ').indexOf(data_id)<0) {
			file_list.push(data_id);
			$('#attached_files').val(file_list.join(' ').trim());
			$('#files').append('<div id="'+data_id+'">'+data_text+' <div id="file_delete" class="file_delete">&nbsp;</div></div>');
			//console.info(file_list.join(' '));
			//console.info(data_text);
		}
		list_dialog.dialog("close");
	});	
	

    $('#file_delete').live('click', function() {
        files = $('#attached_files').val();
        file_list = files.split(' ');
        var id_to_delete = $(this).parent('div').attr('id');
        for (i=0; i < file_list.length; i++){
            if (id_to_delete == file_list[i]){
                file_list.splice(i, 1);
                --i;
            }
        }
        $('#attached_files').val(file_list.join(' '));
        $(this).parent('div').remove();
    });

    $('#Topic_topic_type input').click(function() {
        //$('#Profile_opt_comments_view_mode input').click(function() {
        if($(this).val() != 'common') {
        //if($(this).prop("checked")) {
            $('#corporate_post').show('fast');
        }
        else {
            $('#corporate_post').hide('fast');
        }
        //console.info($(this).val());
    });

    $(document).on("click touchend", '.topic_kind span, .file_class span', function() {
        var _this = $(this);
        _this.removeClass('gray');
        _this.parent().find('input:hidden').val(_this.attr('id'));
        _this.siblings('span').not('input, .gray').addClass('gray');
    });

    var currentSelector =  $('.topic_kind input').val();
    if (currentSelector){
        $('.topic_kind').find('span.' + currentSelector).removeClass('gray');
    }
    //�������� ������� �� �����
    $('#add_file').on('click', function() {
        $('.addFileForm').dialog("open");
    });
    //�������� ������� �� �����
    $('.addFileForm #cancel').on('click', function() {
        $('.addFileForm').hide();
    });

    if (user_dialog.length ){
        user_dialog.dialog({
            modal: true,
            autoOpen: false,
            closeOnEscape: true,
            resizable: false,
            title: "������ �� ���������",
            height: 500,
            buttons: {
                "�������": function() {
                    $(this).dialog("close");
                }
            }
        });
    }
    //������� ��� ������ ����� ��������
    $(imageAdd).find('#upload_image').on('change', function(e) {
        var file = e.target.files || e.dataTransfer.files,
            _this = $(this),
            val = _this.val(),
            form = _this.parents('form'),
            dialog_form = form.parent().parent();
        if(val) {
            if (file && file[0].type.indexOf("image") == 0) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    setImagePreview(form, e.target.result);
                };
                form.find('#upload_image_error').text('');
                dialog_form.find('#saveBtn').attr('disabled', false);
                reader.readAsDataURL(file[0]);
            } else {
                dialog_form.find('#saveBtn').attr('disabled', 'disabled');
                form.find('#upload_image_error').text('���� ������ ���� ��������� �������: jpg, png, gif, bmp');
                setImagePreview(form);
            }

        } else {
            setImagePreview(form);
        }
    });

    $('#uploadfilename_preview').on('click',function() {
        $(this).parents('form').find('#upload_image').trigger('click')});


    $('#hot').on('click', function(){
        $('.topic_hot_title').toggle();
    });

    if(topic_id) {
        topic_form.on('change', 'input', function(){
            updateData();
        });
    } else {
        name_selector.on('keyup', function(){
            saveData(tinyMCE.get('topic_content'));
        });
    }

    //������� ������� ajax ������ � ������� ��� ��������
    function fileResponse(responseText, statusText, xhr, $form) {
        var answer = $.parseJSON(responseText);
        if (answer.errors && answer.errors instanceof Object) {
            var keys =  Object.keys(answer.errors);
            for(var i = 0; i < keys.length; i++) {
                $('#' + keys[i]).text(answer.errors[keys[i]][0]);
            }
        } else if(answer.file && answer.file instanceof Object) {
            var data_id = answer.file.id;
            var data_text = answer.file.name;
            files = $('#attached_files').val();
            file_list = files.split(' ');
            if (file_list.join(' ').indexOf(data_id)<0) {
                file_list.push(data_id);
                $('#attached_files').val(file_list.join(' ').trim());
                $('#files').append('<div id="'+data_id+'">'+data_text+' <div id="file_delete" class="file_delete">&nbsp;</div></div>');
            }
            list_dialog.dialog("close");
            $(fileAdd).dialog("close");
            clearFileAddForm(fileAdd);
        }
        enableButtons(fileAdd, true);
    }
    //������� ������� ajax ������ � ������� ��� ��������
    function imageResponse(responseText, statusText, xhr, $form) {
        var answer = $.parseJSON(responseText),
            link,
            height,
            width;
        if (answer.errors && answer.errors instanceof Object) {
            var keys =  Object.keys(answer.errors);
            for(var i = 0; i < keys.length; i++) {
                $('#' + keys[i]).text(answer.errors[keys[i]]);
            }
            enableButtons(imageAdd, true);
            return;
        } else if(answer.image && answer.image instanceof Object && answer.image.link != undefined) {
                link = answer.image.link;
                height = answer.image.height;
                width = answer.image.width;
        } else if(answer.original && answer.original instanceof Object && answer.original.link != undefined) {
                link = answer.original.link;
                height = answer.original.height;
                width = answer.original.width;
        }
        var id = answer.id != 'undefined' ? answer.id : null;
        if(answer.original && answer.original instanceof Object && answer.original.link != undefined) {
            tinyMCE.activeEditor.selection.setContent('<a href="' + answer.original.link + '" target="_blank"><img src="' + link + '" id="' + id + '"' + ' height="' + height + '" width="' + width + '" /></a>');
        } else {
            tinyMCE.activeEditor.selection.setContent('<img src="' + link + '" id="' + id + '"' + ' height="' + height + '" width="' + width + '" />');
        }
        imageAdd.dialog("close");
        clearFileAddForm(imageAdd);
        enableButtons(imageAdd, true);
    }

    //�������� �� ajax �������
    function fileBeforeSubmit() {
        enableButtons(fileAdd, false);
    }
    //�������� �� ajax �������
    function imageBeforeSubmit() {
        enableButtons(imageAdd, false);
    }
    //������� ����� �� ��������� ������
    function clearFileAddForm(form) {
        $(form).find('form').trigger( 'reset' );
        $(form).find('.errorMessage, #uploadfilename').text('');
        $(form).find('#uploadbutton').text('������� ����');
        var image_form = $(form).find('#upload_image');
        if(image_form) {
            setImagePreview(form);
        }
    }
    //���������/��������� ������ � ����� ��� �� ������������� ������� ����.
    function enableButtons(elem, bool) {
        var saveBtn =$(elem).parent().find("#saveBtn span"),
            buttons = $(elem).parent().find("button");
        if(!bool) {
            $(buttons).each(function() {
                $(this).attr('disabled', true);
            });
            $(saveBtn).css('background', '#e6e6e6 url(/ximg/ajax-loader.gif) 50% 50% no-repeat');
            $(saveBtn).css('color', 'transparent');
            $(saveBtn).css('background-size', '75%');
        } else {
            $(buttons).each(function() {
                $(this).attr('disabled', false);
            });
            $(saveBtn).css('background','');
            $(saveBtn).css('color','');
            $(saveBtn).css('background-size','');
        }
    }

    //������������� ��������/�������� �� ���������
    function setImagePreview(form, src){
        if(src) {
            $(form).find('#uploadfilename_preview').attr('src', src);
        } else {
            $(form).find('#uploadfilename_preview').attr('src', '/ximg/no_image.png');
        }
    }
    //������������� tinyMCE
    var options = {
        mode : "exact",
        theme : "advanced",
        gecko_spellcheck: true,
        language : "ru",
        plugins : "paste,preview,fullscreen,searchreplace,inlinepopups",
        width: "100%",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,upper_�ase,lower_�ase,outdent,indent,blockquote,inquotes,user,|,search,replace,|,pasteword,|,bullist,numlist,|,undo,redo,|,youtube,|,image,insertimage,link,unlink", //
        theme_advanced_buttons2 : "styleselect,formatselect,|,removeformat,preview,code,fullscreen,|,topiccut",
        theme_advanced_buttons3 : "",
        theme_advanced_buttons4 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : false,
        convert_urls : false,
        theme_advanced_blockformats : "p,h2,blockquote",
        content_css : "/xcss/tinymce_body.css?12",
        onchange_callback: $.regforum.profile.companies.add.editorCounter,
        handle_event_callback: $.regforum.profile.companies.add.editorCounter,
        save_callback: function(element_id, html, body){
            html = $('<div>' + html + '</div>').find('iframe').filter(':not(iframe[src*="youtube.com"])').remove().end().end().html();
            return html;
        },
        setup : function(ed) {
            ed.onLoadContent.add(function(ed) {
                var state = ed.dom.select('hr.insert-cut').length;
                ed.controlManager.setActive('topiccut', state);
            });
            ed.onKeyUp.add(function(ed, e) {
                if(!topic_id && ed.id == "topic_content"){
                    saveData(ed, e)
                }
                else {
                    updateData();
                }
            });
            ed.addButton('topiccut', {
                title : '���',
                image : '/ximg/toolbar/cut_20x20.png',
                onclick: function() {
                    var hr = ed.dom.select('hr.insert-cut'),
                        state = true;
                    if(hr.length){
                        ed.dom.remove(hr);
                        state = false;
                    } else {
                        ed.selection.setNode(ed.dom.create('hr', {class : 'insert-cut'},''));
                    }
                    ed.controlManager.setActive('topiccut', state);
                    return false;
                }
            });
            ed.addButton('youtube', {
                title : '�������� ����� � Youtube',
                image : '/ximg/toolbar/youtube_20x20.png',
                onclick: function() {
                    var url = window.prompt('������� �����, �� �������� �������� ����� Youtube'),
                        a,
                        s,
                        i,
                        params,
                        pair,
                        code,
                        m = '��������! ������ ������������ �����.';

                    a = document.createElement('a');
                    a.href = url;

                    if( a.host.indexOf('youtube') != -1){
                        s = a.search;
                        if(s.length){
                            s = s.substring(1);
                            params = s.split('&');
                            for (i = 0; i < params.length; i++ ) {
                                pair = params[i].split('=');
                                if(pair[0] == 'v') {
                                    code = pair[1];
                                }
                            }
                            if(!!code){
                                ed.focus();
                                ed.selection.select();
                                ed.selection.setContent(
                                    '<div>'
                                        + '<iframe width="640" height="360" src="http://www.youtube.com/embed/' + code + '" frameborder="0" allowfullscreen="allowfullscreen"></iframe>'
                                        + '</div>'
                                );
                            }
                        }
                        else {
                            alert(m);
                        }
                    }
                    else {
                        alert(m);
                    }
                    return false;
                }
            });
            ed.addButton('upper_�ase', {
                title : '������������� � ������� ��������',
                image : '/ximg/toolbar/edit-uppercase.png',
                onclick : function() {
                    ed.focus();
                    ed.selection.select();
                    ed.selection.setContent(ed.selection.getContent().toUpperCase());
                }
            });
            ed.addButton('lower_�ase', {
                title : '������������� � ������ ��������',
                image : '/ximg/toolbar/edit-lowercase.png',
                onclick : function() {
                    ed.focus();
                    ed.selection.select();
                    ed.selection.setContent(ed.selection.getContent().toLowerCase());
                }
            });
            ed.addButton('inquotes', {
                title : '� ��������',
                image : '/ximg/toolbar/in_quotes.png',
                onclick : function() {
                    ed.focus();
                    ed.selection.select();
                    ed.selection.setContent('&laquo;'+ed.selection.getSel()+'&raquo;');
                }
            });
            ed.addButton('user', {
                title : '������ �� ���������',
                image : '/ximg/toolbar/user-add.png',
                onclick : function() {
                    ed.focus();
                    if (!user_dialog.dialog('option', 'disabled')) {
                        user_dialog.dialog("open");
                    }

                    $(".dialog_filter").live('keyup', function() {
                        var _this = $(this);
                        clearTimeout(getUsersTimeout);
                        getUsersTimeout = setTimeout(function() {
                            var fastsearch = _this.val();
                            $.getJSON('/posts/getusers/', { fastsearch: fastsearch },
                                function(obj) {
                                    if (obj) {
                                        $("#user_dialog_content").html(obj._content);
                                    }
                                });
                        }, 500);
                    });
                    //������� ��� ������
                    $('#user_dialog_content').one('click', 'p', function() {
                        var _this = $(this),
                            parents = _this.parent(),
                            data_id = _this.attr("id"),
                            span = _this.find('span'),
                            data_text;
                        if(span.length) {
                            _this.find('span').remove();
                        }

                        data_text = _this.text();
                        ed.selection.setContent('<a class="member" href="http://regforum.ru/members/' + data_id + '/">' + data_text + '</a>');
                        user_dialog.dialog("close");
                        parents.html('').siblings('.top_filter').find('input').val('');
                    });
                }

            });
            ed.addButton('insertimage', {
                title : '��������� ��������',
                image : '/ximg/icons/insert-image.png',
                onclick : function() {
                    ed.focus();
                    if (!imageAdd.dialog('option', 'disabled')) {
                        imageAdd.dialog("open");
                    }
                }

            });
        },
        verify_html : true,
        inline_styles : false,
        apply_source_formatting : true,
        convert_fonts_to_spans : true,
        //valid_children : "-div[style,class],-p[style],-span[style,class]",
        valid_elements : "a[href|target|class|title|rel],strong,br,img[src|width|height|align|hspace|vspace|id|class],em,p[class],h2,blockquote,u,ol,ul,li,del,table[class],td[class],tr[class],hr[class]," +
            "iframe[src|title|width|height|allowfullscreen|frameborder|class|id]",
        invalid_elements : "script,applet,object,h3,h4,h5,h6,h7,h8,h9,cite,button,section,head,header,footer,article,embed,pre",
        removeformat_selector : 'b,strong,em,i,span,ins,div,li,ul,h2',
        style_formats : [
            {title : '����', selector : 'p', classes : 'incut'},
            {title : '�������', selector : 'p', classes : 'hint'},
            {title : '������ ������ 1', selector : 'p', classes : 'fs13'},
            {title : '������ ������ 2', selector : 'p', classes : 'fs14'},
            {title : '���������', selector : 'p', classes : 'h3'}
        ],
        'formats' : {
            'alignleft' : {'selector' : 'h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-left'},
            'aligncenter' : {'selector' : 'h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-center'},
            'alignright' : {'selector' : 'h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-right'},
            'alignfull' : {'selector' : 'h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-justify'},
            'italic' : {'inline' : 'em'},
            'bold' : {'inline' : 'strong'},
            'underline' : {'inline' : 'u'},
            'strikethrough' : {'inline' : 'del'}
        },
        removeformat : [
            {selector : 'div,b,strong,em,i,font,u,strike', remove : 'all', split : true, expand : false, block_expand : true, deep : true},
            {selector : 'span', attributes : ['style', 'class'], remove : 'empty', split : true, expand : false, deep : true}
            //{selector : '*', attributes : ['style', 'class'], remove: 'all', split : true, expand : true, deep : true}
        ],
        protect: [
            /\<\/?(if|endif)\>/g, // Protect <if> & </endif>
            /\<xsl\:[^>]+\>/g, // Protect <xsl:...>
            /<\?php.*?\?>/g // Protect php code
        ]
    };
    tinyMCE.init($.extend({}, options, {elements: "topic_content", height:  "460"}));
    topic_log.length && tinyMCE.init($.extend({}, options, {elements: "Topic_topic_log", height:  "230"}));

    //��������� � localStorage
    function saveData(ed, e){
        clearTimeout(saveDataTimeout);
        saveDataTimeout = setTimeout(
            function(){
                var name = name_selector.val(),
                    content = ed.getContent(),
                    params = null;
                localStorage.setItem('postContent', content);
                localStorage.setItem('postName', name);
                $.regforum.helpers.flash('���������� ���������!', true)
            },
            5000);
    }

    //��������� �� ������� �������
    function updateData() {
        clearTimeout(saveDataTimeout);
        saveDataTimeout = setTimeout(
            function(){
                $('#topic_form').find('#topic_content').text(tinyMCE.get('topic_content').getContent());
                $('#topic_form').find('#Topic_topic_log').text(tinyMCE.get('Topic_topic_log').getContent());
                $('#topic_form').ajaxSubmit();
                $.regforum.helpers.flash('���������� ���������!', true)
            },
            30000);
    }

	jQuery.fn.extend({
		insertAtCaret: function(myValue){
			return this.each(function(i) {
				if (document.selection) {
					// ��� ��������� ���� Internet Explorer
					this.focus();
					var sel = document.selection.createRange();
					sel.text = myValue;
					this.focus();
				}
				else if (this.selectionStart || this.selectionStart == '0') {
					// ��� ��������� ���� Firefox � ������ Webkit-��
					var startPos = this.selectionStart;
					var endPos = this.selectionEnd;
					var scrollTop = this.scrollTop;
					this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
					this.focus();
					this.selectionStart = startPos + myValue.length;
					this.selectionEnd = startPos + myValue.length;
					this.scrollTop = scrollTop;
				} else {
					this.value += myValue;
					this.focus();
				}
			})
		}
	});
	
});