/**
 * Created with JetBrains PhpStorm.
 * User: RegforumTeam
 * Date: 15.05.13
 * Time: 17:06
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function() {
    //Binding
    var block = $('.main_block'),
        cookieName = 'subject',
        cookieExp = 10,
        timeout_input, //id ������� �������
        search_timeout = 500; //�������� ��. �� ������

    block.find('.select_button').on('click', function(){
        var _this = $(this);
        closeOtherList(_this);
        if (_this.parents('.main_block').hasClass('disabled')) return;
        if (_this.hasClass('active')) {
            _this.removeClass('active');
            _this.find('span.arrow_down').show();
            _this.siblings('div.objects_list').hide();
        } else {
            _this.addClass('active');
            _this.find('span.arrow_down').hide();
            _this.siblings('div.objects_list').show().find('input').focus();
        }
    });

    $(document).on('click', function(e){
        if(!$(e.target).closest('.main_block').length){
            block.find('div.select_button').removeClass('active');
            block.find('div.objects_list').hide();
            block.find('span.arrow_down').show();
        }
    });

    $(document).on('click', '.objects_list li', function(){
        var _this = $(this),
            block = _this.parents('.main_block'),
            selected = block.find('ul li.selected'),
            selector = block.attr('id');
        closeObjectList(this);
        if(selected && _this.attr('id') == selected.attr('id')) return false; //���� ���� �� ��� ����������, ������������.
        _this.siblings().removeClass('selected');
        _this.addClass('selected');
        hideChildSelectors(this);
        setObjectTitle(this);
        fillKladrString();
        $('.kladrStringField').show();
        if(selector == 'street' || selector == 'home') {
            getKladrData(_this.attr('id'), selector);
        } else {
            getAjaxData(this);
        }
        if(selector == cookieName) setCookie(this);
    });

    $('div.main_block div.objects_list').on('keyup', 'input', function(){
        var text = $(this).val(),
            ells = $(this).parents('.objects_list').find('ul li');
        clearTimeout(timeout_input);
        timeout_input = setTimeout(function() {
            ells.each(function(){
                var re = new RegExp('(^|\\s)' + text, 'ig'),
                    liText = $(this).text();
                var result = liText.match(re);
                if(result) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        }, search_timeout);
    });

    function hideChildSelectors(el){
        var current = $(el).parents('.main_block').attr('id'),
            selectors = $('.current div.street, .current div.kladrData, .current div.info, .current div.home, .current div.allHomes');
        switch (current) {
            case 'subject':
                $("div.region, div.city, div.location, div.street, div.kladrData, div.info, div.home, div.allHomes").hide();
                break;
            case 'region':
                $('.current div.city, .current div.location').hide();
                selectors.hide();
                break;
            case 'city':
                $('.current div.location').hide();
                selectors.hide();
                break;
            case 'location':
                selectors.hide();
                break;
            default:
                break;
        }
    }

    $("span.showHomes").on('click', function() {
        if ($(this).parent().parent().hasClass('kladr_search')){
            var id = $('div#allFoundedStreets').find('.selected').attr('id')
        }
        else {
            var id = $("#street li.selected").attr('id');
        }
        var location = "street";
        getAllHomesKladrData(id, location);
        $(".current span.showHomes, .current .kladrData, .current .kladrStringField").hide();
        clearObjectBlock(".current div.home");
    });

    $("ul.kladr_tabs").delegate("li:not(.current)", "click", function() {
        $(this).addClass("current").siblings().removeClass("current")
            .parents("div.content").find("div.kladr_tabs_content").addClass("current").show(500).eq($(this).index()).removeClass("current").hide();
        $('.subject #subject').removeClass('disabled');
        $('.kladr_search #allSubj').removeAttr('checked');
    });

    $(document).on('change click keyup', '#kladr_street_search:input, .container .content #subject', function(e) {
        var code = e.keyCode || e.which;
        if (!$("#subject li.selected").attr('id') && !$('.kladr_search #allSubj').is(':checked')) {
            return;
        }
        var str = $("#kladr_street_search:input").val().length;
        if(str > 2) {
            $(".kladr_search button").removeAttr('disabled');
        }
        else {
            $(".kladr_search button").attr('disabled', 'disabled');
        }
        if (code == 13) {
            $(".kladr_search button").click();
        }
    });

    $(".kladr_search button").on('click', function() {
        $(".current div.showHomes, .current .home, .current .kladrData, .current .allHomes, .current #allFoundedStreets, .current .infoForStreet").hide();
        var region = $('.kladr_search #allSubj').prop("checked") ?  'all' : $("#subject li.selected").attr('id');
        getSearchData($("#kladr_street_search "), region, 1, $('#searchObject input:checked').val());
    });

    $('#searchObject input').on('change', function() {
        $('div#allFoundedObjects, div#allFoundedStreets, .infoForStreet, .current .kladrData').hide();
        $("#kladr_street_search:input").change();
    });

    $('div#allFoundedObjects, div#allFoundedStreets').on('click', '.pagination a', function() {
        var region = $('.kladr_search #allSubj').prop("checked") ?  'all' : $("#subject li.selected").attr('id');
        getSearchData($("#kladr_street_search "), region, $(this).attr('id'), $('#searchObject input:checked').val());
        return false;
    });

    $('.kladr_search #allSubj').on('click', function(){
        $("#kladr_street_search:input").change();
        if($(this).prop("checked")) {
            $('.subject #subject').addClass('disabled');
        } else {
            $('.subject #subject').removeClass('disabled');
        }
    });

    $('div#allFoundedStreets ul').live("click", function() {
        $(this).siblings().removeClass("selected");
        $(this).addClass("selected");
        getKladrData($(this).attr('id'), 'street');
    });

    $('.kladrData .homes').on('click', function() {
        $('.current .kladrData .HOMES').toggle();
    });


    //���������� �������� �������, ���� � ������ ��� ����������
    var cookieObj = $("#subject .objects_list li.selected");
    if (cookieObj.length){
        getAjaxData(cookieObj);
        setObjectTitle(cookieObj);
    }

    $('.kladrData .IDX').on('click', function() {
        var idx = $('.current .kladrData .IDX').text(),
            currAddress = $(this).siblings('.address'),
            currHtml = currAddress.html(),
            currIdx = currAddress.attr('id');
        if(idx != currIdx || currHtml == '') {
            getPostAddress(idx);
        } else {
            currAddress.toggle();
        }
    });

    /**
     * Main function. Returns html data and input selector
     * @param name - object val.
     */
    function getSearchData(name, region, page, searchObject) {
        var objName = name.val(),
            searchObject = searchObject || null;
        name.attr("disabled", "disabled");
        name.parent().find('span.ajaxLoader').show();
        name.parent().find('button').attr("disabled", "disabled");
        $.ajax('/services/kladr/kladrsearch/', {
            type: "GET",
            data: { name: objName, region: region, page: page, searchObject: searchObject },
            cache: false,
            success: function(response) {
                var answer = $.parseJSON(response);
                name.removeAttr("disabled");
                if (!answer){
                    console.log ('ERROR: Some Ajax error');
                    return;
                }
                var selector = answer.selector || false,
                    pagesCount = answer.pagesCount || 0,
                    page = answer.page || 0,
                    content = answer.content || false,
                    warning = answer.warning || false;
                if (selector == 'foundedObject' ) {
                    if (content) {
                        var newcontent = '';
                        for(var i = 0; i < content.length; i++) {
                            var displayClass = content[i]['RSTATUS'] == '00' ? '' : 'class="abandoned"';
                            newcontent += '<ul id="' + content[i]['RCODE'] + '" ' + displayClass + '>';
                            newcontent += '<li>' + content[i]['NAME'] + '<span> ' + content[i]['SOCR'] + '.;</span></li>';
                            newcontent += '<li>&nbsp;' + (content[i]['PARENTS']['r'] != undefined ? content[i]['PARENTS']['r'] + '.' : '');
                            newcontent += (content[i]['PARENTS']['g'] != undefined ?  ', ' + content[i]['PARENTS']['g'] + '.' : '');
                            newcontent += (content[i]['PARENTS']['p'] != undefined ? ', ' + content[i]['PARENTS']['p'] + '.' : '') + '</li>';
                            newcontent += displayClass ? '<li class="right">�� ��������</li>' : '';
                            newcontent += '</ul>';
                        }
                        var pagination = ajaxPagination(pagesCount, page, 'obj');
                        // Replace old content with new content
                        newcontent = pagination + newcontent;
                        $('.' + selector).html(newcontent);
                        $('.' + selector).show();
                        $('.foundedStreet, .infoForStreet').hide();
                    }

                    else if (warning){

                        $("." + selector).empty().html(warning).show();
                    }
                    name.parent().find('span.ajaxLoader').hide();
                }
                else if (selector == 'foundedStreet') {
                    if (content) {
                        var newcontent = '';
                        for(var i = 0; i < content.length; i++) {
                            var displayClass = content[i][5]=='00' ? '' : 'class="abandoned"';
                            newcontent += '<ul id="' + content[i][0] + '" ' + displayClass + '>';
                            newcontent += '<li>' + content[i][1] + '<span>'+ content[i][2] + ';</span></li>';
                            newcontent += '<li>' + content[i][3] + '<span>' + content[i][4] + '.</span></li>';
                            newcontent += displayClass ? '<li class="right bold">�� ��������</li>' : '';
                            newcontent += content[i][0].substr(-2) == '51' ?  '<li class="right gray pr10">[������ ������������]</li>' : '';
                            newcontent += '</ul>';
                        }
                        var pagination = ajaxPagination(pagesCount, page, 'str');
                        // Replace old content with new content
                        newcontent = pagination + newcontent;
                        // Replace old content with new content
                        $('.'+selector).html(newcontent);

                        $('.infoForStreet').show();
                        $('.'+selector).show();
                        $('.foundedObject').hide();
                    }

                    else if (warning){
                        $("." + selector).empty().html(warning).show();
                    }
                    name.parent().find('span.ajaxLoader').hide();
                }

                else {
                    console.log ('ERROR: Some Ajax error');
                }
            }
        });
    }

    /**
     * Main function. Returns html data and input selector
     * @param selector - object id.
     */
    function getAjaxData(obj) {
        var obj = $(obj),
            block = obj.parents('.main_block'),
            name = block.attr('id'),
            subject = obj.attr('id');
        block.addClass('disabled');
        block.find('span.arrow_down').hide();
        block.find('span.ajaxLoader').show();
        if(!subject || !name) return;
        $.ajax('/services/kladr/getajaxdata/', {
            type: 'GET',
            data: { subject: subject, selector: name },
            cache: false,
            success: function(response){
                var answer = $.parseJSON(response);
                block.removeClass('disabled');
                if (!answer){
                    getKladrData(subject, false);//������ ���� � ���. ����� ����������� �����.
                    return;
                }
                var selector = answer.selector || false;
                if (selector) {
                    $('.'+selector).show();
                    clearObjectBlock('.' + selector);
                    $('.'+selector+' div.objects_list ul').empty().append(answer.content);
                    block.find('span.arrow_down').show();
                    block.find('span.ajaxLoader').hide();
                    $('.current .custom-combobox-input').val('');
                }
                else {
                    console.error('ERROR: Some Ajax error:' + answer);
                }
            }
        });
    }

    /**
     * Gets Kladr data from the server
     * @param object id.
     */
    function getKladrData(id, location) {
        if (!id) return console.log('error');
        location = location || null;
        hidePostAddress();
        if (location=='street') {
            $('.current div.kladrData, .current div.home, .current div.info, .current div.allHomes').hide();
            $('.current span.showHomes').show();
            $('.current div.home .custom-combobox-input').val('');
            $('.current div.home select').empty();
        }
        $.ajax('/services/kladr/getkladrdata/', {
            type: "GET",
            data: { id: id, location: location },
            cache: false,
            success: function(response){
                var answer = $.parseJSON(response),
                    kladrData = answer.data,
                    homes= answer.homes,
                    kladrSelector = '.current div.kladrData ';
                $('span.ajaxLoader').hide(); //��������� ��� �������
                if (kladrData) {    //����������� ������
                    $(kladrSelector + '.IDX').empty().append(kladrData.IDX);
                    $(kladrSelector + '.GNINMB').empty().append(kladrData.GNINMB);
                    $(kladrSelector + '.OCTMONAME').empty().append(kladrData.OCTMONAME);
                    $(kladrSelector + '.OCATD').empty().append(kladrData.OCATD);
                    if(kladrData.TAX_AUTHORITY) {
                        $(kladrSelector + '.TAX_AUTHORITY').empty().append(kladrData.TAX_AUTHORITY.NAIM);
                        $(kladrSelector + '.NAIMK').empty().append(kladrData.TAX_AUTHORITY.NAIMK);
                        $(kladrSelector + '.CODESPRO').empty().append(kladrData.TAX_AUTHORITY.KOD);
                    }
                    if(kladrData.OCTMO) {
                        $(kladrSelector + '.OCTMO').empty().append(kladrData.OCTMO.OCTMO);
                        $(kladrSelector + '.OCTMONAME').empty().append(kladrData.OCTMO.OCTMONAME);
                    } else {
                        $(kladrSelector + '.OCTMO').empty();
                        $(kladrSelector + '.OCTMONAME').empty();
                    }
                    if(kladrData.allHomes && kladrData.allHomes.length) {
                        $(kladrSelector + 'span.homes').show();
                        $(kladrSelector + '.HOMES').empty().append(kladrData.allHomes.join(', '));
                    } else {
                        $(kladrSelector + 'span.homes, ' + kladrSelector + '.HOMES').hide();
                    }
                    $(kladrSelector + ', .current span.showHomes').show();
                    $('.current div.allHomes').hide();
                }
                else if(homes) {    //������ � ������
                    $('.current div.home').show();
                    $('.current div.home ul').empty().append(homes);
                }
                else {              //������
                    $('.current div.info').show();
                    console.log ('ERROR: kladr data has some Ajax error');
                }
            }
        });
    }

    /**
     * ��������� ���� ����� �� �����
     * @param id
     * @param location
     * @returns {*}
     */
    function getAllHomesKladrData(id, location) {
        if (!id) return console.log('error id');
        if (!location) return console.log('error location');
        hidePostAddress();
        $.ajax('/services/kladr/getallhomeskladrdata/', {
            type: "GET",
            data: { id: id, location: location },
            cache: false,
            async: false,
            success: function(response){
                var answer = $.parseJSON(response);
                var kladrData = answer.data;
                $('.current span.ajaxLoader').hide(); //��������� ��� �������
                if (kladrData) {    //����������� ������
                    $('.current div.allHomes').empty().append(kladrData).show();
                    $('.current div.kladrData').hide();
                }
                else {              //������
                    $('.current div.info').show();
                    console.log ('ERROR: kladr data has some Ajax error');
                }
            }
        });
    }

    /**
     * ��������� ��������� ������ �� ajax
     * @param index - ������
     */
    function getPostAddress(index) {
        if(!index) return false;
        $('.current div.post_address span.ajaxLoader').show();

        $.ajax('/tools/getpostaddress/', {
            type: "GET",
            data: { idx: index },
            cache: false,
            success: function(response){
                var answer = $.parseJSON(response),
                    selector = $('.current div.post_address');
                if (answer) {
                    selector.find('.address').show().empty().append(answer);
                    selector.find('.address').attr('id', index);
                    selector.find('span.ajaxLoader').hide();
                }
                else {
                    console.log ('ERROR: Some Ajax error');
                }
            }
        });
    }
    /**
     * �������� ��������� �������� �����
     */
    function hidePostAddress() {
        $('.current .kladrData .post_address .address').hide();
    }

    /**
     * ��������� ��� ���������� ��������
     * @param elem - this
     */
    function setCookie(el) {
        var value = $(el).attr('id');
        $.cookie(cookieName, value, {expires: cookieExp, path: '/'});
    }

    /**
     * ���������� ������ ���������� ������
     */
    function fillKladrString() {
        var arr = $('.current').find('ul'),
            text = $('.subject li.selected').text(),
            arrText = new Array(),
            visibleArr = new Array(),
            j = 0; //������� ������� �������� ��������
        for (var i = 0; i < arr.length; i++) {
            var select = $(arr[i]).find('li.selected').text(),
                visible = $(arr[i]).parents('.main_block').parent().is(':visible');
            if (typeof select == 'undefined' || select == '[�� ���������]' || !visible) {
                select = '';
            }
            arrText.push(select);
            if (visible) {
                visibleArr.push(true);
            } else {
                visibleArr.push(false);
            }
        }
        for(var i = 0; i < visibleArr.length; i++) {
            j = visibleArr[i] ? i : j;
        }
        arrText.splice(j + 1, arrText.length);
        for (var i = 0; i< arrText.length; i++) {
            text += ', ' + arrText[i];
        }
        $('.kladrString').text(text);
    }

    /**
     * �� ����������� this (�� �������) ������� ������������ ���� � �������� ��� � �������������� ���������.
     * @param el - this
     */
    function closeObjectList(el) {
        var block = $(el).closest('.main_block');
        block.find('div.select_button').removeClass('active');
        block.find('div.objects_list').hide();
        block.find('span.arrow_down').show();
    }

    /**
     * �� ����������� this (�� �������) ������� �������� ��������� � ��������� ������ ��.
     * @param el - this
     */
    function closeOtherList(el) {
        var siblings = $(el).parents('.main_block').parent().siblings('div');
        siblings.each(function(){
            var block = $(this).find('.main_block');
            if (!block) return;
            block.find('div.select_button').removeClass('active');
            block.find('div.objects_list').hide();
            block.find('span.arrow_down').show();
        });
    }

    /**
     * �� ����������� this (�� �������) ������� ������������ �������� � ������������� ����� �� ���������� �������.
     * @param el - this
     */
    function setObjectTitle(el) {
        var parent = $(el).closest('.main_block');
        parent.find('span.select').text($(el).text());
        parent.find('span.select').show();
        parent.find('span.default').hide();
    }

    /**
     * �� ����������� ��������� ������� main_block, �������� � ��������� �� ���������(��� �������� �� ������� ��������� li ��������).
     * @param el - $('.main_block').attr('id') - �������� �������� ������
     */
    function clearObjectBlock(el) {
        var el = $(el);
        el.find('input').val('');
        el.find('.objects_list li').removeClass('selected');
        el.find('span.select').hide();
        el.find('span.default').show();
    }

    $('span.poshytip_click').poshytip({
        className: 'tip-yellowsimple',
        showTimeout: 200,
        alignTo: 'target',
        alignX: 'center',
        alignY: 'top',
        offsetY: 7,
        liveEvents: true
    });

});
