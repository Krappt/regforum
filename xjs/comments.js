/**
 * Created with JetBrains PhpStorm.
 * User: VavilovSN
 * Date: 03.07.15
 * Time: 15:33
 * To change this template use File | Settings | File Templates.
 */

$(document).ready(function () {
    var mainContainer = $('#comments'),
        propose = mainContainer.find('.propose'),
        commentsContainer = mainContainer.find('div.rComments'),
        commentForm = mainContainer.find('.comment-form'),
        bottomAddComment = mainContainer.find('.add-comment .ll'),
        wysiwyg = 'div.comment-wysiwyg',
        submitBtn = wysiwyg + ' button.btn',
        wysiwygCounter = ' .count',
        commentSize = 1500,
        like = '.like',
        liked = '.liked',
        close = '.close',
        reply = '.reply',
        cDelete = '.controls .delete',
        cRestore = '.controls .restore',
        cRemove = '.controls .remove',
        cErase = '.controls .erase',
        cEdit = '.controls .edit',
        cComplaints = '.complaint',
        editId,
        parentId;

    /*    mainContainer.on('click', wysiwyg + ' a.btn', function () {
     var _this = $(this),
     commentForm = _this.parents('.comment-form');
     commentForm.find('.btn-toolbar.collapse').toggleClass('in');

     });*/

    mainContainer.on('keydown', wysiwyg + ' #commentWysiwyg', function (ev) {
        var count = $(this).html().length,
            summ = commentSize - count,
            pressedKey = ev.charCode || ev.keyCode || -1;
        if (summ <= 0 && pressedKey != 46 && pressedKey != 8 && !(pressedKey > 37 && pressedKey < 41))
            return false;
    });

    mainContainer.on('keyup', wysiwyg + ' #commentWysiwyg', function () {
        var _this = $(this),
            parent = _this.parents(wysiwyg),
            count = $(this).html().length;
        parent.find(wysiwygCounter).text(commentSize - count);
    });

    //���� ������ �����������
    commentsContainer.on('click', like, function () {
        var _this = $(this),
            id = getCommentId(_this);
        if (id) {
            $.post('/comments/vote/',
                {id: id},
                function (response) {
                    _this.removeClass('rf-heart like').addClass('liked rf-heart-fill');
                    _this.text(response);
                });
        }
    });

    //���� ������ ������ �����������
    commentsContainer.on('click', liked, function () {
        var _this = $(this),
            id = getCommentId(_this);
        if (id) {
            $.post('/comments/unvote/',
                {id: id},
                function (response) {
                    _this.removeClass('liked rf-heart-fill').addClass('rf-heart like');
                    _this.text(response);
                });
        }
    });

    commentsContainer.on('click', reply, function () {
        var _this = $(this),
            parentBlock = _this.parent().parent(),
            comment = _this.parents('div.comment');
        if (!comment.hasClass('deleted')) {
            //commentFormCopy = commentFormCopy ? commentFormCopy : commentForm.css({opacity: 0.0}).clone();
            parentId = comment.attr('id'); //important. do not move
            parentBlock.append(commentForm.css({opacity: 0.0}).animate({opacity: 1.0}, 200));
            //commentForm.hide();
        }
    });

    commentsContainer.on('click', close, function () {
        parentId = null;
        propose.append(commentForm.css({opacity: 0.0}).animate({opacity: 1.0}, 200));
    });

    commentsContainer.on('click', '.redact ' + close, function () {
        commentForm.find('#commentWysiwyg').html('');
        commentForm.hasClass('redact') && commentForm.removeClass('redact');
        commentForm.removeAttr('data-nestlevel');
        commentsContainer.find('#' + editId).show();
        propose.removeClass('redact');
        editId = null;
    });

    mainContainer.on('click', submitBtn, function () {
        var _this = $(this);
        if (!_this.hasClass('disabled')) {
            _this.addClass('disabled');
            if (editId) {
                updateComment(this, _this.parents(wysiwyg).find('#commentWysiwyg'));
            } else {
                addComment(this, _this.parents(wysiwyg).find('#commentWysiwyg'));
            }
        }
    });

    //noinspection JSLint
    /**
     * delete own comment
     */
    $(commentsContainer).on('click', cErase, function () {
        var id = getCommentId(this);
        //noinspection JSLint
        deleteComment(id, function () {
            commentsContainer.find('#c' + id).hide();
        });
    });

    //noinspection JSLint
    /**
     * delete comment control
     */
    $(commentsContainer).on('click', cDelete, function () {
        var id = getCommentId(this);
        //noinspection JSLint
        deleteComment(id, function () {
            var comment = commentsContainer.find('#c' + id);
            comment.addClass('deleted');
            comment.find(cDelete).css('display', 'none');
            comment.find(cRestore).css('display', 'block');
        });
    });

    //noinspection JSLint
    /**
     * restore comment control
     */
    $(commentsContainer).on('click', cRestore, function () {
        var id = getCommentId(this);
        //noinspection JSLint
        restoreComment(id);
    });

    //noinspection JSLint
    /**
     * kill comment control
     */
    $(commentsContainer).on('click', cRemove, function () {
        var id = getCommentId(this);
        //noinspection JSLint
        killComment(id);
    });

    //noinspection JSLint
    /**
     * kill comment control
     */
    $(commentsContainer).on('click', cEdit, function () {
        var _this = $(this),
            comment = _this.parents('div.comment'),
            nestLevel = comment.data('nestlevel'),
            text = comment.find('.text').html();

        if(editId) {
            var previousComment = commentsContainer.find('#' + editId);
            commentForm.find('#commentWysiwyg').html('');
            previousComment.show().animate({opacity: 1.0}, 200);
        }

        editId = comment.attr('id');
        commentForm.attr('data-nestlevel', nestLevel);
        commentForm.find('#commentWysiwyg').html(text);
        commentForm.addClass('redact');
        comment.after(commentForm);
        comment.hide();
        propose.addClass('redact');
    });

    $(commentsContainer).on('click', '.comment .link', function() {
        var input = $(this).parents('.comment').find('.www-link');
        input.toggle().focus().select();
    });

    $(commentsContainer).on('click', '.comment .www-link .close', function() {
        var input = $(this).parents('.comment').find('.www-link');
        input.hide();
    });

    bottomAddComment.on('click', function () {
        var _this = $(this),
            parentBlock = _this.parent();
        parentId = null;
        parentBlock.append(commentForm.css({opacity: 0.0}).animate({opacity: 1.0}, 200));
    });

    //���� ���������
    commentsContainer.on({
        mouseenter: function () {
            var id = $(this).attr('href');
            $(id).addClass('selected');
        },
        mouseleave: function () {
            var id = $(this).attr('href');
            $(id).removeClass('selected');
        }
    }, 'a.rf-reply-fill');

    function getCommentId(selector) {
        var anchor = $(selector).parents('.comment').attr('id');
        return anchor ? anchor.replace('c', '') : 0;
    }

    /**
     * �������� �����������
     * @param button
     * @param wysiwyg
     * @returns {boolean}
     */
    function addComment(button, wysiwyg) { //TODO fix it
        var button = $(button),
            text = wysiwyg.cleanHtml(),
            model = mainContainer.find('#model').val(),
            target = mainContainer.find('#target').val();
        if (!text) {
            button.removeClass("disabled");
            return false;
        }
        $.post('/comments/add/',
            {
                parentId: parentId,
                model: model,
                target: target,
                text: text
            },
            function (response) {
                button.removeClass("disabled");//������� ���������� ���������� �������
                if (response.comment && !response.errors) {
                    wysiwyg.html('');
                    if (parentId) {
                        var el = document.createElement('tmp'),
                            parent = commentsContainer.find('#' + parentId),
                            parentNestLevel = parent.data('nestlevel');
                        $(el).append(response.comment);
                        var comment = $(el).find('.comment');
                        comment.attr('data-nestlevel', (parentNestLevel > 1 ? parentNestLevel : parentNestLevel + 1));
                        propose.append(commentForm.show().animate({opacity: 1.0}, 200));
                        commentsContainer.find('#' + parentId).after(comment);
                    } else {
                        commentsContainer.append(response.comment);
                        $.scrollTo(commentsContainer.find('div.main_comment:last'), 100);
                    }

                }
                else if (response.errors) {
                    console.log(response.errors);
                }
                else {
                    console.error('some ajax error');
                }
            });
    }

    function updateComment(button, wysiwyg) {
        var button = $(button),
            text = wysiwyg.cleanHtml(),
            model = mainContainer.find('#model').val(),
            target = mainContainer.find('#target').val();
        if (!text) {
            button.removeClass("disabled");
            return false;
        }

        $.post('/comments/update/',
            {
                id: editId.replace('c', ''),
                model: model,
                target: target,
                text: text
            },
            function (response) {
                var comment = commentsContainer.find('#' + editId);
                editId = null;
                button.removeClass("disabled");
                propose.removeClass('redact');
                commentForm.find('#commentWysiwyg').html('');
                propose.append(commentForm.show().animate({opacity: 1.0}, 200));
                comment.find('.text').html(response);
                comment.show().animate({opacity: 1.0}, 200);
            });
    }

    /**
     * deleteComment
     * @param id
     * @param successFunc success function
     */
    function deleteComment(id, successFunc) {
        id && $.ajax('/comments/delete/', {
            type: "GET",
            data: {id: id},
            cache: false,
            success: successFunc
        });
    }

    /**
     * restoreComment
     * @param id
     */
    function restoreComment(id) {
        id && $.ajax('/comments/restore/', {
            type: "GET",
            data: {id: id},
            cache: false,
            success: function () {
                var comment = commentsContainer.find('#c' + id);
                comment.removeClass('deleted');
                comment.find(cRestore).css('display', 'none');
                comment.find(cDelete).css('display', 'block');
            }
        });
    }

    /**
     * killComment
     * @param id
     */
    function killComment(id) {
        if (confirm("�� ������� ��� ������ ������� ���� �����������?")) {
            id && $.ajax('/comments/kill/', {
                type: "GET",
                data: {id: id},
                cache: false,
                success: function () {
                    commentsContainer.find('#c' + id).css('display', 'none');
                },
                statusCode: {
                    404: function (xhr) {
                        commentsContainer.find('#c' + id).css('display', 'none');
                        if (window.console) console.log(xhr.responseText);
                    }
                }
            });
        }
    }

});
