/**
 * Created with JetBrains PhpStorm.
 * User: RegforumTeam
 * Date: 02.03.15
 * Time: 13:41
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function () {
    var internal;

    internal = {
        admin: {
            rubricator: {
                init: function () {
                    var list = $('.draggable .items'),
                        notifier = $('#notifier'),
                        view = '.view',
                        fader = 'slow';

                    list.length && list.dragsort({
                        dragSelector: view,
                        placeHolderTemplate: '<div class="view placeholder"></div>',
                        dragEnd: function () {
                            var m = [];
                            $(list).find(view).each(function (i) {
                                m[i] = $(this).data('id');
                            });
                            $.post('/admin/rubricator/updatesort/', {'data': m }, function (response) {
                                notifier.attr('class', 'sign');
                                if (response.success) {
                                    notifier.addClass('sign_green').html('изменения сохранены').show();
                                }
                                else {
                                    notifier.addClass('sign_ruby').html('изменения не удалось сохранить').show();
                                }

                                setTimeout(function () {
                                    notifier.fadeOut(fader);
                                }, 1000);
                            });
                        }
                    });
                }
            }
        }
    };

    (function (internal) {
        var initFn = internal,
            pathName = document.location.pathname.split('/'),
            namespace = pathName.filter(function (n) {
                return n != false
            });

        for (var n in namespace) {
            initFn = initFn && initFn[namespace[n]] ? initFn[namespace[n]] : null;
        }

        if (initFn && initFn.init && typeof initFn.init == "function") {
            return initFn.init.call({});
        } else {
            return null;
        }
    }(internal));
});