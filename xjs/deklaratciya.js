$(document).ready(function() {

	$("#date").datepicker({
	changeYear: true,
	clearText: '��������', clearStatus: '',
	closeText: '�������', closeStatus: '',
	prevText: '&#x3c;����',  prevStatus: '',
	prevBigText: '&#x3c;&#x3c;', prevBigStatus: '',
	nextText: '����&#x3e;', nextStatus: '',
	nextBigText: '&#x3e;&#x3e;', nextBigStatus: '',
	currentText: '�������', currentStatus: '',
	monthNames: ['������','�������','����','������','���','����','����','������','��������','�������','������','�������'],
	monthNamesShort: ['���','���','���','���','���','���','���','���','���','���','���','���'],
	monthStatus: '', yearStatus: '',
	weekHeader: '��', weekStatus: '',
	dayNames: ['�����������','�����������','�������','�����','�������','�������','�������'],
	dayNamesShort: ['���','���','���','���','���','���','���'],
	dayNamesMin: ['��','��','��','��','��','��','��'],
	dayStatus: 'DD', dateStatus: 'D, M d',
	dateFormat: 'dd.mm.yy', firstDay: 1,
	initStatus: '', isRTL: false,
	showMonthAfterYear: false, yearSuffix: ''
	});

	$("#date").click(function() {
	    $(this).datepicker("show");
	});


    $("#inn, #tax_dep, #okato, #phone_prefix, #phone_number").on('keypress', function(event) {
        if(event.charCode == 0) return true;
        var max = $(this).attr('max') || 9;
        if((isNaN(String.fromCharCode(event.keyCode)) && isNaN(String.fromCharCode(event.charCode))) || this.value.length +1 > max) return false;
    });

    $("#okved").on('keypress', function(event) {
        if(event.charCode == 0) return true;
        if((isNaN(String.fromCharCode(event.keyCode)) && isNaN(String.fromCharCode(event.charCode))) && (event.keyCode != 46 || event.charCode != 46) || this.value.length > 7) return false;
        this.value = formatOkvedNum(this.value);
        if((event.keyCode == 46 || event.charCode == 46) && this.value.charAt(this.value.length - 1) == '.') return false;
    });

    $(".money_moving input").on('keypress', function(event) {
        if(event.charCode == 0) return true;
        if((isNaN(String.fromCharCode(event.keyCode)) && isNaN(String.fromCharCode(event.charCode))) && !(event.keyCode == 46 || event.keyCode == 44 || event.charCode == 46 || event.charCode == 44) || ((event.keyCode == 46 || event.keyCode == 44 || event.charCode == 46 || event.charCode ==44) && (!!this.value.match(/\./) || !!this.value.match(/,/)))) return false;
    });

    $("#fiscal_year").on('change', function(){
        $(".money_moving .year").text($(this).val());
    });

    $("#okved").autocomplete({
        dataType: "json",
        delay:10,
        minChars:1,
        matchSubset:1,
        autoFill:false,
        matchContains:2,
        cacheLength:10,
        selectFirst:true,
        maxItemsToShow:10,
        source: '/okved/searchokved/'
    });

	/*$("#duty_form").validate({
		rules: {
			kv02: {
				required: true
			},
			kv03: {
				required: true,
				minlength: 6				
			}
		},
		messages: {
			kv02: { required: '������� ��� ���������' },
			kv03: { required: '������� ����� ���������'}
		}
	});*/

    function formatOkvedNum(num) {
        if(!num) return '';
        num = num.replace(/\./g, '');
        return num.replace(/(\d{2})/g, '$1.');
    }

});