$(document).ready(function() {
/*-------------------------------------------binding------------------------------------------------------------------*/
    var shCapitalPercent = true; //���������� ����������� �� ������� ����������� �������/����
    $('.add').live('click', function () {
        var relative = $(this).parent().parent(),
            parent = relative.parent(),
            count = $(parent).find('div.shareholder, div.newShareholder').length + 1,
            el = relative.clone().insertAfter(relative);
        el.find('.remove').css('display', 'inline-block'); //�� ����� show()
        el.find('input:not("input#sh")').val('');
        el.find('input#sh').val('�������� ' + count);
        el.find('input#newSH').val('����� �������� ' + count);
        el.find('span.add_button').attr('title', '��������'); //poshytip ������ ��� ��� ���������
        el.find('.info').hide();
    });
//������ ��������
    $('.remove').live('click', function () {
        var model = $(this).parent().parent().parent().attr('id');
        $(this).parent().parent('div').remove();
        if (!checkAllEllEmpty('div.shareholders', 'div.shareholder')) {
            fillOverallResults(model);
        }
        $('.tip-yellowsimple').remove();//������� ����� �� ��������� poshytip
    });

    $('input#CharterCapital').live("change keyup input", function() {
        var result = parseFloat(this.value),
            allSh = $('div.shareholders div.shareholder'),
            allEllEmpty = checkAllEllEmpty('div.shareholders', 'div.shareholder');
        if (result) {
            $('#CharterCapitalSumm').hide();
            $('.clearCharterCapital').show();
            $.each(allSh, function() {
                fillElem(this);
            });
        } else if(!result && allEllEmpty){
            $('#CharterCapitalSumm').hide();
            $('.clearCharterCapital').hide();
            $('div.overallResults').hide();
        } else {
            $('#CharterCapitalSumm').show();
            $('.clearCharterCapital').hide();
        }
        fillOverallResults();
        var ells = $("div.shareholders div.shareholder input#shCapitalMoney");
        for (var i = 0; i < ells.length; i++) {
            checkValue(ells[i], result);
        }
    });

    $('.clearCharterCapital').on('click', function(){
        $('input#CharterCapital').val('').keyup();
    });

    $('div.shareholders input:not("input#sh"), input#CharterCapital').live("change keyup input", function() {
        if (this.value.match(/[^0-9.]/g)) {
            this.value = this.value.replace(/[^0-9.]/g, '');
        }
    });
//������������� � ������� ��� ��������� ������� ������, ������� ��������� ��������
    var ellsArr = 'div.shareholders input#shCapitalMoney, div.shareholders #shCapitalPercent,' +
        ' div.shareholders #shCapitalFraction1, div.shareholders #shCapitalFraction2';
    $(ellsArr).live('keyup', function (e) {
        var result = parseFloat($('input#CharterCapital').val()),
            key = e.charCode || e.keyCode || 0,
            allEmpty = checkAllEllEmpty('div.shareholders', 'div.shareholder');
        if (!result && key != 0 && !allEmpty){
           /* $('#CharterCapitalSummFill').hide();*/
            $('#CharterCapitalSumm').show();
        } else {
            $('#CharterCapitalSumm').hide();
            $('div.overallResults').hide();
        }
        $('#redistributedCharterCapital, #newShCapitalMoney').val('');
        if(result) {
            var _this = $(this).parent().parent(),
               selector = $(this).attr('id'),
               currentShCM = parseFloat($(_this).find("#shCapitalMoney").val()) <= result ? parseFloat($(_this).find("#shCapitalMoney").val()): null,
               currentShCP = parseFloat($(_this).find("#shCapitalPercent").val()) <= 100 ? parseFloat($(_this).find("#shCapitalPercent").val()) : null,
               currentShCC = parseFloat($(_this).find("#shCapitalFraction1").val()) / parseFloat($(_this).find("#shCapitalFraction2").val()) * 100;
            _this.find('.info').css('display', 'none');
            switch (selector) {
                case 'shCapitalMoney':
                    checkValue(this, result);
                    var summ = currentShCM ? Math.round(currentShCM * 100 * 100 / result) / 100: '',
                        fractions = curtFraction(result, currentShCM);
                    if(summ || key == 8) $(_this).find("#shCapitalPercent").val(summ);
                    checkValue($(_this).find("#shCapitalPercent"), 100);
                    if(fractions[0] && fractions[1]) {
                        $(_this).find("#shCapitalFraction1").val(fractions[0]);
                        $(_this).find("#shCapitalFraction2").val(fractions[1]);
                    }
                    fillOverallResults();
                    break;
                case 'shCapitalPercent':
                    checkValue(this, 100);
                    var summ = currentShCP ? Math.round(result * currentShCP / 100 * 100) / 100 : '',
                        fractions = curtFraction(100, currentShCP);
                    if(summ || key == 8) $(_this).find("#shCapitalMoney").val(summ);
                    checkValue($(_this).find("#shCapitalMoney"), summ);
                    if(fractions[0] && fractions[1]) {
                        $(_this).find("#shCapitalFraction1").val(fractions[0]);
                        $(_this).find("#shCapitalFraction2").val(fractions[1]);
                    }
                    fillOverallResults();
                    break;
                case 'shCapitalFraction1':
                    if(currentShCC){
                        checkValue(this, parseFloat($(_this).find("#shCapitalFraction2").val()));
                        var summ = currentShCC <= 100 ? Math.round(result * currentShCC / 100 * 100) / 100 : '';
                        if (summ || key == 8) $(_this).find("#shCapitalMoney").val(summ);
                        if (summ || key == 8) $(_this).find("#shCapitalPercent").val(Math.round(currentShCC * 100) / 100);
                        fillOverallResults();
                    }
                    break;
                case 'shCapitalFraction2':
                    if(currentShCC){
                        checkValue(this, parseFloat($(_this).find("#shCapitalFraction1").val()), true );
                        $(_this).find("#shCapitalFraction1").keyup();
                    }
                    break;
                default:
                    break;
            }
        }
    });

    $('button#CharterCapitalSumm').live('click', function() {
        $('button#CharterCapitalSumm').hide();
        var allSh = $('div.shareholders div.shareholder'),
            summ = countCC(allSh);
        if(summ) {
            $('.clearCharterCapital').show();
        } else {
            $('.clearCharterCapital').hide();
        }
    });

    $('button#CharterCapitalSummFill').live('click', function() {
        var allEmptyElems = getEmptyElems('div.shareholders', 'div.shareholder'),
            capital = parseFloat($('input#CharterCapital').val()),
            freemoney = capital - coutntAllColl('shCapitalMoney');
        if (allEmptyElems.length && freemoney > 0) {
            fillEmptyElems(allEmptyElems, freemoney);
        } else if (freemoney > 0) {
            var count = $('div.shareholders div.shareholder').length + 1;
            var last = $('div.shareholders div.shareholder:last');
            var newEl = last.clone(false, false).insertAfter(last);
            newEl.animate({ backgroundColor:"#87c880" }, 1000).animate({ backgroundColor:"" }, 1000).find('.remove').css('display', 'inline-block') //�� ����� show()
                .parent().parent().find('input:not("input#sh")').val('')
                .parent().parent().find('input#sh').val('�������� ' + count);
            fillEmptyElems(newEl, freemoney);
        }
    });

    $('button#redistributeFractionsAndCapital').click(function(){
        var allSh = $('div.existShareholders div.shareholder, .newShareholder');
        redistributeFractions(allSh);
    });
var redistrShArray = 'div.newShareholder div.newShCapitalPercent input, div.newShareholder div.newShCapitalFraction input,' +
    ' div.existShareholders div.newShCapitalPercent input, div.existShareholders div.newShCapitalFraction input';
    $(redistrShArray).live('keyup', function() {
        var id = $(this).attr('id');
        if (id == 'newShCapitalFraction1' || id == 'newShCapitalFraction2') {
            checkValue(this, parseFloat($(this).parent().find("#newShCapitalFraction2").val()));
        } else {
            checkValue(this, 100);
        }
        fillOverallResults('sharesRedistributionCalc');
    });
    $('#reload').on('click', function() {
        window.location.reload();
    });
    $('#correct').on('click', function() {
        openAdditionalCalc($('.dropdownSelect li#empty'));
        $('#mainCalculator .buttons #correct').hide();
        $('.dropdownSelect li#empty').addClass('current').css('display', 'inline-block').siblings().removeClass('current');
    });

    $('.dropdownSelect li').click(function() {
        var _this = $(this);
        if(_this.parent().hasClass('visible')) {
            if(!_this.hasClass('current')) {
                _this.addClass('current');
                openAdditionalCalc(_this);
            }
            _this.siblings().removeClass('current').hide();
            _this.parent().removeClass('visible');
        } else {
            if(!_this.hasClass('current')) _this.addClass('current');
            _this.siblings().removeClass('current').show();
            _this.parent().addClass('visible');
        }
    });

/*    $('.dropdownSelect li.sharesRedistribution').click(function(){
        var _this = $(this);
        if(_this.parent().hasClass('visible')){
            if(!_this.hasClass('current')) {
                _this.addClass('current');
            }
            _this.siblings().removeClass('current').hide();
            _this.parent().removeClass('visible');
            $('input#CharterCapital').attr('disabled', 'disabled');
            $('div.shareholders').find('input').attr('disabled', 'disabled');
        }
    });*/

    $(document).click(function(e){
        if(!$(e.target).closest('ul.dropdownSelect').length){
            $('ul.dropdownSelect li:not(.current)').hide();
            $('ul.dropdownSelect').removeClass('visible');
        }
    });



    $('.shareholder .shCapitalFraction, .newShareholder .newShCapitalFraction').hide();

    //--------------------------------------------------------------------------------------------------------------

    $( ".slider" ).click(function() {
        if(shCapitalPercent) {
            $(this).animate({backgroundPosition: -43}, "slow", function() {
                /*jQuery(this).attr('src', settings.switch_off_container_path);
                switched_off_callback();*/
            });
            shCapitalPercent = false;
            $('div.shareholders div.shareholder div.shCapitalFraction, div.existShareholders .newShCapitalFraction, .newShareholder .newShCapitalFraction').show();
            $('div.shareholders div.shareholder div.shCapitalPercent, div.existShareholders .newShCapitalPercent, .newShareholder .newShCapitalPercent').hide();
        }
        else {
            $(this).animate({backgroundPosition: 0}, "slow", function() {
                /*switched_on_callback();*/
            });
            /*jQuery(this).find('.iphone_switch').attr('src', settings.switch_on_container_path);*/
            shCapitalPercent = true;
            $('div.shareholders div.shareholder div.shCapitalPercent, div.existShareholders .newShCapitalPercent, .newShareholder .newShCapitalPercent').show();
            $('div.shareholders div.shareholder div.shCapitalFraction, div.existShareholders .newShCapitalFraction, .newShareholder .newShCapitalFraction').hide();
        }
    });

    //--------------------------------------------------------------------------------------------------------------


    /*$( ".slider" ).slider({
        range: false,
        animation: true,
        min: 0,
        max:1,
        animate: "normal",
        step: 0.02,
        slide: function(event, ui) {
            var currentValue = Math.round( ui.value );
        },
        stop: function(event, ui) {
            var slider = $(".slider .ui-slider-handle"),
                currentValue = Math.round(ui.value);
            *//*$range.animate({'width': currentValue*25 + '%'}, 100);*//*
            slider.animate({'left': currentValue*100 + '%'}, 50, function() {
               if(currentValue == 1) {
                   shCapitalPercent = false;
                   $('div.shareholders div.shareholder div.shCapitalFraction, div.existShareholders .newShCapitalFraction, .newShareholder .newShCapitalFraction').show();
                   $('div.shareholders div.shareholder div.shCapitalPercent, div.existShareholders .newShCapitalPercent, .newShareholder .newShCapitalPercent').hide();
               } else {
                   shCapitalPercent = true;
                   $('div.shareholders div.shareholder div.shCapitalPercent, div.existShareholders .newShCapitalPercent, .newShareholder .newShCapitalPercent').show();
                   $('div.shareholders div.shareholder div.shCapitalFraction, div.existShareholders .newShCapitalFraction, .newShareholder .newShCapitalFraction').hide();
               }
                *//*if ( o && discounts[currentValue] && discounts[currentValue].m == '>12'*//*
            });
        }
    });*/

/*----------------------------------------------functions-------------------------------------------------------------*/
//������� ��������� ��������
// elArr - ������ ����������.
    function countCC(elArr){
        var shCMSumm = null,
            percentSumm = null,
            shCMEmpty = false,
            resultEmpty = [];
        for (var i = 0; i < elArr.length; i++) {
            var shCM = parseFloat($(elArr[i]).find("#shCapitalMoney").val()),
                shCP = parseFloat($(elArr[i]).find("#shCapitalPercent").val()),
                shCC = parseFloat($(elArr[i]).find("#shCapitalFraction1").val()) / parseFloat($(elArr[i]).find("#shCapitalFraction2").val()) * 100,
                percent = shCP || shCC;
            if (shCM && (shCP || shCC)) {
                var result = Math.round(shCM * 100 / percent * 100) / 100;
                $('input#CharterCapital').val(result);
                for (var j = 0; j < elArr.length; j++) {
                    fillElem(elArr[j]);
                }
                $(elArr[i]).animate( { backgroundColor:"#87c880"}, 1000 ).animate( { backgroundColor:""}, 1000 ).find('.info').css('display', 'inline-block');
                //fillOverallResults();
                return true;
            }
            if (shCM) {
                shCMSumm += shCM;
            } else {
                shCMEmpty = true;
            }
            if (percent) {
                percentSumm += percent;
            }
            if (!shCM && !percent) {
                resultEmpty.push([i]);
            }
        }
        if (!resultEmpty.length){
            if (!shCMEmpty) {
                $('input#CharterCapital').val(shCMSumm);
            } else {
                var shCMPercent = 100 - percentSumm;
                var shCMResult = Math.round(shCMSumm/shCMPercent * 100 * 100) / 100;
                $('input#CharterCapital').val(shCMResult);
            }
            for (var j = 0; j < elArr.length; j++) {
                fillElem(elArr[j]);
            }
            fillOverallResults();
            return true;
        } else {
            $('input#CharterCapital').val('');
            $.each(resultEmpty, function(index, val) {
                $($('div.shareholders .shareholder')[val]).animate({ backgroundColor:"red" }, 1000).animate({ backgroundColor:"" }, 1000);
            });
            return false;
        }
    }
//���������� �������� �������� ����� ���������.
    function fillOverallResults(model) {
        if(!model || model == 'mainCalculator') {
            var result = parseFloat($('input#CharterCapital').val()) || 0,
                overallMoney = coutntAllColl('shCapitalMoney'),
                overallPercent = coutntAllColl('shCapitalPercent'),
                overallMoneyClass = overallMoney <= result ? 'green' : 'red',
                overallPercentClass = overallPercent <= 100 ? 'green' : 'red',
                freeMoney = Math.round((result - overallMoney) * 100) / 100,
                freePercent = Math.round((100 - overallPercent) * 100) / 100,
                freeMoneyClass = freeMoney >= 0 ? 'green' : 'red',
                freePercentClass = freePercent >= 0 ? 'green' : 'red',
                dataBlock = $('div.shareholders div.overallResults');
            $(dataBlock).show();
            $(dataBlock).find('span#money').text(overallMoney + '���.').removeClass().addClass(overallMoneyClass);
            $(dataBlock).find('span#percent').text(overallPercent + '%').removeClass().addClass(overallPercentClass);
            $(dataBlock).find('span#freeMoney').text(freeMoney + '���.').removeClass().addClass(freeMoneyClass);
            $(dataBlock).find('span#freePercent').text(freePercent + '%').removeClass().addClass(freePercentClass);
            freeMoney > 0 ? $('#CharterCapitalSummFill').show() : $('#CharterCapitalSummFill').hide();
        } else if (model == 'sharesRedistributionCalc') {
            var dataBlock = $('div.sharesRedistribution div.overallResults'),
                overallPercent = coutntAllColl('newShCapitalPercent', model),
                overallFraction = coutntAllFractions('sharesRedistributionCalc'),
                overallPercent = overallPercent || overallFraction * 100,
                overallPercentClass = overallPercent <= 100 ? 'green' : 'red';
            $(dataBlock).find('span#percent').text(overallPercent + '%').removeClass().addClass(overallPercentClass);
            $(dataBlock).show();
        }
    }
//������� �������� ���� ������� ��� ���������� ��������(����� �������: ������, ��������)
// name - input id, model ��� ������������
    function coutntAllColl(name, model) {
        if (!name) return null;
        if (!model) {
            var rowArr = $('div.shareholders input#' + name);
        } else if(model == 'sharesRedistributionCalc') {
            var rowArr = $('div.sharesRedistribution input#' + name);
        }
            var val = null;
        for (var i = 0; i < rowArr.length; i++){
            val += rowArr[i].value ? parseFloat(rowArr[i].value) : 0;
        }
        return Math.round(val * 100) / 100;
    }
//������� �������� ���� ������� ��� ���������� ��������(����� �������: ������, ��������, ����)
// name - input id, model ��� ������������
    function coutntAllFractions(model) {
        if (!model || model == 'sharesRedistributionCalc') {
            var ellsArr = $('div.sharesRedistribution div.shCapitalFraction');
        } else if(model == 'sharesRedistributionCalc') {
            var ellsArr = $('div.sharesRedistribution div.newShCapitalFraction');
        }
        var val = null;
        for (var i = 0; i < ellsArr.length; i++){
            var topFraction = parseFloat($(ellsArr[i]).find('#newShCapitalFraction1').val()),
                botFraction = parseFloat($(ellsArr[i]).find('#newShCapitalFraction2').val());
            if(topFraction && botFraction) {
                val += topFraction / botFraction;
            }
        }
        return Math.round(val * 100) / 100;
    }
//��������� �������� � �������� � ���������
// el- ������� compare ����� ������ back - �������� ������������������
    function checkValue(el, compare, back) {
        var val = parseFloat($(el).val());
        var bool = val > compare;
        if (back) bool = !bool;
        if (bool) {
            $(el).addClass('red');
            return '';
        } else {
            $(el).removeClass('red');
            return val;
        }
    }
//�������� ������ ������ ���������
// arr -��� ����, el- ��� ����
    function getEmptyElems(arr, el) {
        var allEls = $(arr).find(el),
            emptEls = [];
        for (var i = 0; i < allEls.length; i++) {
            if (isElemEmpty(allEls[i])) {
                emptEls.push(allEls[i]);
            }
        }
        return emptEls;
    }
//��������� ������ ������
// els-������ ��������, freemoney - ��������� ��������
    function fillEmptyElems(els, freemoney) {
        var count = els.length,
            elMoney = freemoney / count;
        for(var i = 0; i < count; i++) {
            $(els[i]).find('#shCapitalMoney').val(elMoney);
        }
        $('div.shareholders input#shCapitalMoney').keyup();
    }
//��������� �� ������� ��������
// el-�������, blockClass-������������ �����(��� ������������� ����� ��������� - �������� ��� ��� �����)
    function isElemEmpty(el, blockClass) {
        var money = !!$(el).find('#shCapitalMoney').val();
        if (blockClass == 'sharesRedistribution') {
            var percent = !!$(el).find('#newShCapitalPercent').val(),
                fraction1 = !!$(el).find('#newShCapitalFraction1').val(),
                fraction2 = !!$(el).find('#newShCapitalFraction2').val();
        } else {
            var percent = !!$(el).find('#shCapitalPercent').val(),
                fraction1 = !!$(el).find('#shCapitalFraction1').val(),
                fraction2 = !!$(el).find('#shCapitalFraction2').val();
        }
        var fraction = fraction1 && fraction2;
        return !(money || percent || fraction);
    }
//�������� ������� ���� ��� ������������� ���� � �������� � ����������� �� ��� ��������� � ��� ������
// el- �������
    function fillElem(el) {
        var money = $(el).find('#shCapitalMoney'),
            percent = $(el).find('#shCapitalPercent'),
            fraction1 = $(el).find('#shCapitalFraction1'),
            fraction2 = $(el).find('#shCapitalFraction2'),
            fraction = parseFloat(fraction1.val()) / parseFloat(fraction2.val());
        if (!!parseFloat(money.val())) {
            money.keyup();
            return;
        }
        if (!!fraction) {
            fraction1.keyup();
            return;
        }
        if (!!parseFloat(percent.val())) {
            percent.keyup();
        }
    }
//�������� �� ������� ��������� ������ ���������
// arr -��� ����, el- ��� ����
    function checkAllEllEmpty(arr, el) {
        var allEmptyElems = getEmptyElems(arr, el),
            allElems = $(el);
        return allEmptyElems.length == allElems.length;
    }
//���������� ���������� �����
    function curtFraction(n, m) {
        var M = m,
            N = n;
        for (var i = 2; i <= m; i++) {
            if (m % i === 0 &&  n % i === 0)
               M = m / i,
               N = n / i;
        }
        if (M && N) {
            return [M, N];
        } else {
            return [null, null];
        }
    }
//����������������� �����
// elArr - ������ ����������
    function redistributeFractions(elArr){
        if (!elArr) return;
        var summ = null;
        //������� ������������ ����
        for (var i = 0; i < elArr.length; i++) {
            var shCMoney = parseFloat($(elArr[i]).find("#shCapitalMoney").val()) || 0,
                newShCP = parseFloat($(elArr[i]).find("#newShCapitalPercent").val()),
                newShCC = parseFloat($(elArr[i]).find("#newShCapitalFraction1").val()) / parseFloat($(elArr[i]).find("#newShCapitalFraction2").val()) * 100,
                newPercent = newShCP || newShCC,
                calc = shCMoney * 100 / newPercent;
            summ = calc > summ ? Math.round(calc * 100) / 100 : summ;
        }
        $('input#redistributedCharterCapital').val(summ);
        for (var i = 0; i < elArr.length; i++) {
            var newShCP = parseFloat($(elArr[i]).find("#newShCapitalPercent").val()),
                newShCC = parseFloat($(elArr[i]).find("#newShCapitalFraction1").val()) / parseFloat($(elArr[i]).find("#newShCapitalFraction2").val()) * 100,
                newPercent = newShCP || newShCC,
                capital = Math.round(summ * newPercent / 100 * 100) / 100 || '';
            $(elArr[i]).find("#newShCapitalMoney").val(capital);
        }
    }

//������ ��������� � ��������� ������������
    function sharesRedistribution() {
        var existSh = $('.shareholders > div.shareholder').clone(true);
        $(existSh).find('input#sh, input#newShCapitalMoney').attr('disabled', 'disabled');
        if (!shCapitalPercent) {
            $(existSh).find('div.newShCapitalFraction').css('display', '');
        } else {
            $(existSh).find('div.newShCapitalPercent').css('display', '');
        }
        $(existSh).find('div.newShCapitalMoney').css('display', '');
        $(existSh).find('div.shCapitalMoney, div.shCapitalPercent, div.shCapitalFraction').css('display', 'none');
        $(existSh).find('.shHelp').css('display', 'inline-block'); //�� ����� show()
        $(existSh).find('.input_ctrl').remove();
        $(existSh).appendTo('.existShareholders');
    }
//��������� �������������� �����������
// el - ������� li �� �������� ��������� �������
    function openAdditionalCalc(el){
        var model = el ? $(el).attr('id') : null;
        switch (model){
            case 'sharesRedistribution':
                sharesRedistribution();
                $('input#CharterCapital').attr('disabled', 'disabled');
                var el = $('div.shareholders');
                el.find('input').attr('disabled', 'disabled');
                el.find('div.input_ctrl').hide();
                $('div.additionalCalculators div.sharesRedistribution').show();
                $('#mainCalculator .buttons #correct').show();
                $('div.additionalCalculators div.sharesRedistribution').siblings().hide();
                break;
            case 'bulbulator':
                $('div.additionalCalculators div.bulbulator').show();
                $('div.additionalCalculators div.bulbulator').siblings().hide();
                break;
            case 'empty':
                $('.existShareholders').empty();
                $('input#CharterCapital').removeAttr('disabled');
                $('div.shareholders').find('div.input_ctrl').css('display', 'inline-block');
                $('div.shareholders').find('input').removeAttr('disabled');
                $('div.additionalCalculators > div').hide();
                break;
            default :
                break;
        }
    }
});