/**
 * Created by JetBrains PhpStorm.
 * User: Vasilevs
 * Date: 14.08.12
 * Time: 16:23
 */

$(document).ready(function () {

    tinyMCE.init({
        mode : "exact",
        elements : "good_response, bad_response, profile_adv_content",
        theme : "advanced",
        language : "ru",
        plugins : "paste,searchreplace,inlinepopups",
        width: "664",
        height: "200",
        theme_advanced_buttons1 : "bold,italic,underline,blockquote,inquotes,link,unlink,|,search,replace,|,bullist,numlist,|,undo,redo,|,code", //
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_buttons4 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : false,
        theme_advanced_blockformats : "p,h2,blockquote",
        content_css : "/xcss/tinymce_body.css",
        onchange_callback: $.regforum.profile.companies.add.editorCounter,
        handle_event_callback: $.regforum.profile.companies.add.editorCounter,
        setup : function(ed) {
            ed.addButton('inquotes', {
                title : '� ��������',
                image : '/ximg/toolbar/in_quotes.png',
                onclick : function() {
                    ed.focus();
                    ed.selection.select();
                    ed.selection.setContent('&laquo;'+ed.selection.getSel()+'&raquo;');
                }
            });
        },
        verify_html : true,
        inline_styles : false,
        apply_source_formatting : true,
        convert_fonts_to_spans : true,
        //valid_children : "-div[style,class],-p[style],-span[style,class]",
        valid_elements : "a[href|target],strong,br,em,p[align],blockquote,u,ol,ul,li,del",
        invalid_elements : "iframe,script,applet,object,h2,h3,h4,h5,h6,h7,h8,h9,cite,button,section,head,header,footer,article,embed,pre,img",
        removeformat_selector : 'b,strong,em,i,span,ins,div,li,ul,h2',
        'formats' : {
            'italic' : {'inline' : 'em'},
            'bold' : {'inline' : 'strong'},
            'underline' : {'inline' : 'u'},
            'strikethrough' : {'inline' : 'del'}
        },
        removeformat : [
            {selector : 'div,b,strong,em,i,font,u,strike', remove : 'all', split : true, expand : false, block_expand : true, deep : true},
            {selector : 'span', attributes : ['style', 'class'], remove : 'empty', split : true, expand : false, deep : true},
            {selector : '*', attributes : ['style', 'class'], remove: 'all', split : true, expand : true, deep : true}
        ],
        protect: [
            /\<\/?(if|endif)\>/g, // Protect <if> & </endif>
            /\<xsl\:[^>]+\>/g, // Protect <xsl:...>
            /<\?php.*?\?>/g // Protect php code
        ]
    });

});