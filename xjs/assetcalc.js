/**
 * Created with JetBrains PhpStorm.
 * User: RegforumTeam
 * Date: 07.06.13
 * Time: 10:20
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function() {
    $('.actives, .liabilities, .percentage').find('input')
        .keydown(function(){ return ((event.keyCode>0)&&(event.keyCode<58)||(event.keyCode>95)&&(event.keyCode<106)||(event.keyCode==32)||(event.keyCode==17)||(event.keyCode==189)||(event.keyCode==187));})
        .keyup(function(){
        var activesArr = $('.actives').find('input'),
            liabilitiesArr = $('.liabilities').find('input'),
            liabVal = 0,
            actVall = 0;
        for (i=0; i<activesArr.length; i++) {
            var activeVal = parseInt($('.actives').find('input')[i].value, 10);
            if (activeVal) {
                actVall += activeVal;
            } else {
                continue;
            }
        }
        for (i=0; i<liabilitiesArr.length; i++) {
            var liabilityVal = parseInt($('.liabilities').find('input')[i].value, 10);
            if (liabilityVal) {
                liabVal += liabilityVal;
            } else {
                continue;
            }
        }
        if (actVall) {
            $('div.LASumm').find('#activeSumm:input').val(actVall + ' ���.���.');
        }
        if (liabVal) {
            $('div.LASumm').find('#liabilitiesSumm:input').val(liabVal + ' ���.���.');
        }
        if (actVall && liabVal) {
            var summ = actVall-liabVal;
            $('input#finalSumm').val(summ + ' ���.���.');
        }
        if (!summ) {
            return; //���� ����� ��� ������������
        }
        var percent1 = $('div.percentage div.percent input#percent1').val(),
            percent2 = $('div.percentage div.percent input#percent2').val(),
            decimal1 = $('div.percentage div.decimalFraction input#decimal1').val(),
            decimal2 = $('div.percentage div.decimalFraction input#decimal2').val(),
            simple1 = $('div.percentage div.simpleFraction input#simple1').val(),
            simple2 = $('div.percentage div.simpleFraction input#simple2').val();
        if (percent1 || percent2) {
            var compositePercent = parseInt(percent1) + parseFloat('0.'+percent2);
            if (compositePercent <= 100) {
                var finalSumm = Math.round(summ * compositePercent*1000*100/100)/100;
                $('div.percentage span.error').hide();
            }
            else {
                $('div.percentage div.percent span.error').show();
                return;
            }
        }
        else if (decimal1 || decimal2) {
            var compositePercent = parseFloat(decimal1) + parseFloat('0.'+decimal2);
            if (compositePercent <= 1) {
                var finalSumm = summ * compositePercent*1000;
                $('div.percentage span.error').hide();
            }
            else {
                $('div.percentage div.decimalFraction span.error').show();
                return;
            }
        }
        else if (simple1 && simple2) {
            var compositePercent = parseFloat(simple1) / parseInt(simple2);
            if (compositePercent <= 1) {
                var finalSumm = summ * Math.round(compositePercent*1000*100)/100;
                $('div.percentage span.error').hide();
            }
            else {
                $('div.percentage div.simpleFraction span.error').show();
                return;
            }
        }
        if (finalSumm){
            $('div.realSumm').show().find('input').val(finalSumm + ' ���.');
        }
    });
    $('.percentage').find('input').keyup(function(){
        if ($(this).val()!='') {
            $(this).parent().siblings().find('input').attr('disabled','disabled');
        } else {
            $(this).parent().siblings().find('input').removeAttr('disabled','disabled');
        }
    });
});
