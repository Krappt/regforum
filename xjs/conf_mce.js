/**
 * Created by JetBrains PhpStorm.
 * User: Vasilevs
 * Date: 14.08.12
 * Time: 16:23
 */

$(document).ready(function () {

    var user_dialog = $('#user_dialog'),
        timeout;

    if (user_dialog.length ){
        user_dialog.dialog({
            modal: true,
            autoOpen: false,
            closeOnEscape: true,
            resizable: false,
            title: "������ �� ���������",
            height: 500,
            buttons: {
                "�������": function() {
                    $(this).dialog("close");
                }
            }
        });
    }

    tinyMCE.init({
        mode : "exact",
        elements : "company_about, topic_content",
        theme : "advanced",
        language : "ru",
        plugins : "paste,preview,fullscreen,searchreplace,inlinepopups",
        width: "970",
        height: "460",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,outdent,indent,blockquote,inquotes,user,|,search,replace,|,pasteword,|,bullist,numlist,|,undo,redo,|,image,insertimage,link,unlink,|,formatselect,|,removeformat,preview,code,fullscreen",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_buttons4 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : false,
        convert_urls : false,
        theme_advanced_blockformats : "p,h2,blockquote",
        content_css : "/xcss/tinymce_body.css?12",
        setup : function(ed) {
            ed.addButton('inquotes', {
                title : '� ��������',
                image : '/ximg/toolbar/in_quotes.png',
                onclick : function() {
                    ed.focus();
                    ed.selection.select();
                    ed.selection.setContent('&laquo;'+ed.selection.getSel()+'&raquo;');
                }
            });
            ed.addButton('user', {
                title : '������ �� ���������',
                image : '/ximg/toolbar/user-add.png',
                onclick : function() {
                    ed.focus();
                    if (!user_dialog.dialog('option', 'disabled')) {
                        user_dialog.dialog("open");
                    }

                    $(".dialog_filter").live('keyup', function() {
                        var _this = $(this);
                        clearTimeout(timeout);
                        timeout = setTimeout(function() {
                            var fastsearch = _this.val();
                            $.getJSON('/posts/getusers/', { fastsearch: fastsearch },
                                function(obj) {
                                    if (obj) {
                                        $("#user_dialog_content").html(obj._content);
                                    }
                                });
                        }, 500);
                    });
                    //������� ��� ������
                    $('#user_dialog_content').one('click', 'p', function() {
                        var _this = $(this),
                            parents = _this.parent(),
                            data_id = _this.attr("id"),
                            span = _this.find('span'),
                            data_text;
                        if(span.length) {
                            _this.find('span').remove();
                        }

                        data_text = _this.text();
                        ed.selection.setContent('<a class="member" href="http://regforum.ru/members/' + data_id + '/">' + data_text + '</a>');
                        user_dialog.dialog("close");
                        parents.html('').siblings('.top_filter').find('input').val('');
                    });
                }

            });
        },
        verify_html : true,
        inline_styles : false,
        apply_source_formatting : true,
        convert_fonts_to_spans : true,
        //valid_children : "-div[style,class],-p[style],-span[style,class]",
        valid_elements : "a[href|target|class],strong,br,img[src|width|height|align|hspace|vspace],em,p[align|class],h2,blockquote,u,ol,ul,li,del",
        invalid_elements : "iframe,script,applet,object,h3,h4,h5,h6,h7,h8,h9,cite,button,section,head,header,footer,article,embed,pre",
        removeformat_selector : 'b,strong,em,i,span,ins,div,li,ul,h2',
        'formats' : {
            'alignleft' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-left'},
            'aligncenter' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-center'},
            'alignright' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-right'},
            'alignfull' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-justify'},
            'italic' : {'inline' : 'em'},
            'bold' : {'inline' : 'strong'},
            'underline' : {'inline' : 'u'},
            'strikethrough' : {'inline' : 'del'}
        },
        removeformat : [
            {selector : 'div,b,strong,em,i,font,u,strike', remove : 'all', split : true, expand : false, block_expand : true, deep : true},
            {selector : 'span', attributes : ['style', 'class'], remove : 'empty', split : true, expand : false, deep : true},
            {selector : '*', attributes : ['style', 'class'], remove: 'all', split : true, expand : true, deep : true}
        ],
        protect: [
            /\<\/?(if|endif)\>/g, // Protect <if> & </endif>
            /\<xsl\:[^>]+\>/g, // Protect <xsl:...>
            /<\?php.*?\?>/g // Protect php code
        ]
    });

});