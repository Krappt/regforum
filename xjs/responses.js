/**
 * Created with JetBrains PhpStorm.
 * User: RegforumTeam
 * Date: 13.02.14
 * Time: 11:09
 * To change this template use File | Settings | File Templates.
 * TODO REG-250
 */
function ResponseComplaint(src, user, mode, dialog, title) {

    mode = mode || 'responseAddress';
    var title = title || "������ �� �����";
    $('#comlaint_description').val('');

    var complaint_dialog = dialog ? $(dialog) : $('#complaint_dialog');

    if (complaint_dialog.length ){
        complaint_dialog.dialog({
            modal: true,
            autoOpen: false,
            closeOnEscape: true,
            resizable: false,
            title: title,
            //height: 370,
            width: 510,
            buttons: {
                "������": function() {
                    $(this).dialog("close");
                },
                "���������": function() {
                    var c_description = complaint_dialog.find('#comlaint_description').val(),
                        type;
                    if(mode == 'responseService') {
                        var ells = complaint_dialog.find('.complaint_motive');
                        type = new Array();
                        ells.each(function(){
                            if($(this).is(':checked')) type.push($(this).attr('id'));
                        });
                    } else {
                        type = complaint_dialog.find('#complaint_motive').val();
                    }

                    if(c_description.length > 5) {
                        complaint_dialog.find('.errorMessage').hide();
                        $.ajax('/companies/setcomlaint/', {
                            type: "GET",
                            data: { src: src, user: user, cd: c_description, mode: mode, type: type },
                            cache: false,
                            success: function(response){
                                if(mode == 'responseService') {
                                    $('.service_footer').find('.complaint').removeClass('ll').removeClass('complaint').unbind('click').removeAttr('onClick').text('���� ������ ����������');
                                }
                            }
                        });
                        $(this).dialog("close");
                    }
                    else {
                        complaint_dialog.find('.errorMessage').show();
                    }
                }
            }
        });
    }

    if (!complaint_dialog.dialog('option', 'disabled')) {
        complaint_dialog.dialog("open");
    }
}