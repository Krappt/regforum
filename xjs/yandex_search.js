/**
 * Created with JetBrains PhpStorm.
 * User: RegforumTeam
 * Date: 27.03.14
 * Time: 15:40
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function() {

    var cat = $('.search_categories'),
        hash = window.location.hash;
    cat.find('.select_button').on('click', function(){
        var _this = $(this);
        if (_this.parent().hasClass('disabled')) return;
        if (_this.hasClass('active')) {
            _this.removeClass('active');
            _this.find('span.arrow_down').show();
            _this.siblings('div.categories_list').hide();
        } else {
            _this.addClass('active');
            _this.find('span.arrow_down').hide();
            _this.siblings('div.categories_list').show();
        }
    });

    $(document).on('click', function(e){
        if(!$(e.target).closest('.search_categories').length){
            cat.find('div.select_button').removeClass('active');
            cat.find('div.categories_list').hide();
            cat.find('span.arrow_down').show();
        }
    });

    $('.yandex_search').on('keypress', 'input#search_value', function(event){
        if (event.which == 13 || event.keyCode == 13) {
            $('.yandex_search span#search').click();
        }
    });

    $('.yandex_search').on('click', 'span#search', function(){
        var url = '/yandsearch/',
            yaIdName = 'searchid',
            yaConstraintid = 'constraintid',
            yaText = 'text',
            params = deparam(hash),
            main = $(this).parent(),
            yaId = main.find('input#searchid').val(),
            value = main.find('input.search').val(),
            searched_value = main.find('input#searched_value').val(),
            id = main.find('li.selected').attr('id'),
            searchid = main.find('input.searchid').val();

        if(value) {
            url = addParameterToURL(url, yaIdName, yaId);
            url = addParameterToURL(url, yaText , value);
            if(id) {
                url = addParameterToURL(url, yaConstraintid, id, true);
            }
            document.location = url;
            if(params.constraintid && params.constraintid != id && value == searched_value) { //������� ��-�� ����, ��� ������� �� ��������� �������� ��� ���������� ����
                document.location.replace(url);
                document.location.reload();
            }
        }
    });

    cat.find('.categories_list').on('click', 'li', function(){
        closeCategoriesList(this);
        setCategoryTitle(this);
        $(this).addClass('selected').siblings('li').removeClass('selected');
    });

    /**
     * �������������� ����������� ���������� ��������(�� �������� ����, ���� ����)
     */
    if(hash) {
        var params = deparam(hash),
            id = params.constraintid ? params.constraintid : 'all',
            li = cat.find('.categories_list li#' + id);
        li.addClass('selected').siblings('li').removeClass('selected');
        setCategoryTitle(li);
    }

    function closeCategoriesList(list) {
        var list = $(list).closest('.search_categories');
        list.find('div.select_button').removeClass('active');
        list.find('div.categories_list').hide();
        list.find('span.arrow_down').show();
    }

    function setCategoryTitle(list) {
        var parent = $(list).closest('.search_categories');
        parent.find('span.select').text($(list).text());
    }

    function addParameterToURL(url, name, value, hash){
        if(hash) {
            url += (url.split('#')[1] ? '&':'#') + name + '=' + value;
        } else {
            url += (url.split('?')[1] ? '&':'?') + name + '=' + value;
        }
        return url;
    }

    function deparam(querystring) {
        // remove any preceding url and split
        querystring = querystring.substring(querystring.indexOf('#')+1).split('&');
        var params = {}, pair, d = decodeURIComponent;
        // march and parse
        for (var i = querystring.length - 1; i >= 0; i--) {
            pair = querystring[i].split('=');
            params[d(pair[0])] = d(pair[1]);
        }

        return params;
    }
});