$(document).ready(function () {
    var block = $('.fill-profile, .information'),
        jcropApi,
        jcropOverlay = $('.jcrop-overlay'),
        avatarJcrop = jcropOverlay.find('.avatar-jcrop'),
        jcropError = avatarJcrop.find('.error'),
        jcropTip = avatarJcrop.find('.tip'),
        jcropSubmit = avatarJcrop.find('.submit'),
        jcropImage = avatarJcrop.find('.preview')[0],
        jcropClose = avatarJcrop.find('.close'),
        inputAvatar = block.find('#photos'),
        imageOptions = {},
        profilePhoto = $('.photos .profile-photo');

    block.on('change', 'input:not(.objects_list input, [type=file]), textarea', function () {
        var _this = $(this),
            name = _this.attr('name');

        $.ajax('/profile/default/updateprofile/', {
            type: "POST",
            data: { name: name, val: _this.val() },
            cache: false,
            success: function (response) {
                var answer = $.parseJSON(response);
                if (!answer.error) {
                    var inputs = block.find('[name = ' + name + ']');
                    inputs.val(answer.value);
                    !!answer.value ? inputs.addClass('correct').removeClass('incorrect') : inputs.removeAttr('class');
                } else {
                    _this.addClass('incorrect').removeClass('correct');
                }

            }
        });
    });

    jcropClose.on('click', function () {
        jcropOverlay.hide();
        jcropOverlay.find('.jcrop-holder').remove();
        avatarJcrop.removeClass('valid');
    });

    inputAvatar.on('change', function () {
        jcropOverlay.show();
        fileSelectHandler(this)
    });

    profilePhoto.on('click', function(){
        inputAvatar.click();
    });

    jcropSubmit.on('click', function () {
        var files = $('input#photos[type=file]').prop('files')[0];
        if (!files) return false;
        var formData = new FormData();
        formData.append('file', files);
        $.each(imageOptions, function (key, value) {
            formData.append(key, value);
        });

        $.ajax('/profile/default/updateprofile/', {
            type: "POST",
            data: formData,
            cache: false,
            dataType: 'text',
            contentType: false,
            processData: false,
            success: function (response) {
                var answer = $.parseJSON(response);
                if (answer.uri != 'undefined') {
                    $('.photos img.profile-photo').attr('src', answer.uri);
                    jcropOverlay.hide();
                    jcropOverlay.find('.jcrop-holder').remove();
                    $('input#photos[type=file]').val('');
                    $('input#photos[type=file] + label > span:nth-child(1)').hide();
                    $('input#photos[type=file] + label > span:nth-child(2)').show();
                } else {
                    //some action
                }

            }
        });
    });

    function updateInfo(e) {
        imageOptions = e;
        if (e.h > 0 && e.w > 0) {
            avatarJcrop.addClass('valid');
        } else {
            avatarJcrop.removeClass('valid');
        }
    }

    function clearInfo() {
        imageOptions = {};
    }

    function fileSelectHandler(el) {
        var oFile = $(el)[0].files[0],
            rFilter = /^(image\/jpeg|image\/png)$/i,
            oReader = new FileReader();

        jcropError.hide();
        jcropTip.show().removeAttr('style');

        if (!rFilter.test(oFile.type)) {
            jcropError.html('Пожалуйста, выберите картинку формата jpg или png').show();
            jcropTip.hide();
            return;
        }

        if (oFile.size > 5 * 1024 * 1024) {
            alert('Вы выбрали слишком большой файл');
            jcropError.html('Вы выбрали слишком большой файл').show();
            jcropTip.hide();
            return;
        }

        oReader.onload = function (e) {
            jcropImage.src = e.target.result;
            jcropImage.onload = function () {

                if (typeof jcropApi != 'undefined') {
                    jcropApi.destroy();
                    jcropApi = null;
                }

                $(jcropImage).Jcrop({
                    minSize: [32, 32], // min crop size
                    aspectRatio: 1, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfo,
                    onSelect: updateInfo,
                    onRelease: clearInfo
                }, function () {
                    jcropApi = this;
                });

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }

});
