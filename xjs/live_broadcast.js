/**
 * Created by JetBrains PhpStorm.
 * User: Vasilevs
 * Date: 03.10.12
 * Time: 17:39
 */

$(document).ready(function () {
    var sandbox_button = $('.live_broadcast .sandbox'),
        content_button = $('.live_broadcast .all'),
        content = $('#live_broadcast_content'),
        sandbox = $('#live_broadcast_sandbox');

    $('.comment_link').poshytip({
        className: 'tip-yellowsimple',
        showTimeout: 200,
        alignTo: 'target',
        alignX: 'left',
        alignY: 'center',
        offsetX: 10,
        liveEvents: true,
        allowTipHover: false
    });


    $('#reload_live_broadcast').click(function() {
        var _this = $(this);
        _this.fadeTo('fast', 0);
        //$('#live_broadcast_content').html('<img src="/ximg/bcloader.gif" />');
        var topic_id = $('h1').attr('id');
        if(content.is(':visible')) {
            $.ajax('/posts/livebroadcastrefresh', {
                type: "GET",
                data: { topicid: topic_id },
                cache: false,
                success: function(response){
                    _this.fadeTo('fast', 1);
                    content.html(response);
                }
            });
        }
        else {
            sandbox_button.click();
            _this.fadeTo('fast', 1);
        }
    });

    sandbox_button.click(function() {
        content.hide();
        content_button.show();
        sandbox_button.hide();
        $.ajax('/posts/getSandboxLiveBroadcast', {
            type: "GET",
            data: {},
            cache: false,
            success: function(response){
                sandbox.html(response).show();
            }
        });
    });

    content_button.click(function() {
        content.show();
        sandbox_button.show();
        sandbox.hide();
        content_button.hide();
    });

});