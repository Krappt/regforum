/**
 * ������ ��� ������ �� ����� ����������� ��������
 * .dropdown - ��������� ���������� ������
 * .dropdown_list - ����, ���� � �������
 * Created with JetBrains PhpStorm.
 * User: RegforumTeam
 * Date: 01.08.14
 * Time: 12:19
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function () {
    var dropdown = $('.dropdown'),
        dropdownList = $('.dropdown_list'),
        timeout_input, //id ������� �������
        search_timeout = 50; //�������� ��. �� ������;

    if(dropdownList.length) {
        dropdownList.find('.select_button').on('click', function(){
            var _this = $(this);
            closeOtherList(this);
            if (_this.hasClass('active')) {
                _this.removeClass('active');
                _this.find('span.arrow_down').show();
                _this.siblings('div.objects_list').hide();
            } else {
                _this.addClass('active');
                _this.find('span.arrow_down').hide();
                _this.siblings('div.objects_list').show().find('input').focus();
            }
        });

        $(document).on('click', function(e){
            if(!$(e.target).closest(dropdownList).length){
                dropdownList.find('div.select_button').removeClass('active');
                dropdownList.find('div.objects_list').hide();
                dropdownList.find('span.arrow_down').show();
            }
        });

        dropdownList.find('div.objects_list').on('keyup', 'input', function(){
            var text = $(this).val(),
                ells = $(this).parents('.objects_list').find('ul > li:not(.hidden)');
            clearTimeout(timeout_input);
            timeout_input = setTimeout(function() {
                ells.each(function(){
                    var re = new RegExp('(^|\\s)' + text, 'ig'),
                        liText = $(this).text();
                    var result = liText.match(re),
                        child = $(this).find('ul li');
                    if(child.length) {
                        child.each(function(){
                            var childText = $(this).text(),
                                match = childText.match(re);
                            if(match) {
                                $(this).show();
                                result = true;
                            } else {
                                $(this).hide();
                            }
                        });
                    }
                    if(result) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            }, search_timeout);
        });

        dropdownList.on('click', '.objects_list li div', function(){
            if($(this).attr('data') != undefined){
                $(this).closest(dropdownList).find('input:hidden').val($(this).attr('data')).change();
            }
            setObjectTitle(this);
            cleanInputForm(this);
            closeObjectList(this);
            restoreHiddenElls(this);
        });

        /**
         * �� ����������� this (�� �������) ������� ������������ ���� � �������� ��� � �������������� ���������.
         * @param el - this
         */
        function closeObjectList(el) {
            var block = $(el).closest(dropdownList);
            block.find('div.select_button').removeClass('active');
            block.find('div.objects_list').hide();
            block.find('span.arrow_down').show();
        }

        /**
         * �� ����������� this (�� �������) ������� ������������ ���� � ������� ���� ������.
         * @param el - this
         */
        function cleanInputForm(el) {
            var block = $(el).closest(dropdownList).find('div.objects_list');
            block.find('input').val('');
        }

        /**
         * �� ����������� this (�� �������) ������� ������������ ���� � ���������� ��� ��������.
         * @param el - this
         */
        function restoreHiddenElls(el) {
            $(el).closest(dropdownList).find('div.objects_list li').show();
        }

        /**
         * �� ����������� this (�� �������) ������� �������� ��������� � ��������� ������ ��.
         * @param el - this
         */
        function closeOtherList(el) {
            var thisList = $(el).closest(dropdownList),
            siblings = dropdownList.not(thisList);
            siblings.find('div.objects_list').hide();
            siblings.each(function(){
                var block = $(this);
                block.find('div.select_button').removeClass('active');
                block.find('div.objects_list').hide();
                block.find('span.arrow_down').show();
            });
        }

        /**
         * �� ����������� this (�� �������) ������� ������������ �������� � ������������� ����� �� ���������� �������.
         * @param el - this
         */
        function setObjectTitle(el) {
            var parent = $(el).closest(dropdownList),
                attributes = $(el).prop("attributes"),
                select = parent.find('span.select');
            parent.find('li').removeClass('selected');
            $(el).closest('li').addClass('selected');
            select.text($(el).text());
            select.show();
            parent.find('span.default').hide();
        }

    }
    if(dropdown.length) {
        /**
         * ������������� � ������ � ���������� ����
         */
        dropdown.on('click', function() {
            var menu = $(this).find('.dropdown-menu');
            menu.toggle();
            $('.dropdown-menu').not(menu).hide();
        });

        $(document).on('click', function(e){
            if(!$(e.target).closest(dropdown).length){
                dropdown.find('.dropdown-menu').hide();
            }
        });
    }

});
