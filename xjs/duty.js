$(document).ready(function() {

	$("#pldate").datepicker({
	changeYear: true,
	clearText: '��������', clearStatus: '',
	closeText: '�������', closeStatus: '',
	prevText: '&#x3c;����',  prevStatus: '',
	prevBigText: '&#x3c;&#x3c;', prevBigStatus: '',
	nextText: '����&#x3e;', nextStatus: '',
	nextBigText: '&#x3e;&#x3e;', nextBigStatus: '',
	currentText: '�������', currentStatus: '',
	monthNames: ['������','�������','����','������','���','����','����','������','��������','�������','������','�������'],
	monthNamesShort: ['���','���','���','���','���','���','���','���','���','���','���','���'],
	monthStatus: '', yearStatus: '',
	weekHeader: '��', weekStatus: '',
	dayNames: ['�����������','�����������','�������','�����','�������','�������','�������'],
	dayNamesShort: ['���','���','���','���','���','���','���'],
	dayNamesMin: ['��','��','��','��','��','��','��'],
	dayStatus: 'DD', dateStatus: 'D, M d',
	dateFormat: 'dd-mm-yy', firstDay: 1,
	initStatus: '', isRTL: false,
	showMonthAfterYear: false, yearSuffix: ''
	});

	$("#pldate").click(function() {
	    $(this).datepicker("show");
	});
	
	$("#ql1").click(function() { 
		if($('#nodate:checked').val() == undefined) $("#pldate").val($("#date_now").text());
	});	
	$("#ql2").click(function() {
		if($('#nodate:checked').val() == undefined) $("#pldate").val($("#date_tomorrow").text());
	});

	$("#nodate").click(function() {
		if ($('#nodate:checked').val() !== undefined) { 
			$("#pldate").val('');
			$("#pldate").addClass("disabled");
			$("#pldate").attr("disabled", true);
		}
		else {
			$("#pldate").removeClass("disabled");
			$("#pldate").attr("disabled", false);
		}
	});
	
	$("#duty_form").validate({
		rules: {
			kv02: {
				required: true
			},
			kv03: {
				required: true,
				minlength: 6				
			}
		},
		messages: {
			kv02: { required: '������� ��� ���������' },
			kv03: { required: '������� ����� ���������'}
		}
	});

    $("#duty_type1, #duty_type2").change(function(){
        if($(this).val()==2) {
            $('#ul_name').animate({height: 'hide'}, 400);
            $('#reg_id2').show().removeAttr('disabled');
            $('#reg_id1').hide().attr('disabled', 'disabled');
        }
        else {
            $('#ul_name').animate({height: 'show'}, 400);
            $('#reg_id1').show().removeAttr('disabled');
            $('#reg_id2').hide().attr('disabled', 'disabled');
        }
    });
});