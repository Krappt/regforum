/**
 * Created with JetBrains PhpStorm.
 * User: Vasilevs
 * Date: 17.12.12
 * Time: 22:47
 * To change this template use File | Settings | File Templates.
 */

jQuery.fn.ForceNumericOnly =
    function()
    {
        return this.each(function()
        {
            $(this).keydown(function(e)
            {
                var key = e.charCode || e.keyCode || 0;
                // ��������� backspace, tab, delete, �������, ������� ����� � ����� �� �������������� ����������
                return (
                    key == 8 ||
                        key == 9 ||
                        key == 46 ||
                        (key >= 37 && key <= 40) ||
                        (key >= 48 && key <= 57) ||
                        (key >= 96 && key <= 105));
            });
        });
    };


$(document).ready(function() {
    $("#txtbox").ForceNumericOnly();
});