/**
 * Created with JetBrains PhpStorm.
 * User: RegforumTeam
 * Date: 16.10.13
 * Time: 12:31
 * To change this template use File | Settings | File Templates.
 */

$(document).ready(function() {
    $('.help').poshytip({
        className: 'tip-yellowsimple',
        content: '������ ������ � ������� "����������" ������������� ��������� � �������� "����� ������". ���������� ������������ ����� ��������������� �������� ��������������. <br><a href="/month_author/">������ ������</a>.',
        showOn: 'none',
        showTimeout: 200,
        alignTo: 'target',
        alignX: 'center',
        alignY: 'top',
        offsetY: 7,
        liveEvents: true,
        allowTipHover: false
    });

    $('.help').click(function() { $('.help').poshytip('show'); });

    $(document).click(function(e) {
        if(!$(e.target).closest('.tip-yellowsimple').length && !$(e.target).closest('.help').length) {
            $('.help').poshytip('hide');
        }
    });

});
