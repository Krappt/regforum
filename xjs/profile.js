$(document).ready(function () {
    var mainBlock = $('.profile_topics'),
        grid = $('#all_topics_grid'),
        sortList = $('.sort');

    mainBlock.on('mouseenter', '#all_topics_grid .statistic .read', function () {
        var _this = $(this),
            id = _this.closest('.statistic').data('id');

        if (!_this.find('.dropdown-block').length) {
            $.ajax('/posts/uservisites/', {
                type: "GET",
                data: { id: id },
                cache: false,
                async: false,
                success: function (response) {
                    var html = buildDropdownBlock(response);
                    _this.append(html);
                },
                statusCode: {
                    404: function (xhr) {
                        if (window.console) console.log(xhr.responseText);
                    }
                }
            });
        }
    });

    mainBlock.on('mouseenter', '#all_topics_grid .statistic .vote', function () {
        var _this = $(this),
            id = _this.closest('.statistic').data('id');

        if (!_this.find('.dropdown-block').length) {
            $.ajax('/posts/getvotedmembers/', {
                type: "GET",
                data: { id: id },
                cache: false,
                async: false,
                success: function (response) {
                    var html = buildDropdownBlock(response);
                    _this.append(html);
                },
                statusCode: {
                    404: function (xhr) {
                        if (window.console) console.log(xhr.responseText);
                    }
                }
            });
        }
    });

    sortList.find('> span').on('click', function() {
        var _this = $(this),
            data;
        data = 'Topic_sort=' + _this.data('name') + (_this.hasClass('desc') ? '.asc' : '.desc');

        _this.hasClass('desc') || _this.hasClass('asc') ? _this.toggleClass('desc').toggleClass('asc') : _this.addClass('desc');
        _this.siblings().removeClass('desc asc');

        $.fn.yiiGridView.update(grid.prop('id'), {
            'url': $.fn.yiiGridView.getUrl(grid.prop('id')),
            'data' : data
        });
    });

    function buildDropdownBlock(response){
        var html = '<ul class="dropdown-block">';
        if (typeof response == 'object' && response.length) {
            for (var el in response) {
                if (typeof response[el].class == 'undefined') {
                    html += '<li><a href="' + response[el].url + '">' + response[el].name + '</a></li>';
                } else {
                    html += '<li><a href="' + response[el].url + '" class="' + response[el].class + '">' + response[el].name + '</a></li>';
                }
            }
        }
        html += '</ul>';
        return html;
    }
});