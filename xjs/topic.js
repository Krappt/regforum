/**
 * Created by JetBrains PhpStorm.
 * User: Vasilevs
 * Date: 21.08.12
 * Time: 12:55
 */

function calculate(obj, max) {
    var count = $(obj).val().length,
        available = max - count;

    $('#counter').html(((count < max - 200) ? ('�� ������� ' + count) : ('�������� ' + available)) + ' ��.');
}

$(document).ready(function () {

    var file_list = [],
        files = '',
        list_dialog = $('#list_dialog'),
        user_dialog = $('#user_dialog'),
        timeout;
	//var user_dialog = $('#user_dialog');
	
	if (list_dialog.length ){
	list_dialog.dialog({
		modal: true,
		autoOpen: false,
		closeOnEscape: true,
		resizable: false,
		title: "����� �����",
		height: 500,
		buttons: {
				"�������": function() {
					$(this).dialog("close");
				}
			}
	});
    }

	$('#topic_editor')
        .keyup(function(){calculate(this,30000)})
        .change(function(){calculate(this,30000)});

    var warning = $(".message");

    $('#rubricator').multiselect({
        noneSelectedText: '�������...',
        show: ['fade', 300],
        hide: ['fade', 300],
        selectedText: '������� # �� 3',
        minWidht: 160,
        header: "�� ������ ������� �� ����� ���� ��������",
        click: function(e){
            if( $(this).multiselect("widget").find("input:checked").length > 3 ){
                warning.addClass("error").removeClass("success").html("�� ������ ������� �� ����� ���� ��������");
            } else {
                warning.addClass("success").removeClass("error").html("Check a few boxes.");
            }
            return false;
        }
    });

	// ������ ��� ��������
	$("#add_attached").click(function() {
        if (!list_dialog.dialog('option', 'disabled')) {

            //�������� ���� ������
            $.getJSON('/posts/getfiles', { fastsearch: '' },
                function(obj) {
                    if (obj) {
                        $("#dialog_content").html(obj._content);
                    }
                });
            list_dialog.dialog("open");
        }

    });
	
	//������� ��� ������
	$('#dialog_content p').live('click', function() {
		var data_id = $(this).attr("id");
		var data_text = $(this).text();
		files = $('#attached_files').val();
		file_list = files.split(' ');
		if (file_list.join(' ').indexOf(data_id)<0) {
			file_list.push(data_id);
			$('#attached_files').val(file_list.join(' ').trim());
			$('#files').append('<div id="'+data_id+'">'+data_text+' <div id="file_delete" class="file_delete">&nbsp;</div></div>');
			console.info(file_list.join(' '));
			console.info(data_text);
		}
		list_dialog.dialog("close");
	});	
	

    $('#file_delete').live('click', function() {
        files = $('#attached_files').val();
        file_list = files.split(' ');
        var id_to_delete = $(this).parent('div').attr('id');
        for (i=0; i < file_list.length; i++){
            if (id_to_delete == file_list[i]){
                file_list.splice(i, 1);
                --i;
            }
        }
        $('#attached_files').val(file_list.join(' '));
        $(this).parent('div').remove();
    });

    $('#Topic_topic_type input').click(function() {
        //$('#Profile_opt_comments_view_mode input').click(function() {
        if($(this).val() != 'common') {
        //if($(this).prop("checked")) {
            $('#corporate_post').show('fast');
        }
        else {
            $('#corporate_post').hide('fast');
        }
        console.info($(this).val());
    });


    if (user_dialog.length ){
        user_dialog.dialog({
            modal: true,
            autoOpen: false,
            closeOnEscape: true,
            resizable: false,
            title: "������ �� ���������",
            height: 500,
            buttons: {
                "�������": function() {
                    $(this).dialog("close");
                }
            }
        });
    }

    tinyMCE.init({
        mode : "exact",
        elements : "topic_content",
        theme : "advanced",
        language : "ru",
        plugins : "paste,preview,fullscreen,searchreplace,inlinepopups,table",
        width: "970",
        height: "460",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,outdent,indent,blockquote,inquotes,user,|,search,replace,|,pasteword,|,bullist,numlist,|,undo,redo,|,image,insertimage,link,unlink,|,styleselect,formatselect,|,removeformat,preview,code,fullscreen", //
        theme_advanced_buttons2 : "tablecontrols",
        theme_advanced_buttons3 : "",
        theme_advanced_buttons4 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : false,
        theme_advanced_blockformats : "p,h2,blockquote",
        content_css : "/xcss/tinymce_body.css?12",
        setup : function(ed) {
/*            ed.addButton('topiccut', {
                title : '���',
                image : '/ximg/toolbar/topiccut.png',
                onclick: function() {
                    ed.focus();
                    ed.selection.select();
                    ed.selection.setContent('[topiccut]');
                }
            });*/
            ed.addButton('inquotes', {
                title : '� ��������',
                image : '/ximg/toolbar/in_quotes.png',
                onclick : function() {
                    ed.focus();
                    ed.selection.select();
                    ed.selection.setContent('&laquo;'+ed.selection.getSel()+'&raquo;');
                }
            });
            ed.addButton('user', {
                title : '������ �� ���������',
                image : '/ximg/toolbar/user-add.png',
                onclick : function() {
                    ed.focus();
                    if (!user_dialog.dialog('option', 'disabled')) {
                        user_dialog.dialog("open");
                    }

                    $(".dialog_filter").live('keyup', function() {
                        var _this = $(this);
                        clearTimeout(timeout);
                        timeout = setTimeout(function() {
                            var fastsearch = _this.val();
                            $.getJSON('/posts/getusers/', { fastsearch: fastsearch },
                                function(obj) {
                                    if (obj) {
                                        $("#user_dialog_content").html(obj._content);
                                    }
                                });
                        }, 500);
                    });
                    //������� ��� ������
                    $('#user_dialog_content').one('click', 'p', function() {
                        var _this = $(this),
                            parents = _this.parent(),
                            data_id = _this.attr("id"),
                            span = _this.find('span'),
                            data_text;
                        if(span.length) {
                            _this.find('span').remove();
                        }

                        data_text = _this.text();
                        ed.selection.setContent('<a class="member" href="http://regforum.ru/members/' + data_id + '/">' + data_text + '</a>');
                        user_dialog.dialog("close");
                        parents.html('').siblings('.top_filter').find('input').val('');
                    });
                }

            });
        },
        verify_html : true,
        inline_styles : false,
        apply_source_formatting : true,
        convert_fonts_to_spans : true,
        //valid_children : "-div[style,class],-p[style],-span[style,class]",
        valid_elements : "a[href|target|class],strong,br,img[src|width|height|align|hspace|vspace|class],em,p[align|class],h2,blockquote,u,ol,ul,li,del,table[class],td[class],tr[class]",
        invalid_elements : "iframe,script,applet,object,h3,h4,h5,h6,h7,h8,h9,cite,button,section,head,header,footer,article,embed,pre",
        removeformat_selector : 'b,strong,em,i,span,ins,div,li,ul,h2',
        style_formats : [
            {title : '����', selector : 'p', classes : 'incut'}
        ],
        'formats' : {
            'alignleft' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-left'},
            'aligncenter' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-center'},
            'alignright' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-right'},
            'alignfull' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-justify'},
            'italic' : {'inline' : 'em'},
            'bold' : {'inline' : 'strong'},
            'underline' : {'inline' : 'u'},
            'strikethrough' : {'inline' : 'del'}
        },
        removeformat : [
            {selector : 'div,b,strong,em,i,font,u,strike', remove : 'all', split : true, expand : false, block_expand : true, deep : true},
            {selector : 'span', attributes : ['style', 'class'], remove : 'empty', split : true, expand : false, deep : true}
            //{selector : '*', attributes : ['style', 'class'], remove: 'all', split : true, expand : true, deep : true}
        ],
        protect: [
            /\<\/?(if|endif)\>/g, // Protect <if> & </endif>
            /\<xsl\:[^>]+\>/g, // Protect <xsl:...>
            /<\?php.*?\?>/g // Protect php code
        ]
    });

	jQuery.fn.extend({
		insertAtCaret: function(myValue){
			return this.each(function(i) {
				if (document.selection) {
					// ��� ��������� ���� Internet Explorer
					this.focus();
					var sel = document.selection.createRange();
					sel.text = myValue;
					this.focus();
				}
				else if (this.selectionStart || this.selectionStart == '0') {
					// ��� ��������� ���� Firefox � ������ Webkit-��
					var startPos = this.selectionStart;
					var endPos = this.selectionEnd;
					var scrollTop = this.scrollTop;
					this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
					this.focus();
					this.selectionStart = startPos + myValue.length;
					this.selectionEnd = startPos + myValue.length;
					this.scrollTop = scrollTop;
				} else {
					this.value += myValue;
					this.focus();
				}
			})
		}
	});
	
});