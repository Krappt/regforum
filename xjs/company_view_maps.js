/**
 * Created with JetBrains PhpStorm.
 * User: Vasilevs
 * Date: 22.12.12
 * Time: 20:28
 */

var myMap;

function init () {
    var company_coord = $('#map_coordinates').text().split(',');
    myMap = new ymaps.Map('map', {
        center: [64,104],
        zoom: 10
    });
    myMap.behaviors.disable('drag');
    myMap.cursors.push('pointer');
    myPlacemark = new ymaps.Placemark(company_coord);
    var trafficControl = new ymaps.control.TrafficControl();
        myMap.geoObjects.add(myPlacemark);
    myMap.setCenter(company_coord,17);
    $('.ymaps-copyrights-logo, .ymaps-copyrights-legend').hide();
    $('#maptab').css('display','none');
}


function getCompanyPosition() {
    var company_coord = $('#map_coordinates').text().split(',');
    if(company_coord!='') {
        //console.info(company_coord);
        myPlacemark.geometry.setCoordinates(company_coord);
        myMap.setCenter(company_coord,17);
    }
}

$(document).ready(function () {
    if($('#map').length) ymaps.ready(init);
});