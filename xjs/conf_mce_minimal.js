/**
 * Created with JetBrains PhpStorm.
 * User: nakham
 * Date: 29.12.13
 * Time: 21:39
 * To change this template use File | Settings | File Templates.
 */

$(document).ready(function () {

    tinyMCE.init({
        selector : "textarea",
        theme : "advanced",
        language : "ru",
        plugins : "paste,searchreplace,inlinepopups",
        width: "100%",
        height: "200",
        theme_advanced_buttons1 : "bold,italic,underline,|,link,unlink",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : false,
        theme_advanced_blockformats : "p,h2,blockquote",
        content_css : "/xcss/tinymce_body.css",
        verify_html : true,
        inline_styles : false,
        apply_source_formatting : true,
        convert_fonts_to_spans : true,
        valid_elements : "a[href|target],strong,br,img[src|width|height|align|hspace|vspace],em,p[align],h2,blockquote,u,ol,ul,li,del",
        invalid_elements : "iframe,script,applet,object,h3,h4,h5,h6,h7,h8,h9,cite,button,section,head,header,footer,article,embed,pre",
        removeformat_selector : 'span,ins,div,li,ul,h2',
        removeformat : [
            {selector : 'div,font,strike', remove : 'all', split : true, expand : false, block_expand : true, deep : true},
            {selector : '*', attributes : ['style', 'class'], remove: 'all', split : true, expand : true, deep : true},
            {selector : 'span', attributes : ['style', 'class'], remove : 'empty', split : true, expand : false, deep : true}
        ],
        protect: [
            /\<\/?(if|endif)\>/g, // Protect <if> & </endif>
            /\<xsl\:[^>]+\>/g, // Protect <xsl:...>
            /<\?php.*?\?>/g // Protect php code
        ]
    });

});